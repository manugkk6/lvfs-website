#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison

import json
import time
from typing import Dict, Any, List

from flask import (
    Blueprint,
    Response,
    flash,
    redirect,
    render_template,
    request,
    url_for,
    g,
)
from flask_login import login_required


from lvfs import db, auth

from lvfs.util import admin_login_required
from lvfs.firmware.models import Firmware
from lvfs.tasks.models import Task
from lvfs.vendors.models import Vendor
from lvfs.metadata.models import Remote

bp_fsck = Blueprint("fsck", __name__, template_folder="templates")


@bp_fsck.route("/")
@login_required
@admin_login_required
def route_view() -> Any:
    return render_template("fsck.html", category="admin")


@bp_fsck.route("/list")
@login_required
@admin_login_required
def route_list() -> Any:
    tasks = (
        db.session.query(Task)
        .filter(Task.function == "lvfs.fsck.utils.task_all")
        .order_by(Task.started_ts.desc())
        .all()
    )
    return render_template("fsck-list.html", category="admin", tasks=tasks)


@bp_fsck.post("/update_descriptions")
@login_required
@admin_login_required
def route_update_descriptions() -> Any:

    # asynchronously run
    try:
        db.session.add(
            Task(
                value=json.dumps(
                    {
                        "search": request.form["search"] or None,
                        "replace": request.form["replace"] or None,
                    }
                ),
                caller=__name__,
                user=g.user,
                function="lvfs.fsck.utils.task_all_update_descriptions",
            )
        )
        db.session.commit()
    except KeyError:
        flash("No form data specified", "warning")
        return redirect(url_for("fsck.route_view"))

    flash("Updating update descriptions", "info")
    return redirect(url_for("fsck.route_view"))


@bp_fsck.post("/eventlog/delete")
@login_required
@admin_login_required
def route_eventlog_delete() -> Any:

    # asynchronously run
    try:
        db.session.add(
            Task(
                value=json.dumps({"value": request.form["value"]}),
                caller=__name__,
                user=g.user,
                function="lvfs.fsck.utils.task_all_eventlog_delete",
            )
        )
        db.session.commit()
    except KeyError:
        flash("No form data specified", "warning")
        return redirect(url_for("fsck.route_view"))

    flash("Deleting eventlog entries", "info")
    return redirect(url_for("fsck.route_view"))


@bp_fsck.post("/generate_stats")
@login_required
@admin_login_required
def route_generate_stats() -> Any:

    # asynchronously run
    flash("Generating stats", "info")
    db.session.add(
        Task(
            caller=__name__,
            user=g.user,
            function="lvfs.analytics.utils.task_generate_all",
        )
    )
    db.session.commit()

    return redirect(url_for("fsck.route_view"))


@bp_fsck.route("/healthcheck")
@auth.login_required
def route_healthcheck() -> Any:
    """this is unauthenticated!!"""

    # worst case scenario
    item: Dict[str, Any] = {}
    item["DatabaseRead"] = False
    item["DatabaseWrite"] = False
    item["FirmwareSigning"] = False
    item["MetadataSigning"] = False

    try:
        # find the first private firmware uploaded by the admin
        stmt = (
            db.session.query(Vendor.vendor_id)
            .filter(Vendor.group_id == "admin")
            .subquery()
        )
        fw = (
            db.session.query(Firmware)
            .join(stmt, Firmware.vendor_id == stmt.c.vendor_id)
            .join(Remote)
            .filter(Remote.name == "private")
            .order_by(Firmware.firmware_id.asc())
            .limit(1)
            .with_for_update(of=Firmware)
            .one()
        )
        item["DatabaseRead"] = True

        # mark firmware unsigned
        fw.signed_timestamp = None
        db.session.commit()
        item["DatabaseWrite"] = True

        # sign file
        db.session.add(
            Task(
                value=json.dumps({"id": fw.firmware_id}),
                caller=__name__,
                user=g.user,
                url=url_for("firmware.route_show", firmware_id=fw.firmware_id),
                function="lvfs.firmware.utils.task_sign_fw",
            )
        )
        db.session.commit()
        for _ in range(9):
            time.sleep(1)
            db.session.expire_all()
            if fw.signed_timestamp:
                break
        if fw.signed_timestamp:
            item["FirmwareSigning"] = True

        # check metadata was signed
        for _ in range(9):
            time.sleep(1)
            db.session.expire_all()
            if not fw.remote.is_dirty:
                break
        if not fw.remote.is_dirty:
            item["MetadataSigning"] = True

    except Exception as e:  # pylint: disable=broad-except
        item["Exception"] = str(e)

    dat = json.dumps(item, indent=4, separators=(",", ": "))
    return Response(response=dat, status=200, mimetype="application/json")


@bp_fsck.post("/fs")
@login_required
@admin_login_required
def route_fs() -> Any:

    # asynchronously check
    kinds: List[str] = []
    for key in request.form:
        if key.find("::") != -1:
            kinds.append(key)
    if not kinds:
        flash("No file system kinds selected", "warning")
        return redirect(url_for("fsck.route_view"))

    flash(
        "Checking file system consistency as background task… {}".format(
            ", ".join(kinds)
        ),
        "info",
    )
    db.session.add(
        Task(
            value=json.dumps({"kinds": kinds}),
            caller=__name__,
            user=g.user,
            function="lvfs.fsck.utils.task_all",
        )
    )
    db.session.commit()

    return redirect(url_for("fsck.route_list"))
