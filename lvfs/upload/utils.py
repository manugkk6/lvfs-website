#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=protected-access

import hashlib
import tempfile
import subprocess
import glob
import os
import json

from typing import List, Optional
from flask import render_template, url_for

from sqlalchemy import or_
from sqlalchemy.exc import IntegrityError

from cabarchive import CorruptionError, CabArchive, CabFile

from lvfs import db, ploader

from lvfs.agreements.models import Agreement
from lvfs.categories.models import Category
from lvfs.licenses.models import License
from lvfs.emails import send_email_sync
from lvfs.firmware.models import (
    Firmware,
    FirmwareEvent,
    FirmwareEventKind,
    FirmwareRevision,
    FirmwareVendor,
)
from lvfs.firmware.utils import _firmware_delete, _sign_fw
from lvfs.metadata.models import Remote
from lvfs.metadata.utils import _regenerate_and_sign_metadata_remote
from lvfs.protocols.models import Protocol
from lvfs.tasks.models import Task
from lvfs.upload.uploadedfile import (
    FileNotSupported,
    FileTooLarge,
    FileTooSmall,
    MetadataInvalid,
    UploadedFile,
)
from lvfs.users.models import User
from lvfs.vendors.models import Vendor
from lvfs.util import (
    _fix_component_name,
    _get_settings,
)
from lvfs.verfmts.models import Verfmt


def _user_can_upload(user: User) -> bool:

    # never signed anything
    if not user.agreement:
        return False

    # is it up to date?
    agreement = db.session.query(Agreement).order_by(Agreement.version.desc()).first()
    if not agreement:
        return False
    if user.agreement.version < agreement.version:
        return False

    # works for us
    return True


def _filter_fw_by_id_guid_version(
    fws: List[Firmware], appstream_id: str, provides_value: str, release_version: str
) -> Optional[Firmware]:
    for fw in fws:
        if fw.is_deleted:
            continue
        for md in fw.mds:
            if md.appstream_id != appstream_id:
                continue
            for guid in md.guids:
                if guid.value == provides_value and md.version == release_version:
                    return fw
    return None


def _repackage_archive(buf: bytes) -> bytes:
    """Unpacks an archive (typically a .zip) into a CabArchive object"""

    # decompress to a temp directory
    with tempfile.TemporaryDirectory(prefix="foreignarchive_") as dest:
        with tempfile.NamedTemporaryFile(
            mode="wb", prefix="foreignarchive_", suffix=".zip", dir=None, delete=True
        ) as src:
            src.write(buf)
            src.flush()

            # extract
            with subprocess.Popen(
                ["/usr/bin/bsdtar", "--directory", dest, "-xvf", src.name],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
            ) as ps:
                if ps.wait() != 0:
                    if ps.stderr:
                        raise IOError(
                            "Failed to extract: %s" % ps.stderr.read().decode()
                        )
                    raise IOError("Failed to extract")

        # add all the fake CFFILE objects
        cabarchive = CabArchive()
        for fn in glob.glob(dest + "/**/*.*", recursive=True):
            with open(fn, "rb") as f:
                fn = os.path.basename(fn.replace("\\", "/"))
                cabarchive[fn] = CabFile(f.read())

    # return blob
    return cabarchive.save()


def _upload_firmware_raw(
    revision_upload: FirmwareRevision,
    vendor: Vendor,
    target: str,
    addr: str,
    auto_delete: bool,
    task: Task,
    sign: bool = True,
) -> Firmware:

    # verify the user can upload
    user: User = task.user
    if not _user_can_upload(user):
        raise PermissionError("User has not signed legal agreement")

    # security check
    if not vendor.check_acl("@upload", user=user):
        raise PermissionError(
            "Failed to upload file for vendor: "
            "User with vendor {} cannot upload to vendor {}".format(
                user.vendor.group_id, vendor.group_id
            ),
        )

    # not correct parameters
    if target not in ["private", "embargo", "testing"]:
        raise KeyError("Target not valid, got {}".format(target))

    # find remote, creating if required
    remote_name = target
    if remote_name == "embargo":
        remote = vendor.remote
    else:
        remote = db.session.query(Remote).filter(Remote.name == remote_name).first()
    if not remote:
        raise FileNotSupported("No remote for target {}".format(remote_name))

    # add a new revision actually in .cab format
    if revision_upload.filename.endswith(".zip"):
        revision = FirmwareRevision(
            filename=revision_upload.filename.replace(".zip", ".cab")
        )
        revision.write(_repackage_archive(revision_upload.blob))
    else:
        revision = revision_upload

    # load in the archive
    try:
        ufile = UploadedFile(
            enable_inf_parsing=vendor.default_inf_parsing,
        )
        for cat in db.session.query(Category):
            ufile.category_map[cat.value] = cat.category_id
        for pro in db.session.query(Protocol):
            ufile.protocol_map[pro.value] = pro.protocol_id
        for verfmt in db.session.query(Verfmt):
            ufile.version_formats[verfmt.value] = verfmt
        for lic in db.session.query(License):
            ufile.license_map[lic.value] = lic
        ufile.parse(revision)
    except (
        CorruptionError,
        FileNotSupported,
        FileTooLarge,
        FileTooSmall,
        MetadataInvalid,
    ) as e:
        raise FileNotSupported(str(e)) from e

    # add the ODM as a string used for searching, but that does not get included in the XML
    for md in ufile.fw.mds:
        md.add_keywords_from_string(user.vendor.group_id, priority=-1)
        if user.vendor.display_name:
            md.add_keywords_from_string(user.vendor.display_name, priority=-1)

    # check the file does not already exist
    fw_old = (
        db.session.query(Firmware)
        .filter(
            or_(
                Firmware.checksum_upload_sha1 == ufile.fw.checksum_upload_sha1,
                Firmware.checksum_upload_sha256 == ufile.fw.checksum_upload_sha256,
            )
        )
        .first()
    )
    if fw_old:
        if fw_old.check_acl("@view", user=user):
            task.url = url_for("firmware.route_show", firmware_id=fw_old.firmware_id)
            raise FileExistsError(
                "A file with hash {} already exists: {}".format(
                    fw_old.checksum_upload_sha1,
                    url_for("firmware.route_show", firmware_id=fw_old.firmware_id),
                ),
            )
        raise FileExistsError(
            "Another user has already uploaded this firmware",
        )

    # check the guid and version does not already exist
    task.status = "Checking for duplicates…"
    db.session.commit()
    fws = db.session.query(Firmware).all()
    fws_already_exist: List[Firmware] = []
    for md in ufile.fw.mds:
        if not md.guids:
            continue
        provides_value = md.guids[0].value
        fw = _filter_fw_by_id_guid_version(
            fws, md.appstream_id, provides_value, md.version
        )
        if fw:
            fws_already_exist.append(fw)

    # all the components existed, so build an error out of all the versions
    if len(fws_already_exist) == len(ufile.fw.mds):
        if user.check_acl("@robot") and auto_delete:
            for fw in fws_already_exist:
                if fw.remote.is_public:
                    raise PermissionError(
                        "Firmware {} cannot be autodeleted as is in remote {}".format(
                            fw.firmware_id, fw.remote.name
                        )
                    )
                if fw.user.user_id != user.user_id:
                    raise PermissionError("Firmware was not uploaded by this user")
            for fw in fws_already_exist:
                # auto-delete due to robot upload
                _firmware_delete(fw, task=task)
        else:
            versions_for_display: List[str] = []
            for fw in fws_already_exist:
                for md in fw.mds:
                    if not md.version_display in versions_for_display:
                        versions_for_display.append(md.version_display)
            raise FileExistsError(
                "A firmware file for this device with "
                "version {} already exists".format(",".join(versions_for_display)),
            )

    # allow plugins to copy any extra files from the source archive
    if not ufile.cabarchive_upload:
        raise FileNotSupported("Failed to store uploaded file")
    task.status = "Copying archive…"
    db.session.commit()
    for cffile in ufile.cabarchive_upload.values():
        ploader.archive_copy(ufile.cabarchive_repacked, cffile)

    # create parent firmware object
    task.status = "Compressing archive…"
    db.session.commit()
    settings = _get_settings()
    fw = ufile.fw
    fw.blob = ufile.cabarchive_repacked.save()
    fw.revisions.append(revision_upload)
    if revision != revision_upload:
        fw.revisions.append(revision)
    fw.vendor = vendor
    fw.user = user
    fw.addr = addr
    fw.remote = remote
    fw.checksum_signed_sha1 = hashlib.sha1(fw.blob).hexdigest()
    fw.checksum_signed_sha256 = hashlib.sha256(fw.blob).hexdigest()
    fw.failure_minimum = settings["default_failure_minimum"]
    fw.failure_percentage = settings["default_failure_percentage"]

    for md in fw.mds:
        for issue in md.issues:
            issue.user = user

    # add ODM vendor access to firmware
    if user.vendor != vendor:
        fw.odm_vendors_map.append(FirmwareVendor(vendor=user.vendor, user=user))

    # fix name
    for md in fw.mds:
        name_fixed = _fix_component_name(md.name, vendor.display_name)
        if name_fixed != md.name:
            md.name = name_fixed

    # verify each component has a version format
    for md in fw.mds:
        if not md.verfmt:
            if md.protocol and md.protocol.verfmt:
                md.verfmt = md.protocol.verfmt
            elif (
                md.fw.vendor.verfmt
                and md.protocol
                and md.protocol.value == "org.uefi.capsule"
            ):
                md.verfmt = md.fw.vendor.verfmt

    # add to database
    fw.events.append(
        FirmwareEvent(kind=FirmwareEventKind.UPLOADED.value, remote=remote, user=user)
    )
    try:
        db.session.add(fw)
        db.session.commit()
    except IntegrityError as e:
        db.session.rollback()
        raise FileExistsError from e

    # ensure the test has been added for the firmware type
    task.status = "Ensuring tests…"
    db.session.commit()
    ploader.ensure_test_for_fw(fw)

    # sync everything we added
    db.session.commit()

    # asynchronously run
    task.status = "Running tests…"
    db.session.add(
        Task(
            value=json.dumps({"id": fw.firmware_id}),
            caller=__name__,
            user=task.user,
            url=url_for("firmware.route_show", firmware_id=fw.firmware_id),
            function="lvfs.tests.utils.task_run_for_firmware",
        )
    )
    db.session.commit()

    # send out emails to anyone interested
    if fw.get_possible_users_to_email:
        task.status = "Emailing users…"
        db.session.commit()
    for u in fw.get_possible_users_to_email:
        if u == user:
            continue
        if u.get_action("notify-upload-vendor") and u.vendor == fw.vendor:
            send_email_sync(
                "[LVFS] Firmware has been uploaded",
                u.email_address,
                render_template(
                    "email-firmware-uploaded.txt", user=u, user_upload=user, fw=fw
                ),
                task=task,
            )
        elif u.get_action("notify-upload-affiliate"):
            send_email_sync(
                "[LVFS] Firmware has been uploaded by affiliate",
                u.email_address,
                render_template(
                    "email-firmware-uploaded.txt", user=u, user_upload=user, fw=fw
                ),
                task=task,
            )

    # synchronously sign
    if sign:
        task.status = "Signing file…"
        db.session.commit()
        _sign_fw(fw, task=task)
        task.status = "Signing metadata…"
        db.session.commit()
        _regenerate_and_sign_metadata_remote(fw.remote, task=task)

    # success
    return fw


def task_upload_firmware(task: Task) -> None:

    try:
        values = json.loads(task.value)
    except (KeyError, TypeError) as e:
        task.add_fail("Failed to parse value", str(e))

    rev = (
        db.session.query(FirmwareRevision)
        .filter(FirmwareRevision.firmware_revision_id == values["id"])
        .with_for_update(of=FirmwareRevision)
        .first()
    )
    if not rev:
        return
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == values["vendor_id"])
            .one()
        )
        fw = _upload_firmware_raw(
            rev,
            task=task,
            vendor=vendor,
            target=values["target"],
            addr=values["addr"],
            auto_delete=values["auto_delete"],
        )
        db.session.add(fw)
        task.url = url_for("firmware.route_show", firmware_id=fw.firmware_id)
    except (FileNotSupported, PermissionError, FileExistsError, KeyError) as e:
        task.add_fail("Failed to upload", str(e))
    db.session.commit()
