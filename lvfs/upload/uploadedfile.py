#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=fixme,too-many-instance-attributes,too-few-public-methods,too-many-statements,protected-access

import hashlib
import configparser
import datetime
import fnmatch
from typing import Optional, Dict, Any

from lxml import etree as ET

from cabarchive import CabArchive, CabFile, NotSupportedError
from infparser import InfParser

from lvfs.categories.models import Category
from lvfs.licenses.models import License
from lvfs.components.models import (
    Component,
    ComponentChecksum,
    ComponentGuid,
    ComponentTag,
    ComponentIssue,
    ComponentRequirement,
    ComponentDescription,
)
from lvfs.firmware.models import Firmware, FirmwareRevision
from lvfs.protocols.models import Protocol
from lvfs.util import (
    DEVICE_ICONS,
    _markdown_from_root,
    _validate_appstream_id,
    _validate_guid,
    _validate_tag,
)
from lvfs.verfmts.models import Verfmt


class FileTooLarge(Exception):
    def __init__(self, message: str, *args: Any) -> None:
        if not message:
            message = "File is too large"
        super().__init__(message, *args)


class FileTooSmall(Exception):
    def __init__(self, message: str, *args: Any) -> None:
        if not message:
            message = "File is too small"
        super().__init__(message, *args)


class FileNotSupported(Exception):
    def __init__(self, message: str, *args: Any) -> None:
        if not message:
            message = "Unknown"
        super().__init__("File is not supported: {}".format(message), *args)


class MetadataInvalid(Exception):
    def __init__(self, message: str, *args: Any) -> None:
        if not message:
            message = "Unknown"
        super().__init__("Metadata is invalid: {}".format(message), *args)


def detect_encoding_from_bom(b: bytes) -> str:

    # UTF-8 BOM
    if b[0:3] == b"\xef\xbb\xbf":
        return "utf-8"

    # UTF-16 BOM
    if b[0:2] == b"\xfe\xff" or b[0:2] == b"\xff\xfe":
        return "utf-16"

    # UTF-32 BOM
    if b[0:5] == b"\xfe\xff\x00\x00" or b[0:5] == b"\x00\x00\xff\xfe":
        return "utf-32"

    # fallback
    return "cp1252"


def _node_validate_text_full(
    node: ET.SubElement, minlen: int = 0, maxlen: int = 0, nourl: bool = False
) -> Optional[str]:
    """Validates the style"""

    # unwrap description
    if node.tag == "description":
        try:
            text = _markdown_from_root(node)
        except KeyError as e:
            raise MetadataInvalid(str(e)) from e
    else:
        text = node.text
        if text:
            text = text.strip()
            for tag in ["<p>", "<li>", "<ul>", "<ol>"]:
                if text.find(tag) != -1:
                    raise MetadataInvalid(
                        "{} cannot specify markup tag {}".format(node.tag, tag)
                    )

    # no content
    if not text:
        return None

    # some tags can be split for multiple models
    if node.tag in ["name"]:
        textarray = text.split("/")
    else:
        textarray = [text]
    for textsplit in textarray:
        if minlen and len(textsplit) < minlen:
            raise MetadataInvalid(
                "<{}> is too short: {}/{}".format(node.tag, len(textsplit), minlen)
            )
        if maxlen and len(textsplit) > maxlen:
            raise MetadataInvalid(
                "<{}> is too long: {}/{}".format(node.tag, len(textsplit), maxlen)
            )

    # contains hyperlink
    if nourl:
        for urlhandler in ["http://", "https://", "ftp://"]:
            if text.find(urlhandler) != -1:
                raise MetadataInvalid(
                    "{} cannot contain a hyperlink: {}".format(node.tag, text)
                )

    return text


def _node_validate_text(
    node: ET.SubElement, minlen: int = 0, maxlen: int = 0, nourl: bool = False
) -> str:
    """Validates the style"""
    text = _node_validate_text_full(
        node=node, minlen=minlen, maxlen=maxlen, nourl=nourl
    )
    if not text:
        raise MetadataInvalid("{} has no value".format(node.tag))
    return text


class UploadedFile:
    def __init__(self, enable_inf_parsing: bool = True):
        """default public attributes"""

        self.fw = Firmware()
        self.enable_inf_parsing = enable_inf_parsing
        self.version_formats: Dict[str, Verfmt] = {}
        self.category_map: Dict[str, Category] = {}
        self.protocol_map: Dict[str, Protocol] = {}
        self.license_map: Dict[str, License] = {}

        # strip out any unlisted files
        self.cabarchive_repacked = CabArchive()

        # private
        self._data_size = 0
        self.cabarchive_upload: Optional[CabArchive] = None
        self._version_inf = None

    def _parse_inf(self, contents: str) -> None:

        # FIXME is banned...
        if contents.find("FIXME") != -1:
            raise MetadataInvalid(
                "The inf file was not complete; Any FIXME text must be "
                "replaced with the correct values."
            )

        # check .inf file is valid
        try:
            cfg = InfParser(contents)
        except configparser.MissingSectionHeaderError as e:
            raise MetadataInvalid("The inf file could not be parsed") from e
        try:
            tmp = cfg.get("Version", "Class")
        except (configparser.NoOptionError, configparser.NoSectionError) as e:
            raise MetadataInvalid("The inf file Version:Class was missing") from e
        if tmp.lower() != "firmware":
            raise MetadataInvalid("The inf file Version:Class was invalid")
        try:
            tmp = cfg.get("Version", "ClassGuid")
        except configparser.NoOptionError as e:
            raise MetadataInvalid("The inf file Version:ClassGuid was missing") from e
        if tmp.lower() != "{f2e7dd72-6468-4e36-b6f1-6488f42c1b52}":
            raise MetadataInvalid("The inf file Version:ClassGuid was invalid")
        try:
            version_display = cfg.get("Version", "DriverVer").split(",")
            if len(version_display) != 2:
                raise MetadataInvalid("The inf file Version:DriverVer was invalid")
            self.fw._version_display = version_display[1]
        except configparser.NoOptionError:
            pass

        # this is optional, but if supplied must match the version in the XML
        # -- also note this will not work with multi-component .cab files
        if len(self.fw.mds) == 1 and self.fw.mds[0].version.isdigit():
            try:
                version_inf = cfg.get("Firmware_AddReg", "HKR->FirmwareVersion")
                if version_inf:
                    if version_inf.startswith("0x"):
                        version_inf = str(int(version_inf[2:], 16))
                    if version_inf != "0":
                        self._version_inf = version_inf  # type: ignore
            except (configparser.NoOptionError, configparser.NoSectionError):
                pass

    @staticmethod
    def _parse_release(md: Component, release: ET.SubElement) -> None:

        # get descriptions
        namespaces = {"xml": "http://www.w3.org/XML/1998/namespace"}
        for n_desc in release.xpath("description", namespaces=namespaces):
            locale = n_desc.get("{http://www.w3.org/XML/1998/namespace}lang")
            if locale == "en_US":
                locale = None
            tx = md.get_description_by_locale(locale)
            if tx:
                raise MetadataInvalid(
                    "<description> has multiple translations for locale {}".format(
                        locale
                    )
                )
            md.descriptions.append(
                ComponentDescription(
                    locale=locale,
                    value=_node_validate_text(
                        n_desc, minlen=3, maxlen=1000, nourl=True
                    ),
                )
            )

        md.install_duration = int(release.get("install_duration", "0"))
        md.release_urgency = release.get("urgency")
        if md.release_urgency and md.release_urgency not in [
            "low",
            "medium",
            "high",
            "critical",
        ]:
            raise MetadataInvalid("<release> has invalid urgency attribute")

        # date, falling back to timestamp
        if "date" in release.attrib:
            try:
                dt = datetime.datetime.strptime(release.get("date"), "%Y-%m-%d")
                dt_utc = dt.replace(tzinfo=datetime.timezone.utc)
                md.release_timestamp = int(dt_utc.timestamp())
            except ValueError as e:
                raise MetadataInvalid("<release> has invalid date attribute") from e
        elif "timestamp" in release.attrib:
            try:
                md.release_timestamp = int(release.get("timestamp"))
            except ValueError as e:
                raise MetadataInvalid(
                    "<release> has invalid timestamp attribute"
                ) from e
        else:
            raise MetadataInvalid("<release> had no date or timestamp attributes")

        # optional release tag
        if "tag" in release.attrib:
            md.release_tag = release.attrib["tag"]
            if len(md.release_tag) < 4:
                raise MetadataInvalid(
                    "<release> tag was too short to identify the firmware"
                )
            md.add_keywords_from_string(md.release_tag, priority=6)

        # get list of CVEs
        for issue in release.xpath("issues/issue"):
            kind = issue.get("type")
            if not kind:
                raise MetadataInvalid("<issue> had no type attribute")
            if kind not in ComponentIssue.ISSUE_KINDS:
                raise MetadataInvalid(
                    "<issue> type can only be {}".format(
                        ",".join(ComponentIssue.ISSUE_KINDS)
                    )
                )
            value = _node_validate_text(issue, minlen=3, maxlen=1000, nourl=True)
            issue = ComponentIssue(kind=kind, value=value)
            if issue.problem:
                raise MetadataInvalid(
                    "<issue> invalid {}".format(issue.problem.description)
                )
            md.issues.append(issue)

        # get <url type="details">
        try:
            md.details_url = _node_validate_text(
                release.xpath('url[@type="details"]')[0], minlen=12, maxlen=1000
            )
        except IndexError:
            pass

        # get <url type="source">
        try:
            md.source_url = _node_validate_text(
                release.xpath('url[@type="source"]')[0], minlen=12, maxlen=1000
            )
        except IndexError:
            pass

        # fix up hex version
        md.version = release.get("version")
        if not md.version:
            raise MetadataInvalid("<release> had no version attribute")
        if md.version.startswith("0x"):
            md.version = str(int(md.version[2:], 16))

        # ensure there's always a contents filename
        try:
            md.filename_contents = release.xpath('checksum[@target="content"]')[0].get(
                "filename"
            )
        except IndexError:
            pass
        try:
            md.filename_contents = _node_validate_text(
                release.xpath("artifacts/artifact/filename")[0],
                minlen=3,
                maxlen=1000,
                nourl=True,
            )
        except IndexError:
            pass
        if not md.filename_contents:
            md.filename_contents = "firmware.bin"

        # ensure there's always a contents filename
        for csum in release.xpath('checksum[@target="device"]'):
            text = _node_validate_text(csum, minlen=32, maxlen=128).lower()
            if csum.get("kind") == "sha1":
                md.device_checksums.append(ComponentChecksum(value=text, kind="SHA1"))
            elif csum.get("kind") == "sha256":
                md.device_checksums.append(ComponentChecksum(value=text, kind="SHA256"))
        if not md.filename_contents:
            md.filename_contents = "firmware.bin"

    def _parse_component(self, component: ET.SubElement) -> Component:

        # get priority
        md = Component()
        md.priority = int(component.get("priority", "0"))

        # check type
        md.appstream_type = component.get("type")
        if md.appstream_type not in ["firmware", "generic"]:
            raise MetadataInvalid(
                '<component type="firmware"> or <component type="generic"> required'
            )

        # get <id>
        try:
            md.appstream_id = _node_validate_text(
                component.xpath("id")[0], minlen=10, maxlen=256
            )
            if not _validate_appstream_id(md.appstream_id):
                raise MetadataInvalid("<id> value invalid")
        except IndexError as e:
            raise MetadataInvalid("<id> tag missing") from e

        # get <name>
        try:
            md.name = _node_validate_text(
                component.xpath("name")[0], minlen=3, maxlen=500
            )
            md.add_keywords_from_string(md.name, priority=3)
        except IndexError as e:
            raise MetadataInvalid("<name> tag missing") from e

        # get <summary>
        try:
            md.summary = _node_validate_text(
                component.xpath("summary")[0], minlen=10, maxlen=500
            )
            md.add_keywords_from_string(md.summary, priority=1)
        except IndexError as e:
            raise MetadataInvalid("<summary> tag missing") from e

        # get <icon>
        try:
            md.icon = _node_validate_text(
                component.xpath("icon")[0], minlen=3, maxlen=100
            )
            if md.icon not in DEVICE_ICONS:
                raise MetadataInvalid("<icon> tag should contain a valid name")
        except IndexError:
            pass

        # get optional <name_variant_suffix>
        try:
            md.name_variant_suffix = _node_validate_text(
                component.xpath("name_variant_suffix")[0], minlen=2, maxlen=500
            )
        except IndexError:
            pass

        # get optional <branch>
        try:
            md.branch = _node_validate_text(
                component.xpath("branch")[0], minlen=2, maxlen=500
            )
        except IndexError:
            pass

        # get optional <description}
        try:
            md.description = _node_validate_text(
                component.xpath("description")[0], minlen=25, maxlen=1000, nourl=True
            )
        except IndexError:
            pass

        # get <metadata_license>
        try:
            text = _node_validate_text(component.xpath("metadata_license")[0])
            if not self.license_map:
                md.metadata_license = License(value=text)
            elif text not in self.license_map:
                raise MetadataInvalid(
                    "Unknown <metadata_license> tag of {}".format(text)
                )
            else:
                md.metadata_license = self.license_map[text]
                if not md.metadata_license.is_content:
                    raise MetadataInvalid(
                        "Invalid <metadata_license> tag of {}".format(text)
                    )
        except AttributeError as e:
            raise MetadataInvalid("<metadata_license> tag") from e
        except IndexError as e:
            raise MetadataInvalid("<metadata_license> tag missing") from e

        # get <url type="homepage">
        try:
            md.url_homepage = _node_validate_text(
                component.xpath('url[@type="homepage"]')[0], minlen=7, maxlen=1000
            )
        except IndexError as e:
            raise MetadataInvalid('<url type="homepage"> tag missing') from e
        if not md.url_homepage:
            raise MetadataInvalid('<url type="homepage"> value invalid')

        # add manually added keywords
        for keyword in component.xpath("keywords/keyword"):
            text = _node_validate_text(keyword, minlen=3, maxlen=50, nourl=True)
            if text.find(" ") != -1:
                raise MetadataInvalid("<keywords> cannot contain spaces")
            md.add_keywords_from_string(text, priority=5)

        # add provides
        for prov in component.xpath('provides/firmware[@type="flashed"]'):
            text = _node_validate_text(prov, minlen=5, maxlen=1000)
            if not _validate_guid(text):
                raise MetadataInvalid("The GUID {} was invalid.".format(text))
            md.guids.append(ComponentGuid(value=text))
        if not md.guids and md.appstream_type == "firmware":
            raise MetadataInvalid("The metadata file did not provide any GUID.")

        # add tags
        for prov in component.xpath('tags/tag[@type="lvfs"]'):
            text = _node_validate_text(prov, minlen=3, maxlen=100)
            if not _validate_tag(text):
                raise MetadataInvalid("The tag {} was invalid.".format(text))
            md.tags.append(ComponentTag(value=text))

        # check the file didn't try to add it's own <require> on vendor-id
        # to work around the vendor-id security checks in fwupd
        if component.xpath('requires/firmware[text()="vendor-id"]'):
            raise MetadataInvalid("Firmware cannot specify vendor-id")

        # check only recognized requirements are added
        for hardness in [None, "recommends", "suggests"]:
            for req in component.xpath("{}/*".format(hardness or "requires")):
                if req.tag == "firmware":
                    req_value = _node_validate_text_full(req, minlen=3, maxlen=1000)
                    rq = ComponentRequirement(
                        kind=req.tag,
                        value=req_value,
                        compare=req.get("compare"),
                        version=req.get("version"),
                        depth=req.get("depth", None),
                        hardness=hardness,
                    )
                    md.requirements.append(rq)
                elif req.tag == "id":
                    text = _node_validate_text(req, minlen=3, maxlen=1000)
                    rq = ComponentRequirement(
                        kind=req.tag,
                        value=text,
                        compare=req.get("compare"),
                        version=req.get("version"),
                        hardness=hardness,
                    )
                    md.requirements.append(rq)
                elif req.tag == "hardware":
                    text = _node_validate_text(req, minlen=3, maxlen=1000)
                    for req_value in text.split("|"):
                        rq = ComponentRequirement(
                            kind=req.tag,
                            value=req_value,
                            compare=req.get("compare"),
                            version=req.get("version"),
                            hardness=hardness,
                        )
                        md.requirements.append(rq)
                elif req.tag == "client":
                    text = _node_validate_text(req, minlen=3, maxlen=1000)
                    for req_value in text.split("|"):
                        md.requirements.append(
                            ComponentRequirement(
                                kind=req.tag, value=req_value, hardness=hardness
                            )
                        )
                else:
                    raise MetadataInvalid(
                        "<{}> requirement was invalid".format(req.tag)
                    )

        # from the first screenshot
        try:
            md.screenshot_caption = _node_validate_text(
                component.xpath("screenshots/screenshot/caption")[0],
                minlen=8,
                maxlen=1000,
                nourl=True,
            )
        except IndexError:
            pass
        try:
            md.screenshot_url = _node_validate_text(
                component.xpath("screenshots/screenshot/image")[0],
                minlen=8,
                maxlen=1000,
            )
        except IndexError:
            pass

        # allows OEM to hide the direct download link on the LVFS
        if component.xpath('custom/value[@key="LVFS::InhibitDownload"]'):
            md.inhibit_download = True

        # allows OEM to disable ignore all kinds of statistics on this firmware
        if component.xpath('custom/value[@key="LVFS::DoNotTrack"]'):
            md.fw.do_not_track = True

        # specifies the device signing integrity
        try:
            DEVICE_INTEGRITY_VALUES = ["signed", "unsigned"]
            md.device_integrity = _node_validate_text(
                component.xpath('custom/value[@key="LVFS::DeviceIntegrity"]')[-1]
            )
            if md.device_integrity not in DEVICE_INTEGRITY_VALUES:
                raise MetadataInvalid(
                    "LVFS::DeviceIntegrity can only be {} not {}".format(
                        ",".join(DEVICE_INTEGRITY_VALUES), md.device_integrity
                    )
                )
        except IndexError:
            pass

        # allows OEM to change the triplet (AA.BB.CCDD) to quad (AA.BB.CC.DD)
        try:
            version_format = _node_validate_text(
                component.xpath('custom/value[@key="LVFS::VersionFormat"]')[-1]
            )
            if not self.version_formats:
                md.verfmt = Verfmt(value=version_format)
            elif version_format not in self.version_formats:
                raise MetadataInvalid(
                    "LVFS::VersionFormat can only be {} not {}".format(
                        ",".join(self.version_formats.keys()), version_format
                    )
                )
            else:
                md.verfmt = self.version_formats[version_format]
        except IndexError:
            pass

        # allows OEM to specify protocol
        try:
            text = _node_validate_text(
                component.xpath('custom/value[@key="LVFS::UpdateProtocol"]')[0]
            )
            if not self.protocol_map:
                md.protocol = Protocol(value=text)
            elif text not in self.protocol_map:
                raise MetadataInvalid("No valid UpdateProtocol {} found".format(text))
            else:
                md.protocol_id = self.protocol_map[text]
        except IndexError:
            pass

        # allows OEM to set banned country codes
        try:
            text = _node_validate_text(
                component.xpath('custom/value[@key="LVFS::BannedCountryCodes"]')[0],
                minlen=2,
                maxlen=1000,
                nourl=True,
            )
            self.fw.banned_country_codes = text.split(",")
        except IndexError:
            pass

        # allows OEM to set the update message
        try:
            text = _node_validate_text(
                component.xpath('custom/value[@key="LVFS::UpdateMessage"]')[0],
                minlen=8,
                maxlen=1000,
                nourl=True,
            )
            md.release_message = text
        except IndexError:
            pass

        # allows OEM to set the update image
        try:
            text = _node_validate_text(
                component.xpath('custom/value[@key="LVFS::UpdateImage"]')[0],
                minlen=8,
                maxlen=1000,
            )
            md.release_image = text
        except IndexError:
            pass

        # should we parse the .inf file?
        try:
            text = _node_validate_text(
                component.xpath('custom/value[@key="LVFS::EnableInfParsing"]')[0],
                minlen=2,
                maxlen=10,
                nourl=True,
            )
            if text == "true":
                self.enable_inf_parsing = True
            elif text == "false":
                self.enable_inf_parsing = False
            else:
                raise MetadataInvalid(
                    "LVFS::EnableInfParsing only allowed true or false, got {}".format(
                        text
                    )
                )
        except IndexError:
            pass

        # allows OEM to specify category
        for category in component.xpath("categories/category"):
            text = _node_validate_text(category, minlen=4, maxlen=50, nourl=True)
            if not self.category_map:
                md.category = Category(value=text)
                break
            if text in self.category_map:
                md.category_id = self.category_map[text]
                break

        # lenovo battery team
        if md.icon == "battery" and "X-Battery" in self.category_map:
            md.category_id = self.category_map["X-Battery"]
            md.icon = None

        # parse the default (first) release
        default_release = None
        try:
            default_release = component.xpath("releases/release")[0]
        except IndexError as e:
            if md.appstream_type == "firmware":
                raise MetadataInvalid(
                    "The metadata file did not provide any releases"
                ) from e
        if default_release is not None:
            self._parse_release(md, default_release)

        # get <project_license> if required
        if default_release is not None:
            try:
                text = _node_validate_text(
                    component.xpath("project_license")[0],
                    minlen=3,
                    maxlen=50,
                    nourl=True,
                )
                if text in ["proprietary", "Proprietary"]:
                    text = "LicenseRef-proprietary"
                if not self.license_map:
                    md.project_license = License(value=text)
                elif text not in self.license_map:
                    raise MetadataInvalid(
                        "Unknown <project_license> tag of {}".format(text)
                    )
                else:
                    md.project_license = self.license_map[text]
            except IndexError as e:
                raise MetadataInvalid("<project_license> tag missing") from e
            if not md.project_license:
                raise MetadataInvalid("<project_license> value invalid")

        # ensure the update description does not refer to a file in the archive
        if self.cabarchive_upload:
            for tx in md.descriptions:
                if not tx.value:
                    continue
                for word in tx.value.split(" "):
                    if word.find(".") == -1:  # any word without a dot is not a fn
                        continue
                    if word in self.cabarchive_upload:
                        raise MetadataInvalid(
                            "The release description should not reference other files."
                        )

        # check the inf file matches up with the .xml file
        if self._version_inf and self._version_inf != md.version:
            raise MetadataInvalid(
                "The inf Firmware_AddReg[HKR->FirmwareVersion] "
                "%s did not match the metainfo.xml value %s."
                % (self._version_inf, md.version)
            )

        # success
        return md

    def _parse_metainfo(self, cabfile: CabFile) -> None:

        # sanity check
        if not cabfile.buf:
            raise MetadataInvalid("No metainfo data")
        if not cabfile.filename:
            raise MetadataInvalid("No metainfo filename")
        if not self.cabarchive_upload:
            raise MetadataInvalid("No upload archive")

        # check the file does not have any missing request.form
        if cabfile.buf.decode("utf-8", "ignore").find("FIXME") != -1:
            raise MetadataInvalid(
                "The metadata file was not complete; "
                "Any FIXME text must be replaced with the correct values."
            )

        # has UTF-8 BOM: https://en.wikipedia.org/wiki/Byte_order_mark
        if cabfile.buf.startswith(b"\xEF\xBB\xBF"):
            raise MetadataInvalid(
                "The metadata file has a UTF-8 BOM that must be removed"
            )

        # add to the archive
        self.cabarchive_repacked[cabfile.filename] = cabfile

        # parse MetaInfo file
        try:
            parser = ET.XMLParser()
            components = ET.fromstring(cabfile.buf, parser).xpath("/component")
            if not components:
                raise MetadataInvalid("<component> tag missing")
            if len(components) > 1:
                raise MetadataInvalid("Multiple <component> tags")
        except UnicodeDecodeError as e:
            raise MetadataInvalid(
                "The metadata file could not be parsed as unicode"
            ) from e
        except ET.XMLSyntaxError as e:
            item = parser.error_log[0]
            raise MetadataInvalid(
                "The metadata file could not be parsed as valid XML on line {}: {}".format(
                    item.line, item.message
                )
            ) from e
        md = self._parse_component(components[0])
        md.release_download_size = self._data_size
        md.filename_xml = cabfile.filename

        # add any local images to the archive
        for img in [md.release_image, md.screenshot_url]:
            if not img:
                continue
            if not img.startswith("file://"):
                continue
            try:
                fn = img[7:]
                self.cabarchive_repacked[fn] = self.cabarchive_upload[fn]
            except KeyError as e:
                raise MetadataInvalid("No {} found in the archive".format(fn)) from e

        # add the firmware.bin to the archive
        if md.filename_contents:
            try:
                cabfile_fw = self.cabarchive_upload[md.filename_contents]
            except KeyError as e:
                raise MetadataInvalid(
                    "No {} found in the archive".format(md.filename_contents)
                ) from e
            if not cabfile_fw.buf:
                raise MetadataInvalid(
                    "Invalid {} found in the archive".format(md.filename_contents)
                )
            if not cabfile_fw.filename:
                raise MetadataInvalid("No filename found in the archive")
            self.cabarchive_repacked[cabfile_fw.filename] = cabfile_fw
            md.checksum_contents_sha1 = hashlib.sha1(cabfile_fw.buf).hexdigest()
            md.checksum_contents_sha256 = hashlib.sha256(cabfile_fw.buf).hexdigest()
            md.release_installed_size = len(cabfile_fw.buf)
        self.fw.mds.append(md)

    def parse(self, revision: FirmwareRevision, data: Optional[bytes] = None) -> None:

        # check size
        if data is None:
            data = revision.blob
        self._data_size = len(data)
        if self._data_size > 0x40000000:
            raise FileTooLarge("File too large, limit is 1Gb")
        if self._data_size < 0x400:
            raise FileTooSmall(
                "File too small, minimum is 1k and was {}".format(self._data_size)
            )

        # get new filename
        self.fw.checksum_upload_sha1 = hashlib.sha1(data).hexdigest()
        self.fw.checksum_upload_sha256 = hashlib.sha256(data).hexdigest()

        # parse the file
        try:
            self.cabarchive_upload = CabArchive(data, flattern=True)
        except (NotImplementedError, NotSupportedError) as e:
            raise FileNotSupported("Invalid file type: {}".format(str(e))) from e

        # load metainfo files
        cabfiles = [
            cabfile
            for cabfile in self.cabarchive_upload.values()
            if cabfile.filename and fnmatch.fnmatch(cabfile.filename, "*.metainfo.xml")
        ]
        if not cabfiles:
            raise MetadataInvalid("The firmware file had no .metainfo.xml files")

        # parse each MetaInfo file
        for cabfile in cabfiles:
            self._parse_metainfo(cabfile)

        # all generic component without a firmware does not make sense
        all_generic = True
        for md in self.fw.mds:
            if md.appstream_type != "generic":
                all_generic = False
                break
        if all_generic:
            raise MetadataInvalid("The firmware file had all generic components")

        # verify .inf files if they exists
        inffiles = [
            cabfile
            for cabfile in self.cabarchive_upload.values()
            if cabfile.filename and fnmatch.fnmatch(cabfile.filename, "*.inf")
        ]
        for cabfile in inffiles:

            # add to the archive
            if cabfile.filename:
                self.cabarchive_repacked[cabfile.filename] = cabfile

            # parse
            if cabfile.buf and self.enable_inf_parsing:
                encoding = detect_encoding_from_bom(cabfile.buf)
                self._parse_inf(cabfile.buf.decode(encoding))
