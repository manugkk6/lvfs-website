#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,too-many-lines

import os
import secrets
import fnmatch
import hashlib
import json

from collections import defaultdict
from glob import glob
from typing import Dict, List, Tuple, Iterable, Callable, Any
from flask import current_app as app

from flask import Blueprint, request, flash, url_for, redirect, render_template, g
from flask_login import login_required
from sqlalchemy.orm import joinedload
from sqlalchemy.exc import NoResultFound, IntegrityError
from werkzeug.exceptions import BadRequestKeyError

from lvfs import db, cache

from lvfs.emails import send_email
from lvfs.firmware.models import Firmware
from lvfs.categories.models import Category
from lvfs.hash import _otp_hash
from lvfs.metadata.models import Remote
from lvfs.tasks.models import Task
from lvfs.users.models import User
from lvfs.util import (
    admin_login_required,
    _email_check,
    _generate_password,
)
from lvfs.verfmts.models import Verfmt

from .models import (
    Vendor,
    VendorAffiliation,
    VendorAffiliationAction,
    VendorBranch,
    VendorRestriction,
    VendorNamespace,
    VendorTag,
    VendorAsset,
    VendorProdcert,
)
from .utils import _vendor_hash

bp_vendors = Blueprint("vendors", __name__, template_folder="templates")


def _count_vendor_fws_public(vendor: Vendor, remote_name: str) -> int:
    dedupe_csum: Dict[str, bool] = {}
    for fw in vendor.fws:
        if fw.remote.name == remote_name:
            dedupe_csum[fw.checksum_upload_sha256] = True
    return len(dedupe_csum)


def _count_vendor_fws_downloads(vendor: Vendor, remote_name: str) -> int:
    cnt = 0
    for fw in vendor.fws:
        if fw.remote.name == remote_name:
            cnt += fw.download_cnt
    return cnt


def _count_vendor_fws_devices(vendor: Vendor, remote_name: str) -> int:
    guids: Dict[str, bool] = {}
    for fw in vendor.fws:
        if fw.remote.name == remote_name:
            for md in fw.mds:
                for gu in md.guids:
                    guids[gu.value] = True
    return len(guids)


class VendorStat:
    def __init__(self, stable: int, testing: int):
        self.stable = stable
        self.testing = testing


def _get_vendorlist_stats(
    vendors: List[Vendor], fn: Callable[[Vendor, str], int]
) -> Tuple[List[str], List[float], List[float]]:

    # get stats
    display_names: Dict[str, VendorStat] = {}
    for v in vendors:
        if not v.visible:
            continue
        cnt_stable = fn(v, "stable")
        cnt_testing = fn(v, "testing")
        if not cnt_stable and not cnt_testing:
            continue
        display_name = v.display_name.split(" ")[0]
        if display_name not in display_names:
            display_names[display_name] = VendorStat(cnt_stable, cnt_testing)
            continue
        stat = display_names[display_name]
        stat.stable += cnt_stable
        stat.testing += cnt_testing

    # build graph data
    labels: List[str] = []
    data_stable: List[float] = []
    data_testing: List[float] = []
    keys: Iterable = display_names.items()
    vendors = sorted(list(keys), key=lambda k: k[1].stable + k[1].testing, reverse=True)
    for display_name, stat in vendors[:10]:
        labels.append(str(display_name))
        data_stable.append(float(stat.stable))
        data_testing.append(float(stat.testing))
    return labels, data_stable, data_testing


def _abs_to_pc(data: List[float], data_other: List[float]) -> List[float]:
    total: float = 0
    for num in data:
        total += num
    for num in data_other:
        total += num
    data_pc: List[float] = []
    for num in data:
        data_pc.append(round(num * 100 / total, 2))
    return data_pc


@bp_vendors.route("/list/<page>")
@cache.cached()
def route_list_analytics(page: str) -> Any:
    vendors = (
        db.session.query(Vendor)
        .order_by(Vendor.display_name)
        .options(joinedload(Vendor.fws))
        .all()
    )
    if page == "publicfw":
        labels, data_stable, data_testing = _get_vendorlist_stats(
            vendors, _count_vendor_fws_public
        )
        return render_template(
            "vendorlist-analytics.html",
            vendors=vendors,
            category="vendors",
            title="Total number of public firmware files",
            page=page,
            labels=labels,
            data_stable=data_stable,
            data_testing=data_testing,
        )
    if page == "downloads":
        labels, data_stable, data_testing = _get_vendorlist_stats(
            vendors, _count_vendor_fws_downloads
        )
        return render_template(
            "vendorlist-analytics.html",
            vendors=vendors,
            category="vendors",
            title="Percentage of firmware downloads",
            page=page,
            labels=labels,
            data_stable=_abs_to_pc(data_stable, data_testing),
            data_testing=_abs_to_pc(data_testing, data_stable),
        )
    if page == "devices":
        labels, data_stable, data_testing = _get_vendorlist_stats(
            vendors, _count_vendor_fws_devices
        )
        return render_template(
            "vendorlist-analytics.html",
            vendors=vendors,
            category="vendors",
            title="Total number of supported devices",
            page=page,
            labels=labels,
            data_stable=data_stable,
            data_testing=data_testing,
        )
    flash("Vendorlist kind invalid", "warning")
    return redirect(url_for("vendors.route_list"))


@bp_vendors.route("/")
@cache.cached()
def route_list() -> Any:
    vendors = (
        db.session.query(Vendor)
        .filter(Vendor.visible)
        .join(User, Vendor.vendor_id == User.vendor_id)
        .options(joinedload(Vendor.affiliations))
        .order_by(Vendor.display_name)
        .all()
    )
    return render_template("vendorlist.html", vendors=vendors)


@bp_vendors.route("/admin")
@login_required
@admin_login_required
def route_list_admin() -> Any:
    vendors = (
        db.session.query(Vendor)
        .options(joinedload(Vendor.restrictions))
        .order_by(Vendor.group_id)
        .all()
    )
    return render_template(
        "vendorlist-admin.html", category="vendors", vendors=vendors, page="overview"
    )


@bp_vendors.route("/<int:vendor_id>")
@cache.cached()
def route_show_public(vendor_id: int) -> Any:
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.visible)
            .filter(Vendor.vendor_id == vendor_id)
            .options(joinedload(Vendor.users), joinedload(Vendor.affiliations))
            .one()
        )
    except NoResultFound:
        flash("Failed to show vendor: No visible vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list"), 302)
    return render_template(
        "vendor-show.html", category="vendors", v=vendor, page="overview"
    )


@bp_vendors.route("/create", methods=["GET", "POST"])
@login_required
@admin_login_required
def route_create() -> Any:
    """Add a vendor [ADMIN ONLY]"""

    # only accept form data
    if request.method != "POST":
        return redirect(url_for("vendors.route_list_admin"))

    try:
        if len(request.form["group_id"]) > 80:
            flash("Failed to add vendor: Group ID is too long", "warning")
            return redirect(url_for("vendors.route_list_admin"), 302)
    except KeyError:
        flash("Unable to add vendor as no data", "warning")
        return redirect(url_for("vendors.route_list"))

    # use a random access token, unless running in debug mode
    if app.config.get("DEBUG", None):
        access_token = request.form["group_id"].replace("-", "_")
    else:
        access_token = secrets.token_hex(nbytes=32)

    try:
        remote = Remote(
            name="embargo-%s" % request.form["group_id"],
            access_token=access_token,
        )
        db.session.add(remote)
        v = Vendor(group_id=request.form["group_id"], remote=remote)
        db.session.add(v)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add vendor: Group ID already exists", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    flash("Added vendor %s" % request.form["group_id"], "info")

    # asynchronously run
    db.session.add(
        Task(
            value=json.dumps({"id": remote.remote_id}),
            caller=__name__,
            user=g.user,
            url=url_for("vendors.route_show", vendor_id=v.vendor_id),
            function="lvfs.metadata.utils.task_regenerate_remote",
        )
    )
    db.session.commit()

    return redirect(url_for("vendors.route_show", vendor_id=v.vendor_id), 302)


@bp_vendors.post("/<int:vendor_id>/retoken")
@login_required
@admin_login_required
def route_retoken(vendor_id: int) -> Any:
    """Removes a vendor [ADMIN ONLY]"""
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to delete vendor: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)

    # delete files with the old access token
    fns = glob(
        os.path.join(
            app.config["DOWNLOAD_DIR"],
            "firmware-*-{}.*".format(vendor.remote.access_token),
        )
    )
    for fn in fns:
        os.remove(fn)

    # a random string
    vendor.remote.access_token = secrets.token_hex(nbytes=32)

    # asynchronously run
    db.session.add(
        Task(
            value=json.dumps({"id": vendor.remote.remote_id}),
            caller=__name__,
            user=g.user,
            url=url_for("vendors.route_show", vendor_id=vendor.vendor_id),
            function="lvfs.metadata.utils.task_regenerate_remote",
        )
    )
    db.session.commit()

    flash("Regenerated vendor access token", "info")
    return redirect(url_for("vendors.route_list_admin"), 302)


@bp_vendors.post("/<int:vendor_id>/delete")
@login_required
@admin_login_required
def route_delete(vendor_id: int) -> Any:
    """Removes a vendor [ADMIN ONLY]"""
    try:
        db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Cannot delete", "danger")
        return redirect(url_for("vendors.route_list_admin"), 302)
    flash("Removed vendor", "info")
    return redirect(url_for("vendors.route_list_admin"), 302)


@bp_vendors.route("/<int:vendor_id>")
@bp_vendors.route("/<int:vendor_id>/details")
@login_required
@admin_login_required
def route_show(vendor_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""
    try:
        vendor = db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).one()
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    verfmts = db.session.query(Verfmt).all()
    return render_template(
        "vendor-details.html", category="vendors", verfmts=verfmts, v=vendor
    )


@bp_vendors.route("/<int:vendor_id>/firmware")
@login_required
@admin_login_required
def route_firmware(vendor_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""
    try:
        vendor = db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).one()
    except NoResultFound:
        flash("Failed to get vendor details: No vendor with that ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)

    # get all user IDs for this vendor
    stmt = db.session.query(User.user_id).filter(User.vendor_id == vendor_id).subquery()

    # get all firmware that were uploaded by these user IDs
    fws = (
        db.session.query(Firmware)
        .join(stmt, Firmware.user_id == stmt.c.user_id)
        .order_by(Firmware.timestamp.desc())
        .limit(101)
        .all()
    )

    return render_template(
        "vendor-firmware.html", category="vendors", fws=fws, v=vendor
    )


@bp_vendors.route("/<int:vendor_id>/restrictions")
@login_required
def route_restrictions(vendor_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""

    # security check
    try:
        vendor = db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).one()
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that ID", "warning")
        return redirect(url_for("vendors.route_list"), 302)
    if not vendor.check_acl("@view-restrictions"):
        flash("Permission denied: Unable to view restrictions", "danger")
        return redirect(url_for("vendors.route_list"), 302)
    return render_template("vendor-restrictions.html", category="vendors", v=vendor)


@bp_vendors.route("/<int:vendor_id>/namespaces")
@login_required
@admin_login_required
def route_namespaces(vendor_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""
    try:
        vendor = db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).one()
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)

    # get prefixes from existing firmware
    appstream_ids: Dict[str, int] = defaultdict(int)
    for fw in vendor.fws:
        if fw.is_deleted:
            continue
        for md in fw.mds:
            prefix = md.appstream_id_prefix
            if prefix in [ns.value for ns in vendor.namespaces]:
                continue
            appstream_ids[prefix] += 1

    # try to construct something plausible from the vendor homepage
    if not appstream_ids and vendor.url:
        parts = vendor.url.split("/", maxsplit=3)
        if len(parts) >= 3:
            dotted = parts[2].rsplit(".", maxsplit=3)
            if len(dotted) >= 2:
                prefix = "{}.{}".format(dotted[-1], dotted[-2])
                if prefix not in [ns.value for ns in vendor.namespaces]:
                    appstream_ids[prefix] = 0

    return render_template(
        "vendor-namespaces.html",
        appstream_ids=appstream_ids,
        category="vendors",
        v=vendor,
    )


@bp_vendors.route("/<int:vendor_id>/tags")
@login_required
@admin_login_required
def route_tags(vendor_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""
    try:
        vendor = db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).one()
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    return render_template("vendor-tags.html", category="vendors", v=vendor)


@bp_vendors.route("/<int:vendor_id>/tag/<int:tag_id>")
@login_required
@admin_login_required
def route_tag_details(vendor_id: int, tag_id: int) -> Any:
    """Allow editing a vendor tag [ADMIN ONLY]"""

    # check exists
    try:
        tag = (
            db.session.query(VendorTag)
            .filter(VendorTag.vendor_tag_id == tag_id)
            .join(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .one()
        )
    except NoResultFound:
        flash("Failed to get tag: No tag with those IDs", "warning")
        return redirect(url_for("vendors.route_tags", vendor_id=vendor_id))
    categories = db.session.query(Category).order_by(Category.name.asc()).all()
    return render_template(
        "vendor-tag-details.html",
        category="vendors",
        categories=categories,
        v=tag.vendor,
        tag=tag,
    )


@bp_vendors.route("/<int:vendor_id>/branches")
@login_required
@admin_login_required
def route_branches(vendor_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""
    try:
        vendor = db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).one()
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    return render_template("vendor-branches.html", category="vendors", v=vendor)


@bp_vendors.route("/<int:vendor_id>/users")
@login_required
def route_users(vendor_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""

    # check exists
    try:
        vendor = db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).one()
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list"), 302)

    # security check
    if not vendor.check_acl("@manage-users"):
        flash("Permission denied: Unable to edit vendor as non-admin", "danger")
        return redirect(url_for("vendors.route_show", vendor_id=vendor_id))
    return render_template("vendor-users.html", category="vendors", v=vendor)


@bp_vendors.route("/<int:vendor_id>/oauth")
@login_required
def route_oauth(vendor_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""

    # check exists
    try:
        vendor = db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).one()
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list"), 302)

    # security check
    if not vendor.check_acl("@modify-oauth"):
        flash("Permission denied: Unable to edit vendor as non-admin", "danger")
        return redirect(url_for("vendors.route_show", vendor_id=vendor_id))
    return render_template("vendor-oauth.html", category="vendors", v=vendor)


@bp_vendors.post("/<int:vendor_id>/restriction/create")
@login_required
@admin_login_required
def route_restriction_create(vendor_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    try:
        vendor.restrictions.append(VendorRestriction(value=request.form["value"]))
    except KeyError:
        flash("No value", "warning")
        return redirect(url_for("vendors.route_list"))
    db.session.commit()
    flash("Added restriction", "info")

    # asynchronously run
    db.session.add(
        Task(
            value=json.dumps({"id": vendor.remote.remote_id}),
            caller=__name__,
            user=g.user,
            url=url_for("vendors.route_restrictions", vendor_id=vendor_id),
            function="lvfs.metadata.utils.task_regenerate_remote",
        )
    )
    db.session.commit()

    return redirect(url_for("vendors.route_restrictions", vendor_id=vendor_id), 302)


@bp_vendors.post("/<int:vendor_id>/restriction/<int:restriction_id>/delete")
@login_required
@admin_login_required
def route_restriction_delete(vendor_id: int, restriction_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    for res in vendor.restrictions:
        if res.restriction_id == restriction_id:
            db.session.delete(res)
            break
    flash("Deleted restriction", "info")

    # asynchronously run
    db.session.add(
        Task(
            value=json.dumps({"id": vendor.remote.remote_id}),
            caller=__name__,
            user=g.user,
            url=url_for("vendors.route_restrictions", vendor_id=vendor_id),
            function="lvfs.metadata.utils.task_regenerate_remote",
        )
    )
    db.session.commit()

    return redirect(url_for("vendors.route_restrictions", vendor_id=vendor_id), 302)


@bp_vendors.route("/<int:vendor_id>/namespace/create", methods=["POST", "GET"])
@login_required
@admin_login_required
def route_namespace_create(vendor_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    if "value" in request.form:
        ns = VendorNamespace(value=request.form["value"], user=g.user)
    elif "value" in request.args:
        ns = VendorNamespace(value=request.args["value"], user=g.user)
    else:
        flash("No value", "warning")
        return redirect(url_for("vendors.route_list"))
    if not ns.is_valid:
        flash(
            "Failed to add namespace: Invalid value, expecting something like com.dell",
            "warning",
        )
        return redirect(url_for("vendors.route_namespaces", vendor_id=vendor_id))
    vendor.namespaces.append(ns)
    db.session.commit()
    flash("Added namespace", "info")
    return redirect(url_for("vendors.route_namespaces", vendor_id=vendor_id), 302)


@bp_vendors.post("/<int:vendor_id>/namespace/<int:namespace_id>/delete")
@login_required
@admin_login_required
def route_namespace_delete(vendor_id: int, namespace_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    for res in vendor.namespaces:
        if res.namespace_id == namespace_id:
            db.session.delete(res)
            db.session.commit()
            break
    flash("Deleted namespace", "info")
    return redirect(url_for("vendors.route_namespaces", vendor_id=vendor_id), 302)


@bp_vendors.post("/<int:vendor_id>/tag/create")
@login_required
@admin_login_required
def route_tag_create(vendor_id: int) -> Any:
    """Allows creating a vendor tag [ADMIN ONLY]"""

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to add tag: No vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    try:
        tag = VendorTag(vendor=vendor, name=request.form["name"], user=g.user)
    except BadRequestKeyError:
        flash("Failed to add tag: Required values not found", "warning")
        return redirect(url_for("vendors.route_tags", vendor_id=vendor_id), 302)

    vendor.tags.append(tag)
    db.session.commit()
    flash("Added tag {}".format(tag.name), "info")
    return redirect(
        url_for(
            "vendors.route_tag_details", vendor_id=vendor_id, tag_id=tag.vendor_tag_id
        ),
        302,
    )


@bp_vendors.post("/<int:vendor_id>/tag/<int:tag_id>/modify")
@login_required
@admin_login_required
def route_tag_modify(vendor_id: int, tag_id: int) -> Any:
    """Allows modifying a vendor tag [ADMIN ONLY]"""

    # check exists
    try:
        tag = (
            db.session.query(VendorTag)
            .filter(VendorTag.vendor_tag_id == tag_id)
            .join(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=[Vendor, VendorTag])
            .one()
        )
    except NoResultFound:
        flash("Failed to modify tag: No tag with those IDs", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)

    try:
        tag.name = request.form["name"]
        tag.example = request.form["example"]
        tag.details_url = request.form.get("details_url", None)
        tag.enforce = bool(request.form.get("enforce", False))
        tag.category_id = request.form.get("category_id", 0) or None
    except BadRequestKeyError:
        flash("Failed to modify tag: Required values not found", "warning")
        return redirect(
            url_for("vendors.route_tag_details", vendor_id=vendor_id, tag_id=tag_id),
            302,
        )

    db.session.commit()
    flash("Modified tag {}".format(tag.name), "info")
    return redirect(
        url_for("vendors.route_tag_details", vendor_id=vendor_id, tag_id=tag_id), 302
    )


@bp_vendors.post("/<int:vendor_id>/tag/<int:tag_id>/delete")
@login_required
@admin_login_required
def route_tag_delete(vendor_id: int, tag_id: int) -> Any:
    """Allows deleting a vendor tag [ADMIN ONLY]"""

    # check exists
    try:
        tag = (
            db.session.query(VendorTag)
            .filter(VendorTag.vendor_tag_id == tag_id)
            .join(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=[Vendor, VendorTag])
            .one()
        )
    except NoResultFound:
        flash("Failed to delete tag: No tag with those IDs", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    db.session.delete(tag)
    db.session.commit()
    flash("Deleted tag", "info")
    return redirect(url_for("vendors.route_tags", vendor_id=vendor_id), 302)


@bp_vendors.route("/prodcerts")
@cache.cached()
def route_prodcerts() -> Any:
    """Show the list of product certificatation [PUBLIC]"""
    prodcerts: Dict[Vendor, List[VendorProdcert]] = defaultdict(list)
    for prodcert in (
        db.session.query(VendorProdcert)
        .filter(VendorProdcert.visible)
        .order_by(VendorProdcert.name.asc())
    ):
        prodcerts[prodcert.vendor.display_name].append(prodcert)
    return render_template(
        "vendor-prodcert.html", category="vendors", prodcerts=dict(prodcerts)
    )


@bp_vendors.route("/<int:vendor_id>/prodcert/list")
@login_required
@admin_login_required
def route_prodcert_list(vendor_id: int) -> Any:
    """Show the list of product certificatation [ADMIN ONLY]"""
    try:
        vendor = db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).one()
    except NoResultFound:
        flash("Failed to get vendor details: No vendor with that ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)

    return render_template("vendor-prodcert-list.html", category="vendors", v=vendor)


@bp_vendors.post("/<int:vendor_id>/prodcert/create")
@login_required
@admin_login_required
def route_prodcert_create(vendor_id: int) -> Any:
    """Allows creating a product certificatation for a vendor [ADMIN ONLY]"""

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to get vendor details: No vendor", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    try:
        vb = VendorProdcert(name=request.form["name"], user=g.user)
        vendor.prodcerts.append(vb)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash(
            "Failed to add product certificatation: device model already exists",
            "warning",
        )
        return redirect(
            url_for("vendors.route_prodcert_list", vendor_id=vendor_id), 302
        )
    flash("Added product certificatation", "info")
    return redirect(
        url_for(
            "vendors.route_prodcert_details",
            vendor_id=vendor_id,
            vendor_prodcert_id=vb.vendor_prodcert_id,
        ),
        302,
    )


@bp_vendors.route("/<int:vendor_id>/prodcert/<int:vendor_prodcert_id>")
@login_required
@admin_login_required
def route_prodcert_details(vendor_id: int, vendor_prodcert_id: int) -> Any:
    """Allow editing a vendor product certificatation [ADMIN ONLY]"""

    # check exists
    try:
        prodcert = (
            db.session.query(VendorProdcert)
            .filter(VendorProdcert.vendor_prodcert_id == vendor_prodcert_id)
            .join(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .one()
        )
    except NoResultFound:
        flash("No product certificatation with those IDs", "warning")
        return redirect(url_for("vendors.route_prodcert_list", vendor_id=vendor_id))
    return render_template(
        "vendor-prodcert-details.html",
        category="vendors",
        v=prodcert.vendor,
        prodcert=prodcert,
    )


@bp_vendors.post("/<int:vendor_id>/prodcert/<int:vendor_prodcert_id>/delete")
@login_required
@admin_login_required
def route_prodcert_delete(vendor_id: int, vendor_prodcert_id: int) -> Any:
    """Allows deleting a vendor product certificatation [ADMIN ONLY]"""

    # check exists
    try:
        prodcert = (
            db.session.query(VendorProdcert)
            .filter(VendorProdcert.vendor_prodcert_id == vendor_prodcert_id)
            .join(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=[Vendor, VendorProdcert])
            .one()
        )
    except NoResultFound:
        flash("No product certificatation with those IDs", "warning")
        return redirect(url_for("vendors.route_prodcert_list", vendor_id=vendor_id))
    db.session.delete(prodcert)
    db.session.commit()
    flash("Deleted product certificatation ", "info")
    return redirect(url_for("vendors.route_prodcert_list", vendor_id=vendor_id), 302)


@bp_vendors.post("/<int:vendor_id>/prodcert/<int:vendor_prodcert_id>/modify")
@login_required
@admin_login_required
def route_prodcert_modify(vendor_id: int, vendor_prodcert_id: int) -> Any:
    """Allows modifying a vendor product certificatation [ADMIN ONLY]"""

    # save to database
    try:
        prodcert = (
            db.session.query(VendorProdcert)
            .filter(VendorProdcert.vendor_prodcert_id == vendor_prodcert_id)
            .join(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=VendorProdcert)
            .one()
        )
    except NoResultFound:
        flash("No product certificatation with those IDs", "warning")
        return redirect(url_for("vendors.route_prodcert_list", vendor_id=vendor_id))
    for key in [
        "name",
        "summary",
        "url",
        "signature_kind",
        "signature_details",
    ]:
        if key in request.form:
            setattr(prodcert, key, request.form[key] if request.form[key] else None)
    for key in [
        "visible",
    ]:
        if key in request.form:
            setattr(prodcert, key, bool(request.form[key] == "1"))
    db.session.commit()
    flash("Updated product certificatation", "info")
    return redirect(url_for("vendors.route_prodcert_list", vendor_id=vendor_id), 302)


@bp_vendors.route("/<int:vendor_id>/branch/create", methods=["POST", "GET"])
@login_required
@admin_login_required
def route_branch_create(vendor_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""

    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
        vendor.branches.append(VendorBranch(value=request.form["value"], user=g.user))
        db.session.commit()
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add branch: ID already exists", "warning")
        return redirect(url_for("vendors.route_branches", vendor_id=vendor_id), 302)
    flash("Added branch ID", "info")
    return redirect(url_for("vendors.route_branches", vendor_id=vendor_id), 302)


@bp_vendors.post("/<int:vendor_id>/branch/<int:branch_id>/delete")
@login_required
@admin_login_required
def route_branch_delete(vendor_id: int, branch_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    for vb in vendor.branches:
        if vb.branch_id == branch_id:
            db.session.delete(vb)
            db.session.commit()
            break
    flash("Deleted branch ID", "info")
    return redirect(url_for("vendors.route_branches", vendor_id=vendor_id), 302)


@bp_vendors.route("/<int:vendor_id>/modify_by_admin", methods=["GET", "POST"])
@login_required
@admin_login_required
def route_modify_by_admin(vendor_id: int) -> Any:
    """Change details about the any vendor"""

    # only accept form data
    if request.method != "POST":
        return redirect(url_for("vendors.route_list_admin"))

    # save to database
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to modify vendor: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    for key in [
        "display_name",
        "legal_name",
        "internal_team",
        "group_id",
        "quote_text",
        "quote_author",
        "consulting_text",
        "consulting_link",
        "oauth_unknown_user",
        "oauth_domain_glob",
        "comments",
        "username_glob",
        "verfmt_id",
        "url",
        "keywords",
        "psirt_url",
        "missing_url",
        "subgroups",
    ]:
        if key in request.form:
            setattr(vendor, key, request.form[key] if request.form[key] else None)
            # special case so that the embargo name matches
            if key == "group_id":
                vendor.remote.name = "embargo-{}".format(vendor.group_id)
    for key in [
        "is_embargo_default",
        "do_not_track",
        "default_inf_parsing",
        "visible",
        "visible_on_landing",
        "visible_for_search",
        "relaxed_upload_rules",
        "allow_component_tags",
        "is_community",
    ]:
        if key in request.form:
            setattr(vendor, key, bool(request.form[key] == "1"))
    db.session.commit()
    flash("Updated vendor", "info")
    return redirect(url_for("vendors.route_show", vendor_id=vendor_id), 302)


@bp_vendors.post("/<int:vendor_id>/upload")
@login_required
@admin_login_required
def route_upload(vendor_id: int) -> Any:

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to modify vendor: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)

    # write the pixmap
    try:
        buf = request.files["file"].read()
    except KeyError:
        flash("No file", "warning")
        return redirect(url_for("vendors.route_list"))
    hmac_dig = _vendor_hash(vendor)
    fn = os.path.join(app.config["UPLOAD_DIR"], "vendor-{}.png".format(hmac_dig))
    with open(fn, "wb") as f:
        f.write(buf)

    vendor.icon = os.path.basename(fn)
    db.session.commit()
    flash("Modified vendor", "info")

    return redirect(url_for("vendors.route_show", vendor_id=vendor_id), 302)


def _verify_username_vendor_glob(username: str, username_glob: str) -> bool:
    for tmp in username_glob.split(","):
        if fnmatch.fnmatch(username, tmp):
            return True
    return False


@bp_vendors.post("/<int:vendor_id>/user/create")
@login_required
def route_user_create(vendor_id: int) -> Any:
    """Add a user to the vendor"""

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to modify vendor: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list"), 302)

    # security check
    if not vendor.check_acl("@manage-users"):
        flash("Permission denied: Unable to modify vendor as non-admin", "danger")
        return redirect(url_for("vendors.route_show", vendor_id=vendor_id))

    # verify email
    try:
        username = request.form["username"].lower()
        display_name = request.form["display_name"]
    except IndexError:
        flash("Unable to add user as no data", "danger")
        return redirect(url_for("vendors.route_show", vendor_id=vendor_id))
    if not _email_check(username):
        flash("Failed to add user: Invalid email address", "warning")
        return redirect(url_for("users.route_list"), 302)

    # verify the username matches the allowed vendor glob
    if not g.user.check_acl("@admin"):
        if not vendor.username_glob:
            flash(
                "Failed to add user: "
                "Admin has not set the account policy for this vendor",
                "warning",
            )
            return redirect(url_for("vendors.route_users", vendor_id=vendor_id), 302)
        if not _verify_username_vendor_glob(username, vendor.username_glob):
            flash(
                "Failed to add user: "
                "Email address does not match account policy %s" % vendor.username_glob,
                "warning",
            )
            return redirect(url_for("vendors.route_users", vendor_id=vendor_id), 302)

    # add user
    if g.user.vendor.oauth_domain_glob:
        user = User(
            username=username,
            display_name=display_name,
            auth_type="oauth",
            vendor_id=vendor.vendor_id,
        )
    else:
        user = User(
            username=username,
            display_name=display_name,
            auth_type="local",
            otp_secret=_otp_hash(),
            vendor_id=vendor.vendor_id,
        )
        # this is stored hashed
        password = _generate_password()
        user.password = password
    try:
        db.session.add(user)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add user: Username already exists", "warning")
        return redirect(url_for("vendors.route_users", vendor_id=vendor_id), 302)

    # send email
    if user.auth_type == "local":
        send_email(
            "[LVFS] An account has been created",
            user.email_address,
            render_template("email-confirm.txt", user=user, password=password),
        )

    # done!
    flash("Added user %i" % user.user_id, "info")
    return redirect(url_for("vendors.route_users", vendor_id=vendor_id), 302)


@bp_vendors.route("/<int:vendor_id>/affiliations")
@login_required
def route_affiliations(vendor_id: int) -> Any:
    """Allows changing vendor affiliations [ADMIN ONLY]"""

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list"), 302)

    # security check
    if not vendor.check_acl("@view-affiliations"):
        flash("Permission denied: Unable to view affiliations", "danger")
        return redirect(url_for("vendors.route_show", vendor_id=vendor_id))

    # ACLs possible for the OEM to grant to the ODM
    possible_actions = {
        "@delete": "Delete firmware",
        "@promote-stable": "Promote firmware to stable",
        "@promote-testing": "Promote firmware to testing",
        "@modify": "Modify firmware update details",
        "@modify-affiliation": "Modify firmware affiliation",
        "@nuke": "Delete firmware permanently",
        "@modify-limit": "Change download limits for firmware",
        "@undelete": "Undelete firmware",
        "@view": "View firmware",
        "@view-analytics": "View analytics about firmware",
        "@modify-updateinfo": "Modify the update release notes",
        "@modify-keywords": "Add and remove firmware keywords",
        "@modify-requirements": "Modify firmware requirements, e.g. fwupd version",
        "@modify-checksums": "Add and remove device checksums, e.g. PCR0",
        "@modify-appstream-id": "Modify the AppStream ID",
        "@retry": "Retry a failed test",
        "@waive": "Waive a failing test",
        "@translate": "Translate localized text",
    }

    # add other vendors
    vendors: List[Vendor] = []
    for v in db.session.query(Vendor).order_by(Vendor.display_name):
        if v.vendor_id == vendor_id:
            continue
        if not v.is_account_holder:
            continue
        if v.is_affiliate_for(vendor.vendor_id):
            continue
        vendors.append(v)
    return render_template(
        "vendor-affiliations.html",
        category="vendors",
        v=vendor,
        possible_actions=possible_actions,
        other_vendors=vendors,
    )


@bp_vendors.route(
    "/<int:vendor_id>/affiliation/<int:affiliation_id>/action/create/<action>",
    methods=["POST"],
)
@login_required
def route_affiliation_action_create(
    vendor_id: int, affiliation_id: int, action: str
) -> Any:
    """add an ACL action to an existing affiliation"""

    # security check
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that ID", "warning")
        return redirect(url_for("main.route_dashboard"))
    if not vendor.check_acl("@modify-affiliation-actions"):
        flash("Permission denied: Unable to modify vendor affiliation", "danger")
        return redirect(url_for("vendors.route_affiliations", vendor_id=vendor_id))

    # already exists?
    try:
        aff = (
            db.session.query(VendorAffiliation)
            .filter(VendorAffiliation.affiliation_id == affiliation_id)
            .one()
        )
    except NoResultFound:
        flash("Failed to add action: No affiliation with that ID", "warning")
        return redirect(url_for("vendors.route_affiliations", vendor_id=vendor_id))
    if not action.startswith("@"):
        flash('Failed to add action: Expected "@" prefix', "warning")
        return redirect(url_for("vendors.route_affiliations", vendor_id=vendor_id))
    if aff.get_action(action):
        flash("Failed to add action: Already present", "warning")
        return redirect(url_for("vendors.route_affiliations", vendor_id=vendor_id))

    # add
    aff.actions.append(VendorAffiliationAction(action=action, user=g.user))
    db.session.commit()
    flash("Added action", "info")
    return redirect(url_for("vendors.route_affiliations", vendor_id=vendor_id))


@bp_vendors.route(
    "/<int:vendor_id>/affiliation/<int:affiliation_id>/action/remove/<action>",
    methods=["POST"],
)
@login_required
def route_affiliation_action_remove(
    vendor_id: int, affiliation_id: int, action: str
) -> Any:
    """remove an ACL action to an existing affiliation"""

    # security check
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that ID", "warning")
        return redirect(url_for("main.route_dashboard"))
    if not vendor.check_acl("@modify-affiliation-actions"):
        flash("Permission denied: Unable to modify vendor affiliation", "danger")
        return redirect(url_for("vendors.route_affiliations", vendor_id=vendor_id))

    # already exists?
    try:
        aff = (
            db.session.query(VendorAffiliation)
            .filter(VendorAffiliation.affiliation_id == affiliation_id)
            .one()
        )
    except NoResultFound:
        flash("Failed to remove action: No affiliation with that ID", "warning")
        return redirect(url_for("vendors.route_affiliations", vendor_id=vendor_id))
    if not aff.get_action(action):
        flash("Failed to remove action: Not present", "warning")
        return redirect(url_for("vendors.route_affiliations", vendor_id=vendor_id))

    # remove
    for act in aff.actions:
        if act.action == action:
            aff.actions.remove(act)
    db.session.commit()
    flash("Removed action", "info")
    return redirect(url_for("vendors.route_affiliations", vendor_id=vendor_id))


@bp_vendors.post("/<int:vendor_id>/affiliation/create")
@login_required
def route_affiliation_create(vendor_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to add affiliate: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_affiliations", vendor_id=vendor_id), 302)

    # security check
    if not vendor.check_acl("@modify-affiliations"):
        flash("Permission denied: Unable to add vendor affiliation", "danger")
        return redirect(url_for("vendors.route_show", vendor_id=vendor_id))

    # check if it already exists
    try:
        vendor_id_odm = int(request.form["vendor_id_odm"])
    except KeyError:
        flash("No value", "warning")
        return redirect(url_for("vendors.route_show", vendor_id=vendor_id))
    for rel in vendor.affiliations:
        if rel.vendor_id_odm == vendor_id_odm:
            flash(
                "Failed to add affiliate: Already a affiliation with that ODM",
                "warning",
            )
            return redirect(
                url_for("vendors.route_affiliations", vendor_id=vendor_id), 302
            )

    # add a new ODM -> OEM affiliation
    aff = VendorAffiliation(vendor_id=vendor_id, vendor_id_odm=vendor_id_odm)
    for action in [
        "@delete",
        "@modify",
        "@undelete",
        "@modify-updateinfo",
        "@view",
        "@retry",
        "@waive",
    ]:
        aff.actions.append(VendorAffiliationAction(action=action, user=g.user))
    vendor.affiliations.append(aff)
    db.session.commit()
    flash("Added affiliation {}".format(aff.affiliation_id), "info")
    return redirect(url_for("vendors.route_affiliations", vendor_id=vendor_id), 302)


@bp_vendors.post("/<int:vendor_id>/affiliation/<int:affiliation_id>/delete")
@login_required
def route_affiliation_delete(vendor_id: int, affiliation_id: int) -> Any:
    """Allows changing a vendor [ADMIN ONLY]"""

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list"), 302)

    # security check
    if not vendor.check_acl("@modify-affiliations"):
        flash("Permission denied: Unable to delete vendor affiliations", "danger")
        return redirect(url_for("vendors.route_show", vendor_id=vendor_id))

    for res in vendor.affiliations:
        if res.affiliation_id == affiliation_id:
            db.session.delete(res)
            db.session.commit()
            break
    flash("Deleted affiliation", "info")
    return redirect(url_for("vendors.route_affiliations", vendor_id=vendor_id), 302)


@bp_vendors.route("/<int:vendor_id>/exports")
@login_required
def route_exports(vendor_id: int) -> Any:

    # check exists
    try:
        vendor = db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).one()
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that ID", "warning")
        return redirect(url_for("vendors.route_list"), 302)

    # security check
    if not vendor.check_acl("@view-exports"):
        flash("Permission denied: Unable to view exports", "danger")
        return redirect(url_for("vendors.route_show", vendor_id=vendor_id))

    # add other vendors
    vendors: List[Vendor] = []
    for v in db.session.query(Vendor).order_by(Vendor.display_name):
        if v.vendor_id == vendor_id:
            continue
        if not v.is_account_holder:
            continue
        if v.is_affiliate_for(vendor.vendor_id):
            continue
        vendors.append(v)
    return render_template(
        "vendor-exports.html", category="vendors", v=vendor, other_vendors=vendors
    )


def _convert_export_ids(v: Vendor) -> List[str]:
    if not v.banned_country_codes:
        return []
    return v.banned_country_codes.split(",")


@bp_vendors.post("/<int:vendor_id>/country/create")
@login_required
def route_export_create(vendor_id: int) -> Any:

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to add affiliate: No a vendor with that ID", "warning")
        return redirect(url_for("vendors.route_exports", vendor_id=vendor_id), 302)

    # security check
    if not vendor.check_acl("@modify-exports"):
        flash("Permission denied: Unable to add vendor country", "danger")
        return redirect(url_for("vendors.route_show", vendor_id=vendor_id))

    # check if it already exists
    try:
        export_id = request.form["export_id"].upper()
    except KeyError:
        flash("No value", "warning")
        return redirect(url_for("vendors.route_show", vendor_id=vendor_id))
    export_ids = _convert_export_ids(vendor)
    if export_id in export_ids:
        flash("Failed to add country: Already blocked %s" % export_id, "warning")
        return redirect(url_for("vendors.route_exports", vendor_id=vendor_id), 302)

    # add a new ODM -> OEM country
    export_ids.append(export_id)
    vendor.banned_country_codes = ",".join(export_ids)
    db.session.commit()
    flash("Added blocked country %s" % export_id, "info")
    return redirect(url_for("vendors.route_exports", vendor_id=vendor_id), 302)


@bp_vendors.post("/<int:vendor_id>/country/<export_id>/delete")
@login_required
def route_export_delete(vendor_id: int, export_id: str) -> Any:

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that ID", "warning")
        return redirect(url_for("vendors.route_list"), 302)

    # security check
    if not vendor.check_acl("@modify-exports"):
        flash("Permission denied: Unable to delete vendor exports", "danger")
        return redirect(url_for("vendors.route_show", vendor_id=vendor_id))

    export_ids = _convert_export_ids(vendor)
    if export_id not in export_ids:
        flash("Failed to remove country: Not blocked %s" % export_id, "warning")
        return redirect(url_for("vendors.route_exports", vendor_id=vendor_id), 302)
    export_ids.remove(export_id)
    vendor.banned_country_codes = ",".join(export_ids)
    db.session.commit()
    flash("Deleted blocked country %s" % export_id, "info")
    return redirect(url_for("vendors.route_exports", vendor_id=vendor_id), 302)


@bp_vendors.route("/<int:vendor_id>/assets")
@login_required
@admin_login_required
def route_assets(vendor_id: int) -> Any:
    """Shows all vendor assets [ADMIN ONLY]"""
    try:
        vendor = db.session.query(Vendor).filter(Vendor.vendor_id == vendor_id).one()
    except NoResultFound:
        flash("Failed to get vendor details: No a vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    return render_template("vendor-assets.html", category="vendors", v=vendor)


@bp_vendors.post("/<int:vendor_id>/asset/upload")
@login_required
@admin_login_required
def route_asset_upload(vendor_id: int) -> Any:
    """Allows uploading a vendor asset [ADMIN ONLY]"""

    # check exists
    try:
        vendor = (
            db.session.query(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .with_for_update(of=Vendor)
            .one()
        )
    except NoResultFound:
        flash("Failed to add asset: No vendor with that group ID", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)

    # read file
    try:
        fileitem = request.files["file"]
    except KeyError:
        flash("Failed to add asset: No file", "warning")
        return redirect(url_for("vendors.route_assets", vendor_id=vendor_id), 302)
    if not fileitem.filename:
        flash("Failed to add asset: No filename", "warning")
        return redirect(url_for("vendors.route_assets", vendor_id=vendor_id), 302)
    blob = fileitem.read()

    # write to EFS and save to database
    asset = VendorAsset(
        vendor=vendor,
        filename="{}-{}".format(hashlib.sha256(blob).hexdigest(), fileitem.filename),
        user=g.user,
    )
    if os.path.exists(asset.absolute_path):
        flash(
            "Failed to add asset: The file {} already exists".format(asset.filename),
            "warning",
        )
        return redirect(url_for("vendors.route_assets", vendor_id=vendor_id), 302)
    with open(asset.absolute_path, "wb") as f:
        f.write(blob)
    vendor.assets.append(asset)
    db.session.commit()

    # success
    flash("Added asset {}".format(asset.filename), "info")
    return redirect(url_for("vendors.route_assets", vendor_id=vendor_id), 302)


@bp_vendors.post("/<int:vendor_id>/asset/<int:vendor_asset_id>/delete")
@login_required
@admin_login_required
def route_asset_delete(vendor_id: int, vendor_asset_id: int) -> Any:
    """Allows deleting a vendor asset [ADMIN ONLY]"""

    # check exists
    try:
        asset = (
            db.session.query(VendorAsset)
            .filter(VendorAsset.vendor_asset_id == vendor_asset_id)
            .join(Vendor)
            .filter(Vendor.vendor_id == vendor_id)
            .one()
        )
    except NoResultFound:
        flash("Failed to delete asset: No asset with those IDs", "warning")
        return redirect(url_for("vendors.route_list_admin"), 302)
    os.remove(asset.absolute_path)
    db.session.delete(asset)
    db.session.commit()
    flash("Deleted asset", "info")
    return redirect(url_for("vendors.route_assets", vendor_id=vendor_id), 302)
