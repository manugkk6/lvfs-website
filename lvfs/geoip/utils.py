#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=unused-argument

import gzip
import os
import zipfile
import io
import csv
from typing import Dict
from collections import defaultdict
from io import StringIO
from flask import current_app as app

import requests

from lvfs import db

from lvfs.tasks.models import Task

from .models import Geoip


def _convert_ip_addr_to_integer(ip_addr: str) -> int:
    try:
        ip_vals = ip_addr.split(".")
        return (
            int(ip_vals[0]) * 0x1000000
            + int(ip_vals[1]) * 0x10000
            + int(ip_vals[2]) * 0x100
            + int(ip_vals[3])
        )
    except (IndexError, ValueError):
        return 0x0


def task_import_url(task: Task) -> None:

    # download URL
    session = requests.Session()
    basename = os.path.basename(app.config["GEOIP_URL"])
    try:
        rv = session.get(app.config["GEOIP_URL"])
        payload: bytes = rv.content
    except requests.exceptions.RequestException as e:
        task.add_fail(
            "Failed to download from {}".format(app.config["GEOIP_URL"]), str(e)
        )
        return
    if rv.status_code != 200:
        task.add_fail("Failed to download from {}".format(rv.url), rv.text)
        return

    # parse CSV zipped data
    if basename.endswith(".ZIP"):
        zfile = io.BytesIO(payload)
        with zipfile.ZipFile(zfile, "r") as zip_ref:
            payload = zip_ref.read(basename.replace(".ZIP", ""))

    # parse CSV gzipped data
    if basename.endswith(".gz"):
        try:
            payload = gzip.decompress(payload)
        except (OSError, UnicodeDecodeError) as e:
            task.add_fail("No GZipped object could be decoded", str(e))
            return

    # find the last added Geoip ID
    try:
        (last_id,) = (
            db.session.query(Geoip.geoip_id).order_by(Geoip.geoip_id.desc()).first()
        )
    except TypeError:
        last_id = 0

    # import the new data
    task.add_pass("Downloaded payload of {} bytes".format(len(payload)))
    try:
        reader = csv.reader(StringIO(payload.decode(errors="ignore")))
    except ValueError as e:
        task.add_fail("No CSV object could be decoded", str(e))
        return
    countries: Dict[str, int] = defaultdict(int)
    for row in reader:
        try:
            if row[0].startswith("#"):
                continue
            geo = Geoip(
                addr_start=int(row[0]), addr_end=int(row[1]), country_code=row[2]
            )
            countries[row[2]] += 1
            db.session.add(geo)
        except (IndexError, ValueError):
            pass

    # commit new IDs
    db.session.commit()
    task.add_pass("Added new rows")

    # remove old IDs
    db.session.query(Geoip).filter(Geoip.geoip_id <= last_id).delete()
    db.session.commit()

    # log
    task.add_pass("Imported GeoIP data: {}".format(str(countries)))
