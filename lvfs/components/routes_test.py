#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position,singleton-comparison

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_updateinfo(self, _app, client):

        # get the default update info from the firmware archive
        self.login()
        self.add_namespace()
        self.upload()
        rv = client.get("/lvfs/components/1/description")
        assert b"Work around the MCDC04 errata" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/components/1/update")
        assert b'value="low" selected' in rv.data, rv.data.decode()

        # edit the description and severity
        rv = client.post(
            "/lvfs/components/1/modify",
            data=dict(
                urgency="critical",
                description="Not enough cats!",
            ),
            follow_redirects=True,
        )
        assert b"Component updated" in rv.data, rv.data.decode()

        # verify the new update info
        rv = client.get("/lvfs/components/1/description")
        assert b"Not enough cats" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/components/1/update")
        assert b'value="critical" selected' in rv.data, rv.data.decode()

    def test_name_variant_suffix(self, _app, client):

        # get the default update info from the firmware archive
        self.login()
        self.add_namespace()
        self.upload()

        # edit the name_variant_suffix to something contained in the <name>
        rv = client.post(
            "/lvfs/components/1/modify",
            data=dict(
                name_variant_suffix="Pre-Release ColorHug2",
            ),
            follow_redirects=True,
        )
        assert b"Component updated" in rv.data, rv.data.decode()

        # verify the new problems
        rv = client.get("/lvfs/firmware/1/problems")
        assert b"ColorHug2 is already part" in rv.data, rv.data.decode()

        # edit the name_variant_suffix
        rv = client.post(
            "/lvfs/components/1/modify",
            data=dict(
                name_variant_suffix="Pre-Release",
            ),
            follow_redirects=True,
        )
        assert b"Component updated" in rv.data, rv.data.decode()

        # verify the new problems
        rv = client.get("/lvfs/firmware/1/problems")
        assert b"ColorHug2 is already part" not in rv.data, rv.data.decode()

    def test_branch(self, _app, client):

        # get the default update info from the firmware archive
        self.login()
        self.add_namespace()
        self.upload()

        # edit the branch to something the vendor can't have
        rv = client.post(
            "/lvfs/components/1/modify",
            data=dict(
                branch="dave",
                name_variant_suffix="Pre-Release ColorHug2",
            ),
            follow_redirects=True,
        )
        assert b"Component updated" in rv.data, rv.data.decode()

        # edit the branch to something the vendor can't have
        rv = client.get("/lvfs/components/1")
        assert b"dave" in rv.data, rv.data.decode()

        # verify the new problems
        rv = client.get("/lvfs/firmware/1/problems")
        assert b"cannot ship firmware with branches" in rv.data, rv.data.decode()
        assert b"requirement of org.freedesktop.fwupd" in rv.data, rv.data.decode()

        # allow vendor a branch, but the "wrong one"
        rv = client.post(
            "/lvfs/vendors/1/branch/create",
            data=dict(value="oss-firmware"),
            follow_redirects=True,
        )
        assert b"Added branch" in rv.data, rv.data.decode()

        # verify the new problems
        rv = client.get("/lvfs/firmware/1/problems")
        assert b"only allowed to use branches" in rv.data, rv.data.decode()
        assert b"requirement of org.freedesktop.fwupd" in rv.data, rv.data.decode()

        # edit the branch
        rv = client.post(
            "/lvfs/components/1/modify",
            data=dict(
                branch="oss-firmware",
            ),
            follow_redirects=True,
        )
        assert b"Component updated" in rv.data, rv.data.decode()

        # verify the new problems
        rv = client.get("/lvfs/firmware/1/problems")
        assert b"branches" not in rv.data, rv.data.decode()

    def test_vendor_in_name(self, _app, client):

        # get the default update info from the firmware archive
        self.login()
        self.add_namespace()
        self.upload()

        # edit the name_variant_suffix to something contained in the <name>
        rv = client.post(
            "/lvfs/components/1/modify",
            data=dict(
                name="Acme ColorHug2",
            ),
            follow_redirects=True,
        )
        assert b"Component updated" in rv.data, rv.data.decode()

        # verify the new problems
        rv = client.get("/lvfs/firmware/1/problems")
        assert b"The vendor should not be part of the name" in rv.data, rv.data.decode()

    def test_requires(self, _app, client):

        # check existing requires were added
        self.login()
        self.add_namespace()
        self.upload()

        # check requirements were copied out from the .metainfo.xml file
        rv = client.get("/lvfs/components/1/requires")
        assert b"85d38fda-fc0e-5c6f-808f-076984ae7978" in rv.data, rv.data.decode()
        assert b'name="version" value="1.0.3' in rv.data, rv.data.decode()
        assert b'ge" selected' in rv.data, rv.data.decode()
        assert b'regex" selected' in rv.data, rv.data.decode()
        assert b"BOT03.0[2-9]_*" in rv.data, rv.data.decode()

        # remove the CHID requirement
        rv = client.post(
            "/lvfs/components/1/requirement/delete/3", follow_redirects=True
        )
        assert (
            b"Removed requirement 85d38fda-fc0e-5c6f-808f-076984ae7978" in rv.data
        ), rv.data

        # add an invalid CHID
        rv = client.post(
            "/lvfs/components/1/requirement/modify",
            data=dict(
                kind="hardware",
                value="NOVALIDGUID",
            ),
            follow_redirects=True,
        )
        assert b"NOVALIDGUID is not a valid GUID" in rv.data, rv.data.decode()

        # add an invalid AppStream ID
        rv = client.post(
            "/lvfs/components/1/requirement/modify",
            data=dict(
                kind="id",
                value="NOVALID_ID",
                hardness="",
            ),
            follow_redirects=True,
        )
        assert b"NOVALID_ID is not a valid AppStream ID" in rv.data, rv.data.decode()

        # add a valid CHID
        rv = client.post(
            "/lvfs/components/1/requirement/modify",
            data=dict(
                kind="hardware",
                value="85d38fda-fc0e-5c6f-808f-076984ae7978",
                hardness="",
            ),
            follow_redirects=True,
        )
        assert b"85d38fda-fc0e-5c6f-808f-076984ae7978" in rv.data, rv.data.decode()
        assert b"Added requirement" in rv.data, rv.data.decode()

        # modify an existing requirement by adding it again
        rv = client.post(
            "/lvfs/components/1/requirement/modify",
            data=dict(
                kind="id",
                value="org.freedesktop.fwupd",
                compare="ge",
                version="1.0.4",
                hardness="",
            ),
            follow_redirects=True,
        )
        assert b'name="version" value="1.0.4' in rv.data, rv.data.decode()
        assert b"Modified requirement" in rv.data, rv.data.decode()

        # delete a requirement by adding an 'any' comparison
        rv = client.post(
            "/lvfs/components/1/requirement/modify",
            data=dict(
                kind="id",
                value="org.freedesktop.fwupd",
                compare="any",
                version="1.0.4",
                hardness="",
            ),
            follow_redirects=True,
        )
        assert b'name="version" value="1.0.4' not in rv.data, rv.data.decode()
        assert b"Deleted requirement" in rv.data, rv.data.decode()

    def test_guids(self, _app, client):

        # check GUID shows up
        self.login()
        self.add_namespace()
        self.upload()
        rv = client.get("/lvfs/components/1/guids")
        assert b"2082b5e0-7a64-478a-b1b2-e3404fab6dad" in rv.data, rv.data.decode()

        # remove the GUID
        rv = client.post("/lvfs/components/1/guid/delete/1", follow_redirects=True)
        assert (
            b"Removed GUID 2082b5e0-7a64-478a-b1b2-e3404fab6dad" in rv.data
        ), rv.data.decode()

        # add an invalid GUID
        rv = client.post(
            "/lvfs/components/1/guid/create",
            data=dict(
                value="NOVALIDGUID",
            ),
            follow_redirects=True,
        )
        assert b"NOVALIDGUID is not valid" in rv.data, rv.data.decode()

        # add new valid GUID
        rv = client.post(
            "/lvfs/components/1/guid/create",
            data=dict(
                value="85d38fda-fc0e-5c6f-808f-076984ae7978",
            ),
            follow_redirects=True,
        )
        assert b"85d38fda-fc0e-5c6f-808f-076984ae7978" in rv.data, rv.data.decode()
        assert b"Added GUID" in rv.data, rv.data.decode()

        # add duplicate valid GUID
        rv = client.post(
            "/lvfs/components/1/guid/create",
            data=dict(
                value="85d38fda-fc0e-5c6f-808f-076984ae7978",
            ),
            follow_redirects=True,
        )
        assert b"85d38fda-fc0e-5c6f-808f-076984ae7978" in rv.data, rv.data.decode()
        assert b"GUID already added" in rv.data, rv.data.decode()

    def test_tags(self, _app, client):

        # check tag shows up
        self.login()
        self.add_namespace()
        self.upload()
        rv = client.get("/lvfs/components/1/tags")
        assert b"2021q3" not in rv.data, rv.data.decode()

        # add an invalid tag
        rv = client.post(
            "/lvfs/components/1/tag/create",
            data=dict(
                value="NOTVALID",
            ),
            follow_redirects=True,
        )
        assert b"NOTVALID is not valid" in rv.data, rv.data.decode()

        # add new valid tag
        rv = client.post(
            "/lvfs/components/1/tag/create",
            data=dict(
                value="2021q3",
            ),
            follow_redirects=True,
        )
        assert b"2021q3" in rv.data, rv.data.decode()
        assert b"Added tag" in rv.data, rv.data.decode()

        # add duplicate valid tag
        rv = client.post(
            "/lvfs/components/1/tag/create",
            data=dict(
                value="2021q3",
            ),
            follow_redirects=True,
        )
        assert b"2021q3" in rv.data, rv.data.decode()
        assert b"tag already added" in rv.data, rv.data.decode()

    def test_keywords(self, _app, client):

        # upload file with keywords
        self.login()
        self.add_namespace()
        self.upload()

        # check keywords were copied out from the .metainfo.xml file
        rv = client.get("/lvfs/components/1/keywords")
        assert b">alice<" in rv.data, rv.data.decode()
        assert b">bob<" in rv.data, rv.data.decode()
        assert b">admin<" in rv.data, rv.data.decode()
        assert b">acme<" in rv.data, rv.data.decode()

        # add another set of keywords
        rv = client.post(
            "/lvfs/components/1/keyword/create",
            data=dict(
                value="Clara Dave",
            ),
            follow_redirects=True,
        )
        assert b"Added keywords" in rv.data, rv.data.decode()
        assert b">clara<" in rv.data, rv.data.decode()
        assert b">dave<" in rv.data, rv.data.decode()

        # delete one of the added keywords
        rv = client.post("/lvfs/components/1/keyword/3/delete", follow_redirects=True)
        assert b"Removed keyword" in rv.data, rv.data.decode()
        assert b">alice<" in rv.data, rv.data.decode()
        assert b">colorimeter<" not in rv.data, rv.data.decode()

    def test_issues(self, _app, client):

        # upload file with issues
        self.login()
        self.add_namespace()
        self.upload(filename="contrib/intelme.cab", target="private")

        # check CVEs were copied out from the .metainfo.xml file
        rv = client.get("/lvfs/components/1/issues")
        assert b"CVE-2016" in rv.data, rv.data.decode()
        assert b"CVE-2017" in rv.data, rv.data.decode()

        # add another set of CVEs
        rv = client.post(
            "/lvfs/components/1/issue/create",
            data=dict(
                value="DSA-2018-000,INTEL-SA-00000,LEN-99999,VU#123456",
            ),
            follow_redirects=True,
        )
        assert b"Added DSA-" in rv.data, rv.data.decode()
        assert b"CVE-2016" in rv.data, rv.data.decode()
        assert b"CVE-2017" in rv.data, rv.data.decode()
        assert b"DSA-2018-000" in rv.data, rv.data.decode()
        assert b"INTEL-SA-00000" in rv.data, rv.data.decode()
        assert b"LEN-99999" in rv.data, rv.data.decode()
        assert b"VU#123456" in rv.data, rv.data.decode()

        # delete one of the added CVEs
        rv = client.post("/lvfs/components/1/issue/3/delete", follow_redirects=True)
        assert b"Removed DSA-2018-000" in rv.data, rv.data.decode()
        assert b"CVE-2017" in rv.data, rv.data.decode()

        # update the description to include CVEs
        rv = client.post(
            "/lvfs/components/1/modify",
            data=dict(
                urgency="critical",
                description="- Address security advisories INTEL-SA-00233(CVE-2018-12126, "
                "LEN-54321, CVE-2018-12127, LEN-12345, VU#654321)\n"
                "- Firmware updates to address security advisory INTEL-SA-00213",
            ),
            follow_redirects=True,
        )
        assert b"Component updated" in rv.data, rv.data.decode()

        # check there is a problem
        rv = client.get("/lvfs/firmware/1/problems")
        assert b"CVEs in update description" in rv.data, rv.data.decode()

        # autoimport the CVEs
        rv = client.post("/lvfs/components/1/issue/autoimport", follow_redirects=True)
        assert b"Added 7 issues" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/firmware/1/problems")
        assert b"CVEs in update description" not in rv.data, rv.data.decode()
        rv = client.get("/lvfs/components/1/description")
        assert b"- Address security advisories" in rv.data, rv.data.decode()
        assert b"REMOVE_ME" in rv.data, rv.data.decode()
        assert (
            b"- Firmware updates to address security advisory" in rv.data
        ), rv.data.decode()

    def test_device_checksums(self, _app, client):

        # upload file with keywords
        self.login()
        self.add_namespace()
        self.upload()

        # add invalid checksums
        rv = client.post(
            "/lvfs/components/1/checksum/create",
            data=dict(
                value="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
            ),
            follow_redirects=True,
        )
        assert b"is not a recognized SHA1 or SHA256 hash" in rv.data, rv.data.decode()
        rv = client.post(
            "/lvfs/components/1/checksum/create",
            data=dict(
                value="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
            ),
            follow_redirects=True,
        )
        assert b"is not a recognized SHA1 or SHA256 hash" in rv.data, rv.data.decode()

        # add a SHA256 checksum
        rv = client.post(
            "/lvfs/components/1/checksum/create",
            data=dict(
                value="9d72ffd950d3bedcda99a197d760457e90f3d6f2a62b30b95a488511f0dfa4ad",
            ),
            follow_redirects=True,
        )
        assert b"Added device checksum" in rv.data, rv.data.decode()
        assert (
            b"9d72ffd950d3bedcda99a197d760457e90f3d6f2a62b30b95a488511f0dfa4ad"
            in rv.data
        ), rv.data

        # add the same checksum again
        rv = client.post(
            "/lvfs/components/1/checksum/create",
            data=dict(
                value="9d72ffd950d3bedcda99a197d760457e90f3d6f2a62b30b95a488511f0dfa4ad",
            ),
            follow_redirects=True,
        )
        assert b"has already been added" in rv.data, rv.data.decode()

        # add a SHA1 checksum
        rv = client.post(
            "/lvfs/components/1/checksum/create",
            data=dict(
                value="fb6439cbda2add6c394f71b7cf955dd9a276ca5a",
            ),
            follow_redirects=True,
        )
        assert b"Added device checksum" in rv.data, rv.data.decode()
        assert b"fb6439cbda2add6c394f71b7cf955dd9a276ca5a" in rv.data, rv.data.decode()

        # delete the checksum
        rv = client.post("/lvfs/components/1/checksum/delete/1", follow_redirects=True)
        assert b"Removed device checksum" in rv.data, rv.data.decode()
        assert (
            b"9d72ffd950d3bedcda99a197d760457e90f3d6f2a62b30b95a488511f0dfa4ad"
            not in rv.data
        ), rv.data


if __name__ == "__main__":
    unittest.main()
