#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from typing import Any

from flask import Blueprint, request, url_for, redirect, flash, render_template
from flask_login import login_required
from sqlalchemy.exc import NoResultFound, IntegrityError

from lvfs import db

from lvfs.protocols.models import Protocol
from lvfs.util import admin_login_required
from lvfs.util import DEVICE_ICONS
from lvfs.verfmts.models import Verfmt

bp_protocols = Blueprint("protocols", __name__, template_folder="templates")


@bp_protocols.route("/")
@login_required
@admin_login_required
def route_list() -> Any:

    # only show protocols with the correct group_id
    protocols = db.session.query(Protocol).order_by(Protocol.name.asc()).all()
    return render_template("protocol-list.html", category="admin", protocols=protocols)


@bp_protocols.post("/create")
@login_required
@admin_login_required
def route_create() -> Any:

    # ensure has enough data
    try:
        value = request.form["value"]
    except KeyError:
        flash("No form data found!", "warning")
        return redirect(url_for("protocols.route_list"))
    if not value or not value.islower() or value.find(" ") != -1:
        flash("Failed to add protocol: Value needs to be a lower case word", "warning")
        return redirect(url_for("protocols.route_list"))

    # add protocol
    try:
        protocol = Protocol(value=request.form["value"], is_public=True)
        db.session.add(protocol)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add protocol: The protocol already exists", "info")
        return redirect(url_for("protocols.route_list"))
    flash("Added protocol", "info")
    return redirect(url_for("protocols.route_show", protocol_id=protocol.protocol_id))


@bp_protocols.post("/<int:protocol_id>/delete")
@login_required
@admin_login_required
def route_delete(protocol_id: int) -> Any:

    # get protocol
    try:
        db.session.query(Protocol).filter(Protocol.protocol_id == protocol_id).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Cannot delete", "danger")
        return redirect(url_for("protocols.route_list"))
    flash("Deleted protocol", "info")
    return redirect(url_for("protocols.route_list"))


@bp_protocols.post("/<int:protocol_id>/modify")
@login_required
@admin_login_required
def route_modify(protocol_id: int) -> Any:

    # find protocol
    try:
        protocol = (
            db.session.query(Protocol)
            .filter(Protocol.protocol_id == protocol_id)
            .with_for_update(of=Protocol)
            .one()
        )
    except NoResultFound:
        flash("No protocol found", "info")
        return redirect(url_for("protocols.route_list"))

    # modify protocol
    protocol.is_signed = bool("is_signed" in request.form)
    protocol.is_transport = bool("is_transport" in request.form)
    protocol.is_public = bool("is_public" in request.form)
    protocol.can_verify = bool("can_verify" in request.form)
    protocol.has_header = bool("has_header" in request.form)
    protocol.require_report = bool("require_report" in request.form)
    protocol.allow_custom_update_message = bool(
        "allow_custom_update_message" in request.form
    )
    protocol.allow_device_category = bool("allow_device_category" in request.form)
    for key in [
        "value",
        "name",
        "icon",
        "update_request_id",
        "update_message",
        "update_image",
    ]:
        if key in request.form:
            setattr(protocol, key, request.form[key] or None)
    if "verfmt_id" in request.form:
        protocol.verfmt = (
            db.session.query(Verfmt)
            .filter(Verfmt.verfmt_id == request.form["verfmt_id"])
            .first()
        )
    db.session.commit()

    # success
    flash("Modified protocol", "info")
    return redirect(url_for("protocols.route_show", protocol_id=protocol_id))


@bp_protocols.route("/<int:protocol_id>/details")
@login_required
@admin_login_required
def route_show(protocol_id: int) -> Any:

    # find protocol
    try:
        protocol = (
            db.session.query(Protocol).filter(Protocol.protocol_id == protocol_id).one()
        )
    except NoResultFound:
        flash("No protocol found", "info")
        return redirect(url_for("protocols.route_list"))

    # show details
    verfmts = db.session.query(Verfmt).order_by(Verfmt.verfmt_id.asc()).all()
    return render_template(
        "protocol-details.html",
        category="admin",
        protocol=protocol,
        icons=DEVICE_ICONS,
        verfmts=verfmts,
    )
