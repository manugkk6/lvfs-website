#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position,singleton-comparison

import os
import sys
import unittest
from io import BytesIO

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_translations(self, _app, client):

        self.login()
        self.upload()
        rv = client.get("/lvfs/translations/")
        assert "pl_PL" not in rv.data.decode("utf-8"), rv.data.decode()

        # edit the description and severity
        rv = client.post(
            "/lvfs/components/1/modify",
            data=dict(
                description="Za mało kotów!",
                locale="pl_PL",
            ),
            follow_redirects=True,
        )
        assert b"Component updated" in rv.data, rv.data.decode()

        # ensure it exists
        rv = client.get("/lvfs/translations/")
        assert "pl_PL" in rv.data.decode("utf-8"), rv.data.decode()

        # get the built XLIFF file
        rv = client.get("/lvfs/translations/locale/pl_PL")
        assert "pl-PL" in rv.data.decode("utf-8"), rv.data.decode()
        assert "kotów" in rv.data.decode("utf-8"), rv.data.decode()

        # modify a translation
        new = """<?xml version='1.0' encoding='utf-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:2.0" version="2.0" srcLang="en-US" trgLang="pl-PL">
  <file id="1" vendor="admin" product="com.hughski.ColorHug2.firmware">
    <unit id="0">
      <segment>
        <source>This stable release fixes the following problems:

 * Use a quicker start-up sequence to fix flashing on Windows
 * Work around the MCDC04 errata when doing XYZ and non-XYZ measurements</source>
        <target>ZA MAŁO KOTÓW!</target>
      </segment>
    </unit>
  </file>
</xliff>
        """
        with BytesIO(new.encode()) as fd:
            data = {"file": (fd, "pl_PL.xliff")}
            rv = client.post(
                "/lvfs/translations/upload", data=data, follow_redirects=True
            )
            assert "Modified 1 translations" in rv.data.decode(
                "utf-8"
            ), rv.data.decode()
        rv = client.get("/lvfs/translations/locale/pl_PL")
        assert "ZA MAŁO KOTÓW" in rv.data.decode("utf-8"), rv.data.decode()

        # upload a new translation
        new = """<?xml version='1.0' encoding='utf-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:2.0" version="2.0" srcLang="en-US" trgLang="fr-FR">
  <file id="1" vendor="admin" product="com.hughski.ColorHug2.firmware">
    <unit id="0">
      <segment>
        <source>This stable release fixes the following problems:

 * Use a quicker start-up sequence to fix flashing on Windows
 * Work around the MCDC04 errata when doing XYZ and non-XYZ measurements</source>
        <target>TROP DE CHATS!</target>
      </segment>
    </unit>
  </file>
</xliff>
        """
        with BytesIO(new.encode()) as fd:
            data = {"file": (fd, "fr_FR.xliff")}
            rv = client.post(
                "/lvfs/translations/upload", data=data, follow_redirects=True
            )
            assert "Uploaded 1 new translations" in rv.data.decode(
                "utf-8"
            ), rv.data.decode()
        rv = client.get("/lvfs/translations/locale/fr_FR")
        assert "TROP DE CHATS" in rv.data.decode("utf-8"), rv.data.decode()

        # upload an invalid translation
        new = """<?xml version='1.0' encoding='utf-8'?>
<xliff xmlns="urn:oasis:names:tc:xliff:document:2.0" version="2.0" srcLang="en-US" trgLang="fr-FR">
  <file id="1" vendor="admin" product="com.hughski.ColorHug2.firmware">
    <unit id="0">
      <segment>
        <source>NOT GOING TO EXIST</source>
        <target>TROP DE CHATS!</target>
      </segment>
    </unit>
  </file>
</xliff>
        """
        with BytesIO(new.encode()) as fd:
            data = {"file": (fd, "fr_FR.xliff")}
            rv = client.post(
                "/lvfs/translations/upload", data=data, follow_redirects=True
            )
            assert "Cannot change translation source" in rv.data.decode(
                "utf-8"
            ), rv.data.decode()


if __name__ == "__main__":
    unittest.main()
