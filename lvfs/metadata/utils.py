#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-many-statements,too-many-locals,too-many-nested-blocks,deprecated-module
# pylint: disable=singleton-comparison,unused-argument

from enum import IntEnum

import os
import gzip
import lzma
import glob
import datetime
import functools
import hashlib

from collections import defaultdict
from typing import List, Optional, Dict
from distutils.version import StrictVersion
from lxml import etree as ET
from flask import current_app as app
from sqlalchemy.orm import joinedload

from pkgversion import vercmp

from jcat import JcatFile, JcatBlobSha1, JcatBlobSha256, JcatBlobKind

from lvfs import db, ploader

from lvfs.components.models import Component, ComponentRequirement
from lvfs.devices.models import Product
from lvfs.firmware.models import Firmware, FirmwareRevision
from lvfs.util import _get_settings, _xml_from_markdown
from lvfs.verfmts.models import Verfmt
from lvfs.tasks.models import Task

from .models import Remote


class PulpManifest:
    def __init__(self) -> None:
        self._lines: List[str] = []

    def add_file(
        self, filename: str, checksum: Optional[str] = None, size: Optional[int] = None
    ) -> None:

        if not checksum:
            with open(filename, "rb") as f:
                checksum = hashlib.sha256(f.read()).hexdigest()
        if not size:
            size = os.path.getsize(filename)
        self._lines.append(
            "{},{},{}".format(os.path.basename(filename), checksum, size)
        )

    def add_fw(self, fw: Firmware) -> None:

        # add archive
        self.add_file(
            filename=fw.revisions[0].filename,
            checksum=fw.checksum_signed_sha256,
            size=fw.mds[0].release_download_size,
        )

        # add update images
        for md in fw.mds:
            for url in [md.screenshot_url_safe, md.release_image_safe]:
                if not url:
                    continue
                fn = os.path.join(app.config["DOWNLOAD_DIR"], os.path.basename(url))
                if os.path.isfile(fn):
                    self.add_file(fn)

    def export(self) -> str:
        return "\n".join(self._lines) + "\n"


class MetadataExportKind(IntEnum):
    APPSTREAM = 0
    METAINFO = 1
    EXPORT = 2


def _is_verfmt_supported_by_fwupd(md: Component, verfmt: Verfmt) -> bool:

    # fwupd version no specified
    if not verfmt.fwupd_version:
        return False

    # did the firmware specify >= a fwupd version
    req = md.find_req("id", "org.freedesktop.fwupd")
    if not req:
        return False
    if req.compare != "ge":
        return False

    # compare the version number for the protocol and the requirement
    try:
        if StrictVersion(req.version) >= StrictVersion(verfmt.fwupd_version):
            return True
    except ValueError:
        pass

    # failed
    return False


def _use_hex_release_version(md: Component) -> bool:
    if not md.version.isdigit():
        return False
    if not md.verfmt or md.verfmt.value in ["plain", "number"]:
        return False
    return True


def _key_for_req_dedupe(req: ComponentRequirement) -> str:
    return "{}/{}/{}/{}".format(req.kind, req.value, req.compare, req.depth)


def _is_unique_requirement(
    rqs: List[ComponentRequirement], rq: ComponentRequirement
) -> bool:

    for rq_old in rqs:
        if (
            rq.compare == "ge"
            and rq_old.compare == "ge"
            and vercmp(rq_old.version, rq.version) > 0
        ):
            return False
        if (
            rq.compare == "le"
            and rq_old.compare == "le"
            and vercmp(rq_old.version, rq.version) < 0
        ):
            return False
    return True


def _get_unique_requirements(
    mds: List[Component], kind: str, hardness: Optional[str] = None
) -> List[ComponentRequirement]:

    rqs: Dict[str, ComponentRequirement] = {}
    for md in mds:
        for rq in md.requirements:
            if rq.hardness != hardness:
                continue
            if rq.kind != kind:
                continue
            if _is_unique_requirement(list(rqs.values()), rq):
                rqs[_key_for_req_dedupe(rq)] = rq
    return list(rqs.values())


def _generate_metadata_mds(
    mds: List[Component],
    firmware_baseuri: str = "",
    local: bool = False,
    export_kind: MetadataExportKind = MetadataExportKind.APPSTREAM,
    allow_unrestricted: bool = True,
    include_testing: bool = True,
    use_artifact: bool = True,
) -> ET.Element:

    # assume all the components have the same parent firmware information
    md = mds[0]
    component = ET.Element("component")
    component.set("type", md.appstream_type or "firmware")
    if md.appstream_id:
        ET.SubElement(component, "id").text = md.appstream_id
    if md.branch:
        ET.SubElement(component, "branch").text = md.branch

    # mark EOL as required
    product = md.product
    if product and product.state == Product.STATE_EOL:
        component.set("date_eol", "{:%Y-%m-%d}".format(product.mtime))

    # fwupd versions >= 1.3.2 will show name_variant_suffix properly */
    if md.name:
        ET.SubElement(component, "name").text = md.name
    if md.name_variant_suffix:
        ET.SubElement(component, "name_variant_suffix").text = md.name_variant_suffix
    if md.summary:
        ET.SubElement(component, "summary").text = md.summary
    if export_kind in [MetadataExportKind.METAINFO, MetadataExportKind.EXPORT]:
        if md.description:
            component.append(_xml_from_markdown(md.description))
    for md in mds:
        if md.priority:
            component.set("priority", str(md.priority))

    # provides shared by all releases
    provides: Dict[str, str] = {}
    for md in mds:
        for guid in md.guids:
            if guid.value in provides:
                continue
            child = ET.Element("firmware")
            child.set("type", "flashed")
            child.text = guid.value
            provides[guid.value] = child
    if provides:
        parent = ET.SubElement(component, "provides")
        for key in sorted(provides):
            parent.append(provides[key])

    # tags shared by all releases
    tags: List[str] = []
    for md in mds:
        tags.extend(md.tag_values)
    if tags:
        parent = ET.SubElement(component, "tags")
        for tag in sorted(tags):
            child = ET.SubElement(parent, "tag")
            child.set("namespace", "lvfs")
            child.text = tag

    # shared again
    if md.url_homepage:
        child = ET.SubElement(component, "url")
        child.set("type", "homepage")
        child.text = md.url_homepage
    if md.icon:
        child = ET.SubElement(component, "icon")
        child.set("type", "stock")
        child.text = md.icon
    if export_kind in [MetadataExportKind.METAINFO, MetadataExportKind.EXPORT]:
        if md.metadata_license:
            ET.SubElement(
                component, "metadata_license"
            ).text = md.metadata_license.value
    if md.project_license:
        ET.SubElement(component, "project_license").text = md.project_license.value
    if export_kind == MetadataExportKind.APPSTREAM and md.fw.vendor:
        ET.SubElement(component, "developer_name").text = md.fw.vendor.display_name

    # screenshot shared by all releases
    screenshots: Dict[str, ET.Element] = {}
    for md in mds:
        if not md.screenshot_url and not md.screenshot_caption:
            continue
        # try to dedupe using the URL and then the caption
        key = md.screenshot_url
        if not key:
            key = md.screenshot_caption
        if key not in screenshots:
            child = ET.Element("screenshot")
            if not screenshots:
                child.set("type", "default")
            if md.screenshot_caption:
                ET.SubElement(child, "caption").text = md.screenshot_caption
            if md.screenshot_url:
                if (
                    export_kind == MetadataExportKind.EXPORT
                    or not md.screenshot_url_safe
                ):
                    ET.SubElement(child, "image").text = md.screenshot_url
                else:
                    ET.SubElement(child, "image").text = md.screenshot_url_safe
            screenshots[key] = child
    if screenshots:
        parent = ET.SubElement(component, "screenshots")
        for screenshot in screenshots.values():
            parent.append(screenshot)

    # add enumerated categories
    cats: List[str] = []
    for md in mds:
        if not md.category:
            continue
        if md.category.value not in cats:
            cats.append(md.category.value)
        if md.category.fallback and md.category.fallback.value not in cats:
            cats.append(md.category.fallback.value)
    if cats:
        categories = ET.SubElement(component, "categories")
        for cat in cats:
            ET.SubElement(categories, "category").text = cat

    # metadata shared by all releases
    metadata: Dict[str, str] = {}
    for md in mds:
        if md.fw and md.fw.vendor and md.fw.vendor.is_community:
            metadata["LVFS::Distributor"] = "community"
        if md.fw and md.device_integrity:
            metadata["LVFS::DeviceIntegrity"] = md.device_integrity
        if md.inhibit_download:
            metadata["LVFS::InhibitDownload"] = ""
        if export_kind == MetadataExportKind.EXPORT:
            if md.release_message:
                metadata["LVFS::UpdateMessage"] = md.release_message
            if md.release_image_safe:
                metadata["LVFS::UpdateImage"] = md.release_image_safe
        elif export_kind == MetadataExportKind.METAINFO or (
            export_kind == MetadataExportKind.APPSTREAM
            and md.find_req("client", "update-action")
        ):
            if md.protocol:
                if md.protocol.update_request_id:
                    metadata["LVFS::UpdateRequestId"] = md.protocol.update_request_id
                if md.protocol.allow_custom_update_message and md.release_message:
                    metadata["LVFS::UpdateMessage"] = md.release_message
                elif md.protocol.update_message:
                    metadata["LVFS::UpdateMessage"] = md.protocol.update_message
                if md.protocol.allow_custom_update_message and md.release_image_safe:
                    metadata["LVFS::UpdateImage"] = md.release_image_safe
                elif md.protocol.allow_custom_update_message and md.release_image:
                    metadata["LVFS::UpdateImage"] = md.release_image
                elif md.protocol.update_image:
                    metadata["LVFS::UpdateImage"] = md.protocol.update_image
        if md.verfmt:
            if md.verfmt.fallbacks and not _is_verfmt_supported_by_fwupd(md, md.verfmt):
                # we need to support this for older fwupd versions
                metadata["LVFS::VersionFormat"] = "{}|{}".format(
                    "|".join(md.verfmt.fallbacks.split(",")), md.verfmt.value
                )
            else:
                metadata["LVFS::VersionFormat"] = md.verfmt.value
        if md.protocol:
            metadata["LVFS::UpdateProtocol"] = md.protocol.value
    if metadata:
        parent = ET.SubElement(component, "custom")
        for key, value in metadata.items():
            for value_item in value.split("|"):
                child = ET.Element("value")
                child.set("key", key)
                child.text = value_item or None
                parent.append(child)

    # add each release
    releases = None
    for md in mds:
        if not md.version:
            continue
        if releases is None:
            releases = ET.SubElement(component, "releases")
        rel = ET.SubElement(releases, "release")
        if export_kind == MetadataExportKind.APPSTREAM:
            rel.set("id", str(md.component_id))
        if md.version:
            if (
                export_kind != MetadataExportKind.APPSTREAM
                and _use_hex_release_version(md)
            ):
                rel.set("version", hex(int(md.version)))
            else:
                rel.set("version", md.version)
        if md.release_timestamp:
            if export_kind in [MetadataExportKind.METAINFO, MetadataExportKind.EXPORT]:
                rel.set(
                    "date",
                    datetime.date.fromtimestamp(md.release_timestamp).isoformat(),
                )
            else:
                rel.set("timestamp", str(md.release_timestamp))
        if md.release_urgency and md.release_urgency != "unknown":
            rel.set("urgency", md.release_urgency)
        if md.release_tag:
            rel.set("tag", md.release_tag)
        if export_kind == MetadataExportKind.APPSTREAM:
            ET.SubElement(rel, "location").text = (
                firmware_baseuri + md.fw.revisions[0].filename
            )

        # add container checksum
        if not use_artifact and export_kind == MetadataExportKind.APPSTREAM:
            if md.fw.checksum_signed_sha1 or local:
                csum = ET.SubElement(rel, "checksum")
                # metadata intended to be used locally won't be signed
                if local:
                    csum.text = md.fw.checksum_upload_sha1
                else:
                    csum.text = md.fw.checksum_signed_sha1
                csum.set("type", "sha1")
                csum.set("filename", md.fw.revisions[0].filename)
                csum.set("target", "container")
            if md.fw.checksum_signed_sha256 or local:
                csum = ET.SubElement(rel, "checksum")
                if local:
                    csum.text = md.fw.checksum_upload_sha256
                else:
                    csum.text = md.fw.checksum_signed_sha256
                csum.set("type", "sha256")
                csum.set("filename", md.fw.revisions[0].filename)
                csum.set("target", "container")

        # add content checksum
        if export_kind in [MetadataExportKind.METAINFO, MetadataExportKind.APPSTREAM]:
            if md.checksum_contents_sha1:
                csum = ET.SubElement(rel, "checksum")
                csum.text = md.checksum_contents_sha1
                csum.set("type", "sha1")
                csum.set("filename", md.filename_contents)
                csum.set("target", "content")
            if md.checksum_contents_sha256:
                csum = ET.SubElement(rel, "checksum")
                csum.text = md.checksum_contents_sha256
                csum.set("type", "sha256")
                csum.set("filename", md.filename_contents)
                csum.set("target", "content")
        else:
            csum = ET.SubElement(rel, "checksum")
            csum.set("filename", md.filename_contents)
            csum.set("target", "content")

        # add all device checksums
        for csum in md.device_checksums:
            n_csum = ET.SubElement(rel, "checksum")
            n_csum.text = csum.value
            n_csum.set("type", csum.kind.lower())
            n_csum.set("target", "device")

        # add long description
        for tx in sorted(md.descriptions):
            n_desc = _xml_from_markdown(tx.value)
            if n_desc is None:
                continue
            if tx.locale:
                n_desc.set("{http://www.w3.org/XML/1998/namespace}lang", tx.locale)
            rel.append(n_desc)

        # add details URL if set
        if md.details_url_with_fallback:
            child = ET.SubElement(rel, "url")
            child.set("type", "details")
            child.text = md.details_url_with_fallback

        # add source URL if set
        if md.source_url:
            child = ET.SubElement(rel, "url")
            child.set("type", "source")
            child.text = md.source_url

        # add sizes if set
        if not use_artifact and export_kind in [
            MetadataExportKind.METAINFO,
            MetadataExportKind.APPSTREAM,
        ]:
            if md.release_installed_size:
                sz = ET.SubElement(rel, "size")
                sz.set("type", "installed")
                sz.text = str(md.release_installed_size)
            if export_kind == MetadataExportKind.APPSTREAM and md.release_download_size:
                sz = ET.SubElement(rel, "size")
                sz.set("type", "download")
                sz.text = str(md.release_download_size)

        # add artifact to be AppStream compatible
        if use_artifact and export_kind in [
            MetadataExportKind.METAINFO,
            MetadataExportKind.APPSTREAM,
        ]:
            artifacts = ET.SubElement(rel, "artifacts")
            artifact = ET.SubElement(artifacts, "artifact")

            if export_kind == MetadataExportKind.METAINFO:
                # file in the archive
                artifact.set("type", "source")
                ET.SubElement(artifact, "filename").text = md.filename_contents

                # add contents checksums
                if md.checksum_contents_sha1 or local:
                    csum = ET.SubElement(artifact, "checksum")
                    csum.text = md.checksum_contents_sha1
                    csum.set("type", "sha1")
                if md.checksum_contents_sha256:
                    csum = ET.SubElement(artifact, "checksum")
                    csum.text = md.checksum_contents_sha256
                    csum.set("type", "sha256")

            else:

                # HTTP download location
                artifact.set("type", "binary")
                ET.SubElement(artifact, "location").text = (
                    firmware_baseuri + md.fw.revisions[0].filename
                )

                # add filename for local cache
                try:
                    ET.SubElement(artifact, "filename").text = md.fw.original_filename
                except IndexError:
                    ET.SubElement(artifact, "filename").text = md.fw.revisions[
                        0
                    ].filename

                # add container checksum
                if md.fw.checksum_signed_sha1 or local:
                    csum = ET.SubElement(artifact, "checksum")
                    if local:
                        csum.text = md.fw.checksum_upload_sha1
                    else:
                        csum.text = md.fw.checksum_signed_sha1
                    csum.set("type", "sha1")
                if md.fw.checksum_signed_sha256 or local:
                    csum = ET.SubElement(artifact, "checksum")
                    if local:
                        csum.text = md.fw.checksum_upload_sha256
                    else:
                        csum.text = md.fw.checksum_signed_sha256
                    csum.set("type", "sha256")

                # add sizes if set
                if md.release_installed_size:
                    sz = ET.SubElement(artifact, "size")
                    sz.set("type", "installed")
                    sz.text = str(md.release_installed_size)
                if md.release_download_size:
                    sz = ET.SubElement(artifact, "size")
                    sz.set("type", "download")
                    sz.text = str(md.release_download_size)

        # add issues
        if md.issues:
            issues = ET.SubElement(rel, "issues")
            for issue in md.issues:
                category = ET.SubElement(issues, "issue")
                category.text = issue.value
                category.set("type", issue.kind)

        # add verified reports
        if (
            include_testing
            and use_artifact
            and export_kind == MetadataExportKind.APPSTREAM
            and md.fw.reports_verified
        ):
            testing = ET.SubElement(artifact, "testing")
            for rpt in md.fw.reports_verified:
                test_result = ET.SubElement(testing, "test_result")
                test_result.set("date", rpt.timestamp.strftime("%Y-%m-%d"))

                # vendor name
                vendor_name = ET.SubElement(test_result, "vendor_name")
                vendor_name.text = rpt.verified_vendor.display_name
                vendor_name.set("id", str(rpt.verified_vendor.vendor_id))

                # device
                if rpt.verified_host:
                    device = ET.SubElement(test_result, "device")
                    device.text = rpt.verified_host

                # os
                attr = rpt.get_attribute_by_key("DistroId")
                if attr and attr.value:
                    device = ET.SubElement(test_result, "os")
                    device.text = attr.value
                    attr = rpt.get_attribute_by_key("DistroVersion")
                    if attr and attr.value:
                        device.set("version", attr.value)
                    attr = rpt.get_attribute_by_key("DistroVariant")
                    if attr and attr.value:
                        device.set("variant", attr.value)

                # previous_version
                attr = rpt.get_attribute_by_key("VersionOld")
                if attr and attr.value:
                    ET.SubElement(test_result, "previous_version").text = attr.value

                # custom
                custom = ET.SubElement(test_result, "custom")
                for attr_key in ["RuntimeVersion(org.freedesktop.fwupd)"]:
                    attr = rpt.get_attribute_by_key(attr_key)
                    if attr and attr.value:
                        tr_value = ET.SubElement(custom, "value")
                        tr_value.set("key", attr_key)
                        tr_value.text = attr.value

    # add requires for each allowed vendor_ids
    requires: List[ET.Element] = []
    recommends: List[ET.Element] = []
    suggests: List[ET.Element] = []
    if export_kind == MetadataExportKind.APPSTREAM and not local:

        # create a superset of all vendors (there is typically just one)
        vendor_ids: List[str] = []
        for md in mds:

            # prefer the ODM the uploaded the firmware, but fall back to the OEM if not used
            try:
                vendor = md.fw.odm_vendors[0]
            except IndexError:
                vendor = md.fw.vendor
            if vendor.is_unrestricted and allow_unrestricted:
                continue

            # UEFI devices only got vendor-id values assigned in fwupd 1.3.6,
            # which is made worse by earlier versions of fwupd applying the
            # vendor_id restrictions even when the device vendor ID is NULL :/
            if md.protocol and md.protocol.value == "org.uefi.capsule":
                continue

            # no restrictions in place!
            if not vendor.restrictions:
                vendor_ids.append("XXX:NEVER_GOING_TO_MATCH")
                continue

            # add all the actual vendor_ids
            for res in vendor.restrictions:
                if res.value == "*":
                    continue
                if res.value not in vendor_ids:
                    vendor_ids.append(res.value)

        # allow specifying more than one ID
        if vendor_ids:
            child = ET.Element("firmware")
            child.text = "vendor-id"
            if len(vendor_ids) == 1:
                child.set("compare", "eq")
            else:
                child.set("compare", "regex")
            child.set("version", "|".join(vendor_ids))
            requires.append(child)

    # add requires for <id>
    for rq in _get_unique_requirements(mds, "id"):
        requires.append(rq.to_element())
    for rq in _get_unique_requirements(mds, "id", hardness="recommends"):
        recommends.append(rq.to_element())
    for rq in _get_unique_requirements(mds, "id", hardness="suggests"):
        suggests.append(rq.to_element())

    # add requires for <firmware>
    for rq in _get_unique_requirements(mds, "firmware"):
        requires.append(rq.to_element())
    for rq in _get_unique_requirements(mds, "firmware", hardness="recommends"):
        recommends.append(rq.to_element())
    for rq in _get_unique_requirements(mds, "firmware", hardness="suggests"):
        suggests.append(rq.to_element())

    # add requires for <client>
    for rq in _get_unique_requirements(mds, "client"):
        requires.append(rq.to_element())

    # add a single requirement for <hardware>
    rq_hws: List[str] = []
    for md in mds:
        for rq in md.requirements:
            if rq.kind == "hardware" and rq.value not in rq_hws:
                rq_hws.append(rq.value)
    if rq_hws:
        child = ET.Element("hardware")
        child.text = "|".join(rq_hws)
        requires.append(child)

    # requires, recommends and recommends shared by all releases
    if requires:
        parent = ET.SubElement(component, "requires")
        for element in requires:
            parent.append(element)
    if recommends:
        parent = ET.SubElement(component, "recommends")
        for element in recommends:
            parent.append(element)
    if suggests:
        parent = ET.SubElement(component, "suggests")
        for element in suggests:
            parent.append(element)

    # keywords shared by all releases
    if export_kind in [MetadataExportKind.METAINFO, MetadataExportKind.EXPORT]:
        keywords: List[str] = []
        for md in mds:
            for kw in md.keywords:
                if kw.priority != 5:
                    continue
                if kw.value in keywords:
                    continue
                keywords.append(kw.value)
        if keywords:
            parent = ET.SubElement(component, "keywords")
            for keyword in keywords:
                child = ET.Element("keyword")
                child.text = keyword
                parent.append(child)

    # success
    return component


def _compare_appstream_id_version(item1, item2):
    id1, ver1 = item1.split("::", maxsplit=1)
    id2, ver2 = item2.split("::", maxsplit=1)

    # alphabetical
    if id1 < id2:
        return -1
    if id1 > id2:
        return 1

    # newest first
    return vercmp(ver1, ver2)


def _generate_metadata_kind(
    fws: List[Firmware],
    firmware_baseuri: str = "",
    local: bool = False,
    allow_unrestricted: bool = True,
    include_testing: bool = True,
    use_artifact: bool = True,
    number_revisions: int = 3,
) -> bytes:
    """Generates AppStream metadata of a specific kind"""

    root = ET.Element("components")
    root.set("origin", "lvfs")
    root.set("version", "0.9")

    # build a map of appstream_id:mds
    components: Dict[str, List[Component]] = defaultdict(list)
    for fw in sorted(fws, key=lambda fw: fw.mds[0].appstream_id):
        for md in fw.mds:
            if not local and md.appstream_type == "generic":
                continue

            # do not merge components with self-requirements so that the user
            # can update from A -> B -> C even if A -> C is impossible
            if md.find_req("firmware", None) or md.tags:
                merge_key = "{}::{}".format(md.appstream_id, md.version)
            else:
                merge_key = "{}::{}".format(md.appstream_id, 0)
            components[merge_key].append(md)

    # process each component in version order, but only include the latest few
    # releases to keep the metadata size sane
    for appstream_id in sorted(
        components, key=functools.cmp_to_key(_compare_appstream_id_version)
    ):
        mds = sorted(components[appstream_id], reverse=True)[:number_revisions]
        component = _generate_metadata_mds(
            mds,
            firmware_baseuri=firmware_baseuri,
            local=local,
            allow_unrestricted=allow_unrestricted,
            include_testing=include_testing,
            use_artifact=use_artifact,
        )
        root.append(component)

    # dump to file
    for child in root:
        child.tail = "\n"
    return ET.tostring(root, encoding="utf-8", xml_declaration=True, pretty_print=False)


def _export_component_to_xml(md: Component, pretty_print: bool = True) -> str:
    root = _generate_metadata_mds([md], export_kind=MetadataExportKind.EXPORT)
    return ET.tostring(
        root, encoding="utf-8", xml_declaration=True, pretty_print=pretty_print
    ).decode()


def _regenerate_and_sign_metadata_remote_gz(
    r: Remote, fws_filtered: List[Firmware], invalid_fns: List[str]
) -> None:

    settings = _get_settings()
    blob_xml = _generate_metadata_kind(
        fws_filtered,
        firmware_baseuri=settings["firmware_baseuri"],
        allow_unrestricted=r.is_public,
        include_testing=not r.is_public,  # for size reasons
        use_artifact=False,
        number_revisions=3,
    )
    blob_xmlgz = gzip.compress(blob_xml)

    # write metadata-?????.xml.gz
    download_dir = app.config["DOWNLOAD_DIR"]
    remote_basename = r.get_basename(compression="gz")
    if not remote_basename:
        return
    if not r.get_basename_newest():
        return
    fn_xmlgz = os.path.join(download_dir, remote_basename)
    with open(fn_xmlgz, "wb") as f:
        f.write(blob_xmlgz)
    invalid_fns.append(fn_xmlgz)

    # link metadata.xml.gz
    basename_newest = r.get_basename_newest()
    if basename_newest:
        fn_xmlgz_newest = os.path.join(download_dir, basename_newest)
        try:
            os.remove(fn_xmlgz_newest)
        except OSError:
            pass
        os.symlink(fn_xmlgz, fn_xmlgz_newest)

    # create Jcat item with SHA256 checksum blob
    jcatfile = JcatFile()
    jcatitem = jcatfile.get_item(remote_basename)
    if basename_newest:
        jcatitem.add_alias_id(basename_newest)
    jcatitem.add_blob(JcatBlobSha1(blob_xmlgz))
    jcatitem.add_blob(JcatBlobSha256(blob_xmlgz))

    # write each signed file
    for blob in ploader.metadata_sign(blob_xmlgz):

        # not required
        if not blob.data:
            continue
        if not blob.filename_ext:
            continue

        # add GPG only to archive for backwards compat with older fwupd
        if blob.kind == JcatBlobKind.GPG:
            fn_xmlgz_asc = fn_xmlgz + "." + blob.filename_ext
            with open(fn_xmlgz_asc, "wb") as f:
                f.write(blob.data)
            invalid_fns.append(fn_xmlgz_asc)

        # add to Jcat file too
        jcatitem.add_blob(blob)

    # write jcat file
    remote_basename_jcat = r.get_basename_jcat(compression="gz")
    if remote_basename_jcat:
        fn_xmlgz_jcat = os.path.join(download_dir, remote_basename_jcat)
        with open(fn_xmlgz_jcat, "wb") as f:
            f.write(jcatfile.save())
        invalid_fns.append(fn_xmlgz_jcat)


def _regenerate_and_sign_metadata_remote_xz(
    r: Remote, fws_filtered: List[Firmware], invalid_fns: List[str]
) -> None:

    settings = _get_settings()
    blob_xml = _generate_metadata_kind(
        fws_filtered,
        firmware_baseuri=settings["firmware_baseuri"],
        allow_unrestricted=r.is_public,
        use_artifact=True,
        number_revisions=7,
    )
    blob_xmlxz = lzma.compress(blob_xml)

    # write metadata-?????.xml.xz
    download_dir = app.config["DOWNLOAD_DIR"]
    remote_basename = r.get_basename(compression="xz")
    if not remote_basename:
        return
    fn_xmlxz = os.path.join(download_dir, remote_basename)
    with open(fn_xmlxz, "wb") as f:
        f.write(blob_xmlxz)
    invalid_fns.append(fn_xmlxz)

    # link metadata.xml.xz
    basename_newest = r.get_basename_newest(compression="xz")
    if basename_newest:
        fn_xmlxz_newest = os.path.join(download_dir, basename_newest)
        try:
            os.remove(fn_xmlxz_newest)
        except OSError:
            pass
        os.symlink(fn_xmlxz, fn_xmlxz_newest)

    # create Jcat item with SHA256 checksum blob
    jcatfile = JcatFile()
    jcatitem = jcatfile.get_item(remote_basename)
    if basename_newest:
        jcatitem.add_alias_id(basename_newest)
    jcatitem.add_blob(JcatBlobSha1(blob_xmlxz))
    jcatitem.add_blob(JcatBlobSha256(blob_xmlxz))

    # write each signed file
    for blob in ploader.metadata_sign(blob_xmlxz):
        if blob.data and blob.filename_ext:
            jcatitem.add_blob(blob)

    # write jcat file
    remote_basename_jcat = r.get_basename_jcat(compression="xz")
    if remote_basename_jcat:
        fn_xmlxz_jcat = os.path.join(download_dir, remote_basename_jcat)
        with open(fn_xmlxz_jcat, "wb") as f:
            f.write(jcatfile.save())
        invalid_fns.append(fn_xmlxz_jcat)


def _regenerate_pulp_manifest(include_metadata: bool = True) -> str:

    manifest: PulpManifest = PulpManifest()

    # add metadata
    if include_metadata:
        download_dir: str = app.config["DOWNLOAD_DIR"]
        for r in (
            db.session.query(Remote)
            .filter(Remote.is_public)
            .order_by(Remote.remote_id.asc())
        ):
            for basename in r.basenames:
                fn: str = os.path.join(download_dir, basename)
                if os.path.exists(fn):
                    manifest.add_file(fn)

    # add firmware in stable
    for fw in (
        db.session.query(Firmware)
        .join(Remote)
        .filter(Remote.is_public)
        .join(FirmwareRevision)
        .order_by(FirmwareRevision.filename.asc())
    ):
        manifest.add_fw(fw)

    # return response
    return manifest.export()


def _regenerate_and_sign_metadata_remote(r: Remote, task: Task) -> None:

    # not required */
    if not r.is_signed:
        return

    # not needed
    if not r.get_basename():
        task.add_fail("Remote {} has no basename".format(r.name))
        return

    # increment build counter
    if not r.build_cnt:
        r.build_cnt = 0
    r.build_cnt += 1

    # set destination path from app config
    download_dir = app.config["DOWNLOAD_DIR"]
    if not download_dir:
        task.add_fail("No DOWNLOAD_DIR")
        return
    if not os.path.exists(download_dir):
        os.mkdir(download_dir)

    invalid_fns: List[str] = []
    task.add_pass("Updating {}".format(r.name))

    # create metadata for each remote
    fws_filtered: List[Firmware] = []
    for fw in (
        db.session.query(Firmware)
        .filter(Firmware.signed_timestamp != None)
        .options(joinedload(Firmware.mds))
        .order_by(Firmware.firmware_id.desc())
    ):
        if fw.remote.name in ["private", "deleted"]:
            continue
        if r.check_fw(fw):
            fws_filtered.append(fw)
    task.add_pass("Building metadata with {} firmwares".format(len(fws_filtered)))

    # "legacy" gz and more modern xz
    _regenerate_and_sign_metadata_remote_gz(r, fws_filtered, invalid_fns)
    task.add_pass("Built metadata.gz")
    _regenerate_and_sign_metadata_remote_xz(r, fws_filtered, invalid_fns)
    task.add_pass("Built metadata.xz")

    # do this all at once right at the end of all the I/O
    for fn in invalid_fns:
        task.add_pass("Invalidating {}".format(fn))
        ploader.file_modified(fn)

    # log what we did
    task.add_pass("Signed metadata {}".format(r.name), "build {}".format(r.build_cnt))

    # only keep the last 6 metadata builds (24h / stable refresh every 4h)
    fns = glob.glob(
        os.path.join(download_dir, "firmware-*-{}.*".format(r.access_token))
    )
    for fn in sorted(fns):
        try:
            build_cnt = int(fn.split("-")[1])
        except ValueError:
            task.add_pass("ignoring {} in self tests".format(fn))
            continue
        if build_cnt + 6 > r.build_cnt:
            continue
        os.remove(fn)

    # generate PULP_MANIFEST for people mirroring the archive
    if r.name == "stable":
        with open(os.path.join(download_dir, "PULP_MANIFEST"), "wb") as f:
            f.write(_regenerate_pulp_manifest().encode())
        task.add_pass("Generated PULP_MANIFEST")


def task_regenerate_remote(task: Task) -> None:
    r = (
        db.session.query(Remote)
        .filter(Remote.remote_id == task.id)
        .with_for_update(of=Remote)
        .first()
    )
    if not r:
        return
    _regenerate_and_sign_metadata_remote(r, task=task)
