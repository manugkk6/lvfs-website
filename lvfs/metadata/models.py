#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,singleton-comparison

import datetime
import json
from typing import Optional, List

from sqlalchemy import Column, Integer, Text, DateTime, Boolean
from sqlalchemy.orm import relationship

from lvfs import db

from lvfs.firmware.models import Firmware
from lvfs.tasks.models import Task


class Remote(db.Model):  # type: ignore

    __tablename__ = "remotes"

    remote_id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False, index=True, unique=True)
    is_public = Column(Boolean, default=False)
    build_cnt = Column(Integer, default=0)
    access_token = Column(Text, default=None)

    vendors = relationship("Vendor", back_populates="remote")
    fws = relationship("Firmware", back_populates="remote")

    def check_fw(self, fw: Firmware) -> bool:
        # remote is specified exactly
        if self.remote_id == fw.remote.remote_id:
            return True
        # odm uploaded to oem remote, but also include for odm
        if not self.is_public:
            for vendor in self.vendors:
                if vendor in fw.odm_vendors:
                    return True
        return False

    @property
    def is_deleted(self) -> bool:
        return self.name == "deleted"

    @property
    def is_dirty(self) -> bool:
        return (
            db.session.query(Task)
            .filter(Task.function == "lvfs.metadata.utils.task_regenerate_remote")
            .filter(Task.ended_ts == None)
            .filter(Task.value == json.dumps({"id": self.remote_id}))
            .first()
        ) != None

    @property
    def icon_name(self) -> Optional[str]:
        if self.name == "private":
            return "lock"
        if self.name == "testing":
            return "vial"
        if self.name == "stable":
            return "globe-europe"
        if self.name == "deleted":
            return "trash"
        if self.name.startswith("embargo"):
            return "user-lock"
        return None

    @property
    def color(self) -> Optional[str]:
        if self.name == "private":
            return "info"
        if self.name == "testing":
            return "warning"
        if self.name == "stable":
            return "danger"
        if self.name == "deleted":
            return "secondary"
        if self.name.startswith("embargo"):
            return "success"
        return None

    @property
    def description(self) -> Optional[str]:
        if self.name == "private":
            return "Only available to you"
        if self.name in ["testing", "stable"]:
            return "Available to the public"
        if self.name == "deleted":
            return "Deleted"
        if self.name.startswith("embargo"):
            return "Embargoed"
        return None

    @property
    def key(self) -> str:
        if self.name.startswith("embargo"):
            return "embargo"
        return self.name

    @property
    def is_signed(self) -> bool:
        return self.name not in ["deleted", "private"]

    @property
    def build_str(self) -> str:
        if not self.build_cnt:
            return "00000"
        return "{:05d}".format(self.build_cnt)

    @property
    def basenames(self) -> List[str]:
        basenames: List[str] = []

        for compression in ["gz", "xz"]:

            basename = self.get_basename(compression=compression)
            if basename:
                basenames += [basename]
            basename_jcat = self.get_basename_jcat(compression=compression)
            if basename_jcat:
                basenames += [basename_jcat]

            # only legacy gz
            basename_newest = self.get_basename_newest(compression)
            if compression == "gz" and basename_newest:
                basenames += [
                    basename_newest,
                    "{}.asc".format(basename_newest),
                ]

        return basenames

    def get_basename(self, compression: str = "gz") -> Optional[str]:
        if self.name == "private":
            return None
        if self.name == "stable":
            return "firmware-{}-stable.xml.{}".format(self.build_str, compression)
        if self.name == "testing":
            return "firmware-{}-testing.xml.{}".format(self.build_str, compression)
        return "firmware-{}-{}.xml.{}".format(
            self.build_str, self.access_token, compression
        )

    def get_basename_jcat(self, compression: str = "gz") -> Optional[str]:
        if self.name == "private":
            return None
        if self.name == "stable":
            return "firmware.xml.{}.jcat".format(compression)
        if self.name == "testing":
            return "firmware-testing.xml.{}.jcat".format(compression)
        return "firmware-{}.xml.{}.jcat".format(self.access_token, compression)

    def get_basename_newest(self, compression: str = "gz") -> Optional[str]:
        if self.name == "private":
            return None
        if self.name == "stable":
            return "firmware.xml.{}".format(compression)
        if self.name == "testing":
            return "firmware-testing.xml.{}".format(compression)
        return "firmware-{}.xml.{}".format(self.access_token, compression)

    @property
    def scheduled_signing(self) -> DateTime:
        now = datetime.datetime.now()
        if not self.is_public:
            return now
        secs = (((4 - (now.hour % 4)) * 60) + (60 - now.minute)) * 60 + (
            60 - now.second
        )
        return datetime.datetime.now() + datetime.timedelta(seconds=secs)

    def __repr__(self) -> str:
        return "Remote object %s [%s]" % (self.remote_id, self.name)
