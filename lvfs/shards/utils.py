#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison,unused-argument

from typing import Dict

from lvfs import db

from lvfs.components.models import ComponentShard, ComponentShardInfo
from lvfs.tasks.models import Task


def _generate_stats_shard_info(info: ComponentShardInfo, task: Task) -> None:

    cnt = (
        db.session.query(ComponentShard.component_shard_id)
        .filter(ComponentShard.guid == info.guid)
        .count()
    )
    if info.cnt != cnt:
        task.add_pass(
            "Fixing ComponentShardInfo {}: {} -> {}".format(
                info.component_shard_info_id, info.cnt, cnt
            )
        )
        info.cnt = cnt


def _regenerate_shard_infos(task: Task) -> None:

    # Set ComponentShardInfo in ComponentShard if GUID matches
    infos: Dict[str, ComponentShardInfo] = {}
    for info in db.session.query(ComponentShardInfo):
        if info.guid in infos:
            # the GUID already exists; remove the old ComponentShardInfo
            oldinfo = infos[info.guid]
            for shard in oldinfo.shards:
                task.add_pass(
                    "Reparenting shard {} with {}".format(
                        shard.component_shard_id, shard.guid
                    )
                )
                shard.component_shard_info_id = info.component_shard_info_id
            if oldinfo.claim_id:
                info.claim_id = oldinfo.claim_id
            db.session.delete(oldinfo)
            db.session.commit()
        infos[info.guid] = info
    if task:
        task.percentage_current = 0
        task.percentage_total = db.session.query(ComponentShard).count()
    for (component_shard_id,) in db.session.query(
        ComponentShard.component_shard_id
    ).filter(ComponentShard.component_shard_info_id == None):
        if task:
            task.percentage_current += 1
            task.status = "ComponentShard {}".format(component_shard_id)
        shard = (
            db.session.query(ComponentShard)
            .filter(ComponentShard.component_shard_id == component_shard_id)
            .with_for_update(of=ComponentShard)
            .first()
        )
        if not shard:
            continue
        shard.info = infos.get(shard.guid)
        if shard.info:
            task.add_pass(
                "Fixing shard {} with {}".format(component_shard_id, shard.guid)
            )
        else:
            task.add_pass("Creating ComponentShardInfo for {}".format(shard.guid))
            info = ComponentShardInfo(guid=shard.guid)
            db.session.add(info)
            shard.info = info
            infos[shard.guid] = shard.info
        db.session.commit()

    # update ComponentShardInfo.cnt
    if task:
        task.percentage_current = 0
        task.percentage_total = db.session.query(ComponentShardInfo).count()
    for (info_id,) in db.session.query(
        ComponentShardInfo.component_shard_info_id
    ).order_by(ComponentShardInfo.component_shard_info_id.asc()):
        if task:
            task.percentage_current += 1
            task.status = "ComponentShardInfo {}".format(info_id)
        info = (
            db.session.query(ComponentShardInfo)
            .filter(ComponentShardInfo.component_shard_info_id == info_id)
            .with_for_update(of=ComponentShardInfo)
            .one()
        )
        _generate_stats_shard_info(info, task=task)
        db.session.commit()


def task_regenerate(task: Task) -> None:
    _regenerate_shard_infos(task)
