#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position,singleton-comparison

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_agreement_decline(self, _app, client):

        # add a user and try to upload firmware without signing the agreement
        self.login()
        self.add_user("testuser@fwupd.org")
        self.logout()
        self.login("testuser@fwupd.org")
        rv = client.post("/lvfs/agreements/1/decline", follow_redirects=True)
        assert b"Recorded decline of the agreement" in rv.data, rv.data.decode()
        rv = self._upload("contrib/hughski-colorhug2-2.0.3.cab", "private")
        assert b"User has not signed legal agreement" in rv.data, rv.data.decode()

    def test_agreement_list_modify_add_delete(self, _app, client):

        # get the default one
        self.login()
        rv = client.get("/lvfs/agreements/list")
        assert b"New agreement text" in rv.data, rv.data.decode()

        # modify the agreement
        rv = client.post(
            "/lvfs/agreements/1/modify",
            data=dict(
                version=12345,
                text="DONOTSIGN",
            ),
            follow_redirects=True,
        )
        assert b"Modified agreement" in rv.data, rv.data.decode()
        assert b"12345" in rv.data, rv.data.decode()
        assert b"DONOTSIGN" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/agreements/list")
        assert b"12345" in rv.data, rv.data.decode()
        assert b"DONOTSIGN" in rv.data, rv.data.decode()

        # create a new one
        rv = client.post("/lvfs/agreements/create", follow_redirects=True)
        assert b"Created agreement" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/agreements/list")
        assert b"New agreement text" in rv.data, rv.data.decode()

        # delete the original one
        rv = client.post("/lvfs/agreements/1/delete", follow_redirects=True)
        assert b"Deleted agreement" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/agreements/list")
        assert b"DONOTSIGN" not in rv.data, rv.data.decode()


if __name__ == "__main__":
    unittest.main()
