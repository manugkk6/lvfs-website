#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from typing import Any, Optional
from flask import Blueprint, request, flash, url_for, redirect, render_template, g
from flask_login import login_required
from sqlalchemy.exc import NoResultFound, IntegrityError

from lvfs import db

from lvfs.util import admin_login_required

from .models import Agreement

bp_agreements = Blueprint("agreements", __name__, template_folder="templates")


@bp_agreements.route("/")
@bp_agreements.route("/<int:agreement_id>")
@login_required
def route_show(agreement_id: Optional[int] = None) -> Any:
    # find the right version
    if agreement_id:
        try:
            agreement = (
                db.session.query(Agreement)
                .filter(Agreement.agreement_id == agreement_id)
                .one()
            )
        except NoResultFound:
            flash("No agreement ID found", "warning")
            return redirect(url_for("agreements.route_show"))
    else:
        agreement = (
            db.session.query(Agreement).order_by(Agreement.version.desc()).first()
        )
    if not agreement:
        agreement = Agreement(version=1, text="No agreement text found")
        db.session.add(agreement)
        db.session.commit()
    return render_template("agreement.html", category="admin", agreement=agreement)


@bp_agreements.route("/list")
@login_required
@admin_login_required
def route_list() -> Any:

    # find the right version
    agreements = db.session.query(Agreement).all()
    return render_template(
        "agreement-list.html", category="admin", agreements=agreements
    )


@bp_agreements.post("/create")
@login_required
@admin_login_required
def route_create() -> Any:

    # create something
    agreement = Agreement(version=1, text="New agreement text")
    db.session.add(agreement)
    db.session.commit()
    flash("Created agreement")
    return redirect(
        url_for("agreements.route_modify", agreement_id=agreement.agreement_id)
    )


@bp_agreements.post("/<int:agreement_id>/accept")
@login_required
def route_confirm(agreement_id: int) -> Any:

    # find the object for the ID
    try:
        agreement = (
            db.session.query(Agreement)
            .filter(Agreement.agreement_id == agreement_id)
            .one()
        )
    except NoResultFound:
        flash("No agreement ID found", "warning")
        return redirect(url_for("agreements.route_show"))

    # already agreed to a newer version
    if g.user.agreement and g.user.agreement.version >= agreement.version:
        flash("You already agreed to this version of the agreement", "info")
        return redirect(url_for("agreements.route_show"))

    # save this
    g.user.agreement_id = agreement.agreement_id
    db.session.commit()
    flash("Recorded acceptance of the agreement")
    return redirect(url_for("upload.route_firmware"))


@bp_agreements.post("/decline")
@bp_agreements.post("/<int:agreement_id>/decline")
@login_required
def route_decline(agreement_id: Optional[int] = None) -> Any:
    g.user.agreement_id = None
    db.session.commit()
    if agreement_id:
        flash("Recorded decline of the agreement %i" % agreement_id)
    else:
        flash("Recorded decline of the agreement")
    return redirect(url_for("main.route_index_uncached"))


@bp_agreements.route("/<int:agreement_id>/modify", methods=["GET", "POST"])
@login_required
@admin_login_required
def route_modify(agreement_id: int) -> Any:

    # match
    try:
        agreement = (
            db.session.query(Agreement)
            .filter(Agreement.agreement_id == agreement_id)
            .one()
        )
    except NoResultFound:
        flash("No agreement with that ID", "danger")
        return redirect(url_for("agreements.route_list"))

    # view
    if request.method != "POST":
        return render_template(
            "agreement-admin.html", category="admin", agreement=agreement
        )

    # change
    if "version" in request.form:
        agreement.version = int(request.form["version"])
    if "text" in request.form:
        agreement.text = request.form["text"]
    db.session.commit()
    flash("Modified agreement")
    return redirect(url_for("agreements.route_modify", agreement_id=agreement_id))


@bp_agreements.post("/<int:agreement_id>/delete")
@login_required
@admin_login_required
def route_delete(agreement_id: int) -> Any:

    # match
    try:
        db.session.query(Agreement).filter(
            Agreement.agreement_id == agreement_id
        ).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Cannot delete", "danger")
        return redirect(url_for("agreements.route_list"))

    flash("Deleted agreement")
    return redirect(url_for("agreements.route_list"))
