#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from typing import Any

from flask import Blueprint, request, url_for, redirect, flash, render_template
from flask_login import login_required
from sqlalchemy.exc import NoResultFound, IntegrityError

from lvfs import db

from lvfs.util import admin_login_required

from .models import License

bp_licenses = Blueprint("licenses", __name__, template_folder="templates")


@bp_licenses.route("/")
@login_required
@admin_login_required
def route_list() -> Any:
    licenses = db.session.query(License).order_by(License.value.asc()).all()
    return render_template("license-list.html", category="admin", licenses=licenses)


@bp_licenses.post("/create")
@login_required
@admin_login_required
def route_create() -> Any:
    # ensure has enough data
    try:
        value = request.form["value"]
    except KeyError:
        flash("No form data found!", "warning")
        return redirect(url_for("licenses.route_list"))
    if not value or value.find(" ") != -1:
        flash("Failed to add license: Value is not valid", "warning")
        return redirect(url_for("licenses.route_list"))

    # add license
    try:
        lic = License(value=request.form["value"])
        db.session.add(lic)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add license: The license already exists", "info")
        return redirect(url_for("licenses.route_list"))
    flash("Added license", "info")
    return redirect(url_for("licenses.route_show", license_id=lic.license_id))


@bp_licenses.post("/<int:license_id>/delete")
@login_required
@admin_login_required
def route_delete(license_id: int) -> Any:

    # get license
    try:
        db.session.query(License).filter(License.license_id == license_id).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("License cannot be deleted", "info")
        return redirect(url_for("licenses.route_list"))
    flash("Deleted license", "info")
    return redirect(url_for("licenses.route_list"))


@bp_licenses.post("/<int:license_id>/modify")
@login_required
@admin_login_required
def route_modify(license_id: int) -> Any:

    # find license
    try:
        lic = (
            db.session.query(License)
            .filter(License.license_id == license_id)
            .with_for_update(of=License)
            .one()
        )
    except NoResultFound:
        flash("No license found", "info")
        return redirect(url_for("licenses.route_list"))

    # modify license
    lic.is_content = bool("is_content" in request.form)
    lic.is_approved = bool("is_approved" in request.form)
    lic.requires_source = bool("requires_source" in request.form)
    for key in ["name", "text", "url"]:
        if key in request.form:
            setattr(lic, key, request.form[key] or None)
    db.session.commit()

    # success
    flash("Modified license", "info")
    return redirect(url_for("licenses.route_show", license_id=license_id))


@bp_licenses.route("/<int:license_id>")
@login_required
@admin_login_required
def route_show(license_id: int) -> Any:

    # find license
    try:
        lic = db.session.query(License).filter(License.license_id == license_id).one()
    except NoResultFound:
        flash("No license found", "info")
        return redirect(url_for("licenses.route_list"))

    # show details
    return render_template("license-details.html", category="admin", lic=lic)
