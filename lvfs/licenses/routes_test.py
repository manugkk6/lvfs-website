#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2020 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position,singleton-comparison

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_licenses(self, _app, client):

        self.login()
        self.upload()
        rv = client.get("/lvfs/licenses/")
        assert "MPL-2.0" not in rv.data.decode("utf-8"), rv.data

        # create
        rv = client.post(
            "/lvfs/licenses/create",
            data=dict(
                value="MPL-2.0",
            ),
            follow_redirects=True,
        )
        assert b"Added license" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/licenses/")
        assert "MPL-2.0" in rv.data.decode("utf-8"), rv.data.decode()
        rv = client.post(
            "/lvfs/licenses/create",
            data=dict(
                value="MPL-2.0",
            ),
            follow_redirects=True,
        )
        assert b"already exists" in rv.data, rv.data.decode()

        # modify
        rv = client.post(
            "/lvfs/licenses/4/modify",
            data=dict(
                name="ACME",
            ),
            follow_redirects=True,
        )
        assert b"Modified license" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/licenses/")
        assert "ACME" in rv.data.decode("utf-8"), rv.data.decode()

        # show
        rv = client.get("/lvfs/licenses/4", follow_redirects=True)
        assert b"ACME" in rv.data, rv.data.decode()

        # delete
        rv = client.post("/lvfs/licenses/4/delete", follow_redirects=True)
        assert b"Deleted license" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/licenses/")
        assert "MPL-2.0" not in rv.data.decode("utf-8"), rv.data.decode()


if __name__ == "__main__":
    unittest.main()
