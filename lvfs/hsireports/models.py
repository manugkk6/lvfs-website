#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods

import datetime
from typing import Optional, List

from flask import g

from sqlalchemy import Column, Integer, Text, String, DateTime, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from lvfs import db

from lvfs.users.models import User


class HsiReportInfo(db.Model):  # type: ignore
    __tablename__ = "hsi_report_infos"

    hsi_report_info_id = Column(Integer, primary_key=True)
    appstream_id = Column(Text, nullable=False, unique=True, index=True)
    appstream_id_old = Column(Text, default=None, index=True)
    name = Column(Text, default=None)
    level = Column(Integer, default=None)

    def __repr__(self) -> str:
        return "HsiReportInfo object {}".format(self.appstream_id)


class HsiReportAttr(db.Model):  # type: ignore
    __tablename__ = "hsi_report_attrs"

    report_attr_id = Column(Integer, primary_key=True)
    hsi_report_id = Column(
        Integer, ForeignKey("hsi_reports.hsi_report_id"), nullable=False, index=True
    )
    appstream_id = Column(Text, nullable=False)
    hsi_result = Column(Text, default=None)
    is_success = Column(Boolean, default=False)
    is_runtime = Column(Boolean, default=False)
    is_obsoleted = Column(Boolean, default=False)

    report = relationship("HsiReport", back_populates="attrs")

    def __repr__(self) -> str:
        return "HsiReportAttr object %s=%s" % (self.key, self.value)


class HsiReport(db.Model):  # type: ignore

    __tablename__ = "hsi_reports"

    hsi_report_id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    payload = Column(Text, nullable=False)
    signature = Column(Text, default=None)
    machine_id = Column(String(64), nullable=False)
    distro = Column(Text, default=None)
    kernel_cmdline = Column(Text, default=None)
    kernel_version = Column(Text, default=None)
    host_product = Column(Text, default=None, index=True)
    host_vendor = Column(Text, default=None, index=True)
    host_family = Column(Text, default=None, index=True)
    host_sku = Column(Text, default=None, index=True)
    host_security_id = Column(Text, nullable=False)
    host_security_version = Column(Text, nullable=False)
    user_id = Column(Integer, ForeignKey("users.user_id"), default=None)

    user = relationship("User", foreign_keys=[user_id])
    attrs = relationship(
        "HsiReportAttr", back_populates="report", cascade="all,delete,delete-orphan"
    )

    @property
    def host_sku_valid(self) -> bool:
        return self.host_sku not in [
            None,
            "",
            "Default string",
            "None",
            "To be filled by O.E.M.",
        ]

    @property
    def host_vendor_valid(self) -> bool:
        return self.host_vendor not in [
            None,
            "",
            "Default string",
            "None",
            "System manufacturer",
            "To Be Filled By O.E.M.",
            "Unknown",
        ]

    @property
    def host_product_valid(self) -> bool:
        return self.host_product not in [
            None,
            "",
            "Default string",
            "None",
            "System Product Name",
            "To be filled by O.E.M.",
        ]

    @property
    def host_family_valid(self) -> bool:
        return self.host_family not in [
            None,
            "",
            "*",
            "BSW",
            "CML",
            "Default string",
            "Desktop",
            "HSW",
            "KBL",
            "N/A",
            "None",
            "Not Applicable",
            "Notebook",
            "Raspberry Pi",
            "SKL",
            "TGL",
            "Surface",
            "To be filled by O.E.M.",
        ]

    @property
    def title(self) -> str:
        parts: List[str] = []
        if self.host_vendor_valid:
            parts.append(self.host_vendor)
        if self.host_product_valid:
            parts.append(self.host_product)
        if self.host_family_valid:
            parts.append(self.host_family)
        return " ".join(parts)

    def check_acl(self, action: str, user: Optional[User] = None) -> bool:

        # fall back
        if not user:
            user = g.user
        if not user:
            return False
        if user.check_acl("@admin"):
            return True

        # depends on the action requested
        if action == "@delete":
            return False
        if action == "@view":
            return True
        raise NotImplementedError(
            "unknown security check action: %s:%s" % (self, action)
        )

    def __repr__(self) -> str:
        return "HsiReport object {}".format(self.hsi_report_id)
