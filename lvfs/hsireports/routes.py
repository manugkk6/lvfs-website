#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison

import json

from typing import Any, Dict, List, Tuple, Optional
from collections import defaultdict
from flask import (
    Blueprint,
    Response,
    flash,
    redirect,
    render_template,
    request,
    url_for,
    jsonify,
)
from flask_login import login_required
from sqlalchemy.exc import NoResultFound, IntegrityError

from lvfs import db, csrf, cache

from lvfs.hash import _is_sha256
from lvfs.util import admin_login_required, _pkcs7_signature_info
from lvfs.util import _json_success, _json_error
from lvfs.vendors.models import VendorRestriction, Vendor

from .models import HsiReport, HsiReportAttr, HsiReportInfo

bp_hsireports = Blueprint("hsireports", __name__, template_folder="templates")


@bp_hsireports.route("/")
@login_required
@admin_login_required
def route_list() -> Any:
    rpts = (
        db.session.query(HsiReport)
        .order_by(HsiReport.timestamp.desc())
        .limit(100)
        .all()
    )
    return render_template(
        "hsireport-list.html", category="admin", rpts_filter=None, rpts=rpts
    )


@bp_hsireports.route("/vendor/<host_vendor>")
@login_required
@admin_login_required
def route_vendor_show(host_vendor: str) -> Any:
    rpts = (
        db.session.query(HsiReport)
        .filter(HsiReport.host_vendor == host_vendor)
        .order_by(HsiReport.timestamp.desc())
        .limit(100)
        .all()
    )
    return render_template(
        "hsireport-list.html", category="admin", rpts_filter=host_vendor, rpts=rpts
    )


@bp_hsireports.route("/product/<host_product>")
@login_required
@admin_login_required
def route_product_show(host_product: str) -> Any:
    rpts = (
        db.session.query(HsiReport)
        .filter(HsiReport.host_product == host_product)
        .order_by(HsiReport.timestamp.desc())
        .limit(100)
        .all()
    )
    return render_template(
        "hsireport-list.html", category="admin", rpts_filter=host_product, rpts=rpts
    )


@bp_hsireports.route("/sku/<host_sku>")
@login_required
@admin_login_required
def route_sku_show(host_sku: str) -> Any:
    rpts = (
        db.session.query(HsiReport)
        .filter(HsiReport.host_sku == host_sku)
        .order_by(HsiReport.timestamp.desc())
        .limit(100)
        .all()
    )
    return render_template(
        "hsireport-list.html", category="admin", rpts_filter=host_sku, rpts=rpts
    )


@bp_hsireports.route("/family/<host_family>")
@login_required
@admin_login_required
def route_family_show(host_family: str) -> Any:
    rpts = (
        db.session.query(HsiReport)
        .filter(HsiReport.host_family == host_family)
        .order_by(HsiReport.timestamp.desc())
        .limit(100)
        .all()
    )
    return render_template(
        "hsireport-list.html", category="admin", rpts_filter=host_family, rpts=rpts
    )


@bp_hsireports.route("/<int:hsi_report_id>")
@login_required
def route_show(hsi_report_id: int) -> Any:
    try:
        rpt = (
            db.session.query(HsiReport)
            .filter(HsiReport.hsi_report_id == hsi_report_id)
            .one()
        )
    except NoResultFound:
        flash("HsiReport does not exist", "danger")
        return redirect(url_for("main.route_dashboard"))
    # security check
    if not rpt.check_acl("@view"):
        flash("Permission denied: Unable to view report", "danger")
        return redirect(url_for("main.route_dashboard"))
    return render_template("hsireport-details.html", category="admin", rpt=rpt)


@bp_hsireports.route("/<int:hsi_report_id>/raw")
@login_required
def route_view(hsi_report_id: int) -> Any:
    try:
        rpt = (
            db.session.query(HsiReport)
            .filter(HsiReport.hsi_report_id == hsi_report_id)
            .one()
        )
    except NoResultFound:
        return _json_error("HsiReport does not exist")
    # security check
    if not rpt.check_acl("@view"):
        flash("Permission denied: Unable to view report", "danger")
        return redirect(url_for("main.route_dashboard"))
    return Response(response=rpt.payload, status=400, mimetype="application/json")


@bp_hsireports.post("/<int:hsi_report_id>/delete")
@login_required
def route_delete(hsi_report_id: int) -> Any:
    try:
        rpt = (
            db.session.query(HsiReport)
            .filter(HsiReport.hsi_report_id == hsi_report_id)
            .one()
        )
    except NoResultFound:
        flash("No report found!", "danger")
        return redirect(url_for("analytics.route_hsireports"))
    # security check
    if not rpt.check_acl("@delete"):
        flash("Permission denied: Unable to delete report", "danger")
        return redirect(url_for("hsireports.route_show", hsi_report_id=hsi_report_id))
    db.session.delete(rpt)
    db.session.commit()
    flash("Deleted report", "info")
    return redirect(url_for("analytics.route_hsireports"))


@bp_hsireports.post("/upload")
@csrf.exempt
def route_report() -> Any:
    """Upload a report"""

    # only accept form data
    if request.method != "POST":
        return _json_error("only POST supported")

    # parse both content types, either application/json or multipart/form-data
    signature = None
    if request.data:
        payload = request.data.decode("utf8")
    elif request.form:
        data = request.form.to_dict()
        if "payload" not in data:
            return _json_error("No payload in multipart/form-data")
        payload = data["payload"]
        if "signature" in data:
            signature = data["signature"]
    else:
        return _json_error("No data")

    # find user and verify
    if signature:
        try:
            info = _pkcs7_signature_info(signature)
        except IOError as e:
            return _json_error("Signature invalid: %s" % str(e))
        if "serial" not in info:
            return _json_error("Signature invalid, no signature")

    # parse JSON data
    try:
        item = json.loads(payload)
    except ValueError as e:
        return _json_error("No JSON object could be decoded: " + str(e))

    # check we got enough data
    for key in ["ReportVersion", "MachineId", "Metadata"]:
        if not key in item:
            return _json_error("invalid data, expected %s" % key)
        if item[key] is None:
            return _json_error("missing data, expected %s" % key)

    # parse only this version
    if item["ReportVersion"] != 2:
        return _json_error("report version not supported")

    # add each firmware report
    machine_id = item["MachineId"]
    if not _is_sha256(machine_id):
        return _json_error("MachineId invalid, expected SHA256")
    hsireports = item.get("HsiReports", [])
    sec_attrs = item.get("SecurityAttributes", [])
    if not hsireports and not sec_attrs:
        return _json_error("no hsireports included")
    metadata = item["Metadata"]
    if not metadata:
        return _json_error("no metadata included")

    if sec_attrs:

        # required metadata for this report type
        for key in ["HostProduct", "HostFamily", "HostVendor", "HostSku"]:
            if not key in metadata:
                return _json_error("invalid data, expected %s" % key)
            if metadata[key] is None:
                return _json_error("missing data, expected %s" % key)

        # check attrs
        for sec_attr in sec_attrs:
            for key in ["AppstreamId", "HsiResult"]:
                if not key in sec_attr:
                    return _json_error("invalid data, expected %s" % key)
                if sec_attr[key] is None:
                    return _json_error("missing data, expected %s" % key)

        # update any old report
        rpt = (
            db.session.query(HsiReport)
            .filter(HsiReport.machine_id == machine_id)
            .first()
        )
        if rpt:
            db.session.delete(rpt)

        # construct a single string
        distro = "{} {} ({})".format(
            metadata.get("DistroId", "Unknown"),
            metadata.get("DistroVersion", "Unknown"),
            metadata.get("DistroVariant", "Unknown"),
        )

        # save a new report in the database
        host_security_parts = metadata["HostSecurityId"].split(" ")
        host_security_version: Optional[str] = None
        try:
            host_security_version = host_security_parts[1][2:-1]
        except IndexError:
            pass
        rpt = HsiReport(
            payload=payload,  # in case we want to extract
            signature=signature,  # if we want to match the user
            machine_id=machine_id,
            distro=distro,
            kernel_cmdline=metadata.get("KernelCmdline"),
            kernel_version=metadata.get("KernelVersion"),
            host_product=metadata["HostProduct"],
            host_vendor=metadata["HostVendor"],
            host_family=metadata["HostFamily"],
            host_sku=metadata["HostSku"],
            host_security_id=host_security_parts[0],
            host_security_version=host_security_version or "",
        )
        for sec_attr in sec_attrs:
            flags = sec_attr.get("Flags", [])
            attr = HsiReportAttr(
                appstream_id=sec_attr["AppstreamId"],
                hsi_result=sec_attr["HsiResult"],
                is_runtime="runtime-issue" in flags,
                is_success="success" in flags,
                is_obsoleted="obsoleted" in flags,
            )
            rpt.attrs.append(attr)
        db.session.add(rpt)

    # all done
    db.session.commit()
    return _json_success()


@bp_hsireports.route("/levels.json")
@cache.cached()
def route_levels_json() -> Any:

    data: Dict[int, List[str]] = defaultdict(list)
    for info in (
        db.session.query(HsiReportInfo)
        .filter(HsiReportInfo.level != None)
        .order_by(HsiReportInfo.hsi_report_info_id.asc())
    ):
        data[info.level].append(info.appstream_id)
        if info.appstream_id_old:
            data[info.level].append(info.appstream_id_old)
    return jsonify(data)


def _generate_vendor_device_reports() -> Dict[str, List[HsiReport]]:

    vendors: Dict[str, List[HsiReport]] = defaultdict(list)
    for r in (
        db.session.query(HsiReport)
        .distinct(HsiReport.host_vendor, HsiReport.host_product)
        .order_by(HsiReport.host_vendor.asc())
    ):
        if not r.host_vendor_valid:
            continue
        if not r.host_product_valid:
            continue
        host_vendor_fixups = {
            "ASUSTeK Computer INC.": "ASUSTeK COMPUTER INC.",
            "FUJITSU CLIENT COMPUTING LIMITED": "Fujitsu ",
            "FUJITSU SIEMENS": "Fujitsu ",
            "Gigabyte Technology Co., Ltd.": "GIGABYTE",
            "GOOGLE": "Google",
            "Hewlett-Packard": "HP",
            "Intel(R) Client Systems": "Intel Corporation",
            "Micro-Star International Co., Ltd": "MSI",
            "Micro-Star International Co., Ltd.": "MSI",
            "System76, Inc.": "System76",
        }
        vendors[host_vendor_fixups.get(r.host_vendor, r.host_vendor)].append(r)
    return vendors


@bp_hsireports.route("/devices")
@cache.cached()
def route_devices() -> Any:

    # create a list of possible vendor restrictions
    vendors = _generate_vendor_device_reports()
    vendor_dmis: List[str] = []
    for host_vendor in vendors:
        vendor_dmis.append("HSI:{}".format(host_vendor))

    # create a map of HSI host_vendor:Vendor
    vendors_map: Dict[str, List[Vendor]] = defaultdict(list)
    for vr in db.session.query(VendorRestriction).filter(
        VendorRestriction.value.in_(vendor_dmis)
    ):
        vendors_map[vr.value[4:]] = vr.vendor

    return render_template(
        "hsireport-devicelist.html", vendors=vendors, vendors_map=vendors_map
    )


@bp_hsireports.route("/devices.json")
@cache.cached()
def route_devices_json() -> Any:

    vendors: Dict[str, List[Dict[str, Any]]] = defaultdict(list)
    for vendor, reports in _generate_vendor_device_reports().items():
        for report in reports:
            vendors[vendor].append(
                {
                    "host_vendor": report.host_vendor,
                    "host_family": report.host_family,
                    "host_product": report.host_product,
                }
            )

    return jsonify(vendors)


def _make_cache_prefix_with_args(*_args: Any, **_kwargs: Any) -> bytes:
    return (request.path + str(hash(frozenset(request.args.items())))).encode("utf-8")


def _generate_device_sku_report_attr_results() -> Tuple[
    Optional[HsiReport], Dict[str, List[Dict[str, Any]]]
]:

    report = None
    report_attrs_raw: Dict[str, List[str]] = defaultdict(list)
    report_attrs_success: Dict[str, bool] = {}

    # sanity check
    for key, value in request.args.items():
        if "\0" in value:
            raise ValueError("NUL found in {}={}".format(key, value))

    for r in (
        db.session.query(HsiReport)
        .filter(HsiReport.host_vendor == request.args.get("host_vendor"))
        .filter(HsiReport.host_family == request.args.get("host_family"))
        .filter(HsiReport.host_product == request.args.get("host_product"))
        .order_by(HsiReport.timestamp.desc())
        .limit(100)
    ):
        if not report:
            report = r
        for attr in r.attrs:
            if attr.is_runtime:
                continue
            if attr.is_obsoleted:
                continue
            if attr.appstream_id in [
                "org.fwupd.hsi.Fwupd.Attestation",
                "org.fwupd.hsi.FwupdAttestation",
                "org.fwupd.hsi.Fwupd.Updates",
                "org.fwupd.hsi.FwupdUpdates",
                "org.fwupd.hsi.IntelAmt",
                "org.fwupd.hsi.UefiDbx",
            ]:
                continue
            report_attrs_raw[attr.appstream_id].append(attr.hsi_result)
            report_attrs_success[attr.appstream_id + attr.hsi_result] = attr.is_success

    report_attrs: Dict[str, List[Dict[str, Any]]] = defaultdict(list)
    for appstream_id, hsi_results in report_attrs_raw.items():
        tmp: Dict[str, int] = {}
        for hsi_result in set(hsi_results):
            tmp[hsi_result] = hsi_results.count(hsi_result)
        for hsi_result, cnt in tmp.items():
            is_success = report_attrs_success[appstream_id + hsi_result]
            report_attrs[appstream_id].append(
                {
                    "is_success": is_success,
                    "hsi_result": hsi_result,
                    "pc": int(cnt * 100 / len(hsi_results)),
                }
            )

    return (
        report,
        report_attrs,
    )


@bp_hsireports.route("/device")
@cache.cached(key_prefix=_make_cache_prefix_with_args)  # type: ignore
def route_device_sku() -> Any:

    # sanity check
    try:
        report, report_attrs = _generate_device_sku_report_attr_results()
    except ValueError as e:
        flash("Invalid input: {}".format(e), "warning")
        return redirect(url_for("hsireports.route_devices"))
    if not report:
        flash("No device", "warning")
        return redirect(url_for("hsireports.route_devices"))

    # translated from geek to human
    hsi_result_map: Dict[str, str] = {
        "locked": "Locked",
        "not-locked": "Unlocked",
        "valid": "Valid",
        "not-valid": "Invalid",
        "enabled": "Enabled",
        "not-enabled": "Disabled",
        "found": "Found",
        "not-found": "Not found",
        "supported": "Supported",
        "not-supported": "Unsupported",
    }
    hsi_level_map: Dict[int, str] = {
        0: "danger",
        1: "warning",
        2: "success",
        3: "success",
        4: "success",
    }

    # for the translated names
    hsi_info_map: Dict[str, HsiReportInfo] = {}
    for info in db.session.query(HsiReportInfo):
        hsi_info_map[info.appstream_id] = info
        hsi_info_map[info.appstream_id_old] = info

    # work out what this might be
    hsi_level: int = 4
    for appstream_id in report_attrs:
        info = hsi_info_map.get(appstream_id)
        if not info:
            continue
        # already worse than this
        if not info.level or hsi_level < info.level:
            continue
        for attr_result in report_attrs[appstream_id]:
            if attr_result["pc"] > 90 and not attr_result["is_success"]:
                hsi_level = info.level - 1

    return render_template(
        "hsireport-device.html",
        report=report,
        report_attrs=report_attrs,
        hsi_result_map=hsi_result_map,
        hsi_info_map=hsi_info_map,
        hsi_level=hsi_level,
        hsi_level_map=hsi_level_map,
    )


@bp_hsireports.route("/device.json")
@cache.cached()
def route_device_json() -> Any:
    try:
        _, report_attrs = _generate_device_sku_report_attr_results()
    except ValueError as e:
        return jsonify({"error": "Invalid input: {}".format(e)})
    return jsonify(report_attrs)


@bp_hsireports.route("/info/")
@login_required
@admin_login_required
def route_entry_list() -> Any:
    infos = (
        db.session.query(HsiReportInfo)
        .order_by(HsiReportInfo.hsi_report_info_id.asc())
        .all()
    )
    return render_template("hsireport-infolist.html", category="admin", infos=infos)


@bp_hsireports.post("/info/create")
@login_required
@admin_login_required
def route_entry_create() -> Any:

    # add info
    try:
        info = HsiReportInfo(appstream_id=request.form["appstream_id"])
        db.session.add(info)
        db.session.commit()
    except KeyError:
        flash("No form data found!", "warning")
        return redirect(url_for("hsireports.route_entry_list"))
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add HSI report attribute: Already exists", "info")
        return redirect(url_for("hsireports.route_entry_list"))
    flash("Added HSI report attribute", "info")
    return redirect(
        url_for("hsireports.route_entry_show", info_id=info.hsi_report_info_id)
    )


@bp_hsireports.post("/info/<int:info_id>/delete")
@login_required
@admin_login_required
def route_entry_delete(info_id: int) -> Any:

    # get info
    try:
        info = (
            db.session.query(HsiReportInfo)
            .filter(HsiReportInfo.hsi_report_info_id == info_id)
            .with_for_update(of=HsiReportInfo)
            .one()
        )
    except NoResultFound:
        flash("No info found", "info")
        return redirect(url_for("hsireports.route_entry_list"))

    # delete
    db.session.delete(info)
    db.session.commit()
    flash("Deleted HSI report attribute", "info")
    return redirect(url_for("hsireports.route_entry_list"))


@bp_hsireports.post("/info/<int:info_id>/modify")
@login_required
@admin_login_required
def route_entry_modify(info_id: int) -> Any:

    # find info
    try:
        info = (
            db.session.query(HsiReportInfo)
            .filter(HsiReportInfo.hsi_report_info_id == info_id)
            .with_for_update(of=HsiReportInfo)
            .one()
        )
    except NoResultFound:
        flash("No HSI report attribute found", "info")
        return redirect(url_for("hsireports.route_entry_list"))

    # modify info
    for key in ["name", "appstream_id", "level", "appstream_id_old"]:
        if key in request.form:
            setattr(info, key, request.form[key])
    db.session.commit()

    # success
    flash("Modified HSI report attribute", "info")
    return redirect(url_for("hsireports.route_entry_show", info_id=info_id))


@bp_hsireports.route("/info/<int:info_id>")
@login_required
@admin_login_required
def route_entry_show(info_id: int) -> Any:

    # find info
    try:
        info = (
            db.session.query(HsiReportInfo)
            .filter(HsiReportInfo.hsi_report_info_id == info_id)
            .one()
        )
    except NoResultFound:
        flash("No HSI report attribute found", "info")
        return redirect(url_for("hsireports.route_entry_list"))

    # show details
    return render_template("hsireport-infodetails.html", category="admin", info=info)
