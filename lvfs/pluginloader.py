#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods

import os
import sys
from typing import List, Optional, Dict, Any

from cabarchive import CabArchive, CabFile
from jcat import JcatBlob

from .components.models import Component
from .firmware.models import Firmware
from .tests.models import Test


class PluginError(Exception):
    pass


class PluginSetting:
    def __init__(self, key: str, name: str, default: Optional[str] = None) -> None:
        self.key = key
        self.name = name
        self.default = default or ""


class PluginSettingInt(PluginSetting):
    def __init__(self, key: str, name: str, default: int = 0):
        super().__init__(key, name, str(default))


class PluginSettingList(PluginSetting):
    def __init__(
        self, key: str, name: str, default: Optional[List[str]] = None
    ) -> None:
        if default:
            super().__init__(key, name, ",".join(default))
        else:
            super().__init__(key, name, "")


class PluginSettingBool(PluginSetting):
    def __init__(self, key: str, name: str, default: bool = False) -> None:
        if default:
            super().__init__(key, name, "enabled")
        else:
            super().__init__(key, name, "disabled")


class PluginBase:
    def __init__(self, plugin_id: Optional[str] = None) -> None:
        self.id = plugin_id
        self.priority = 0
        self._setting_kvs: Dict[str, str] = {}
        self.name = "Noname Plugin"
        self.summary = "Plugin did not set summary"
        self.order_after: List[str] = []
        self.settings: List[PluginSetting] = []

    def file_modified(self, fn: str) -> None:
        raise NotImplementedError

    def oauth_authorize(self, callback: Any) -> None:
        raise NotImplementedError

    def oauth_get_data(self) -> Any:
        raise NotImplementedError

    def metadata_sign(self, blob: bytes) -> JcatBlob:
        raise NotImplementedError

    def archive_sign(self, blob: bytes) -> JcatBlob:
        raise NotImplementedError

    def component_sign(self, md: Component) -> List[JcatBlob]:
        raise NotImplementedError

    def archive_hash(self, fw: Firmware) -> None:
        raise NotImplementedError

    def archive_presign(self, fw: Firmware) -> None:
        raise NotImplementedError

    def archive_copy(self, cabarchive: CabArchive, cabfile: CabFile) -> None:
        raise NotImplementedError

    def archive_finalize(self, cabarchive: CabArchive, fw: Firmware) -> None:
        raise NotImplementedError

    def ensure_test_for_fw(self, fw: Firmware) -> None:
        raise NotImplementedError

    def oauth_logout(self) -> None:
        raise NotImplementedError

    def require_test_for_fw(self, fw: Firmware) -> bool:
        raise NotImplementedError

    def run_test_on_fw(self, test: Test, fw: Firmware) -> None:
        raise NotImplementedError

    def require_test_for_md(self, md: Component) -> bool:
        raise NotImplementedError

    def run_test_on_md(self, test: Test, md: Component) -> None:
        raise NotImplementedError

    def get_setting(self, key: str, required: bool = False) -> str:
        from .util import _get_settings

        if self.id and not self._setting_kvs:
            self._setting_kvs = _get_settings(self.id.replace("-", "_"))
        if key not in self._setting_kvs:
            raise PluginError("No key %s" % key)
        if required and not self._setting_kvs[key]:
            raise PluginError("No value set for key %s" % key)
        return self._setting_kvs[key]

    def get_setting_bool(self, key: str) -> bool:
        if self.get_setting(key) == "enabled":
            return True
        return False

    def get_setting_int(self, key: str) -> int:
        return int(self.get_setting(key))

    @property
    def enabled(self) -> bool:
        for setting in self.settings:
            if setting.name == "Enabled":
                return self.get_setting_bool(setting.key)
        return True

    @property
    def debug(self) -> bool:
        for setting in self.settings:
            if setting.key.endswith("_debug"):
                return self.get_setting_bool(setting.key)
        return False

    def __repr__(self) -> str:
        return "Plugin object %s" % self.id


class PluginGeneral(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self, "general")
        self.name = "General"
        self.summary = "General server settings"
        self.settings.append(
            PluginSetting(
                key="server_warning",
                name="Server Warning",
                default="This is a test instance and may be broken at any time.",
            )
        )
        self.settings.append(
            PluginSetting(
                key="firmware_baseuri",
                name="Firmware BaseURI",
                default="https://fwupd.org/downloads/",
            )
        )
        self.settings.append(
            PluginSettingInt(
                key="default_failure_minimum",
                name="Report failures required to demote",
                default=5,
            )
        )
        self.settings.append(
            PluginSettingInt(
                key="default_failure_percentage",
                name="Report failures threshold for demotion",
                default=70,
            )
        )
        self.settings.append(
            PluginSettingInt(
                key="signed_epoch",
                name="The epoch used for invaliding signed firmware",
                default=1,
            )
        )
        self.settings.append(
            PluginSettingInt(
                key="unused_revision_gc",
                name="When to garbage collect unused firmware revisions (hours)",
                default=24,
            )
        )


class Pluginloader:
    def __init__(self, dirname: str = "."):
        self._dirname = dirname
        self._plugins: List[PluginBase] = []
        self.loaded = False

    def load_plugins(self) -> None:

        if self.loaded:
            return

        plugins = {}
        sys.path.insert(0, self._dirname)
        for f in os.listdir(self._dirname):
            location = os.path.join(self._dirname, f)
            if not os.path.isdir(location):
                continue
            location_init = os.path.join(location, "__init__.py")
            if not os.path.exists(location_init):
                continue
            mod = __import__(f)
            plugins[f] = mod.Plugin()
            if not plugins[f].id:
                plugins[f].id = f
        sys.path.pop(0)

        # depsolve
        for plugin in plugins.values():
            for name in plugin.order_after:
                if name not in plugins:
                    continue
                plugin2 = plugins[name]
                if not plugin2:
                    continue
                if plugin2.priority <= plugin.priority:
                    plugin.priority = plugin2.priority + 1

        # sort by priority
        for plugin in list(plugins.values()):
            self._plugins.append(plugin)
        self._plugins.sort(key=lambda x: x.priority)

        # general item
        self._plugins.insert(0, PluginGeneral())

        # success
        self.loaded = True

    def get_by_id(self, plugin_id: str) -> Optional[PluginBase]:
        if not self.loaded:
            self.load_plugins()
        for p in self._plugins:
            if p.id == plugin_id:
                return p
        return None

    def get_all(self) -> List[PluginBase]:
        if not self.loaded:
            self.load_plugins()
        return self._plugins

    # a file has been modified
    def file_modified(self, fn: str) -> None:
        if not self.loaded:
            self.load_plugins()
        for plugin in self._plugins:
            if not plugin.enabled:
                continue
            try:
                plugin.file_modified(fn)
            except PluginError as e:
                from .util import _event_log

                _event_log(
                    "Plugin %s failed for FileModifed(%s): %s" % (plugin.id, fn, str(e))
                )
            except NotImplementedError:
                pass

    # metadata is being built
    def metadata_sign(self, blob: bytes) -> List[JcatBlob]:
        if not self.loaded:
            self.load_plugins()
        blobs: List[JcatBlob] = []
        for plugin in self._plugins:
            if not plugin.enabled:
                continue
            try:
                blobs.append(plugin.metadata_sign(blob))
            except PluginError as e:
                from .util import _event_log

                _event_log(
                    "Plugin %s failed for MetadataSign(): %s" % (plugin.id, str(e))
                )
            except NotImplementedError:
                pass
        return blobs

    # an archive is being built
    def archive_sign(self, blob: bytes) -> List[JcatBlob]:
        if not self.loaded:
            self.load_plugins()
        blobs: List[JcatBlob] = []
        for plugin in self._plugins:
            if not plugin.enabled:
                continue
            try:
                blobs.append(plugin.archive_sign(blob))
            except PluginError as e:
                from .util import _event_log

                _event_log(
                    "Plugin %s failed for ArchiveSign(): %s" % (plugin.id, str(e))
                )
            except NotImplementedError:
                pass
        return blobs

    # an archive is being built
    def component_sign(self, md: Component) -> List[JcatBlob]:
        if not self.loaded:
            self.load_plugins()
        blobs: List[JcatBlob] = []
        for plugin in self._plugins:
            if not plugin.enabled:
                continue
            try:
                blobs.extend(plugin.component_sign(md))
            except PluginError as e:
                from .util import _event_log

                _event_log(
                    "Plugin %s failed for ComponentSign(): %s" % (plugin.id, str(e))
                )
            except NotImplementedError:
                pass
        return blobs

    # an archive has just been signed and the hash has changed
    def archive_hash(self, fw: Firmware) -> None:
        if not self.loaded:
            self.load_plugins()
        for plugin in self._plugins:
            if not plugin.enabled:
                continue
            try:
                plugin.archive_hash(fw)
            except PluginError as e:
                from .util import _event_log

                _event_log(
                    "Plugin %s failed for ArchiveHash(): %s" % (plugin.id, str(e))
                )
            except NotImplementedError:
                pass

    # an archive is about to be signed
    def archive_presign(self, fw: Firmware) -> None:
        if not self.loaded:
            self.load_plugins()
        for plugin in self._plugins:
            if not plugin.enabled:
                continue
            try:
                plugin.archive_presign(fw)
            except PluginError as e:
                from .util import _event_log

                _event_log(
                    "Plugin %s failed for ArchivePresign(): %s" % (plugin.id, str(e))
                )
            except NotImplementedError:
                pass

    # an archive is being built
    def archive_copy(self, cabarchive: CabArchive, cabfile: CabFile) -> None:
        if not self.loaded:
            self.load_plugins()
        for plugin in self._plugins:
            if not plugin.enabled:
                continue
            try:
                plugin.archive_copy(cabarchive, cabfile)
            except PluginError as e:
                from .util import _event_log

                _event_log(
                    "Plugin %s failed for archive_copy(): %s" % (plugin.id, str(e))
                )
            except NotImplementedError:
                pass

    # an archive is being built
    def archive_finalize(self, cabarchive: CabArchive, fw: Firmware) -> None:
        if not self.loaded:
            self.load_plugins()
        for plugin in self._plugins:
            if not plugin.enabled:
                continue
            try:
                plugin.archive_finalize(cabarchive, fw)
            except PluginError as e:
                from .util import _event_log

                _event_log(
                    "Plugin %s failed for ArchiveFinalize(): %s" % (plugin.id, str(e))
                )
            except NotImplementedError:
                pass

    # ensure an test is added for the firmware
    def ensure_test_for_fw(self, fw: Firmware) -> None:
        if not self.loaded:
            self.load_plugins()
        for plugin in self._plugins:
            if not plugin.enabled:
                continue

            # allow plugins to set conditionals on ensuring
            ensure_test = False
            has_test_fw = False
            has_test_md = False
            try:
                if plugin.require_test_for_fw(fw):
                    ensure_test = True
                has_test_fw = True
            except NotImplementedError:
                pass
            try:
                for md in fw.mds:
                    if plugin.require_test_for_md(md):
                        ensure_test = True
                has_test_md = True
            except NotImplementedError:
                pass

            # any tests without either vfunc are assumed to always run
            if not has_test_md and not has_test_fw:
                ensure_test = True
            if not ensure_test:
                continue
            try:
                plugin.ensure_test_for_fw(fw)
            except PluginError as e:
                from .util import _event_log

                _event_log(
                    "Plugin %s failed for ensure_test_for_fw(): %s"
                    % (plugin.id, str(e))
                )
            except NotImplementedError:
                pass

    # log out of all oauth providers
    def oauth_logout(self) -> None:
        if not self.loaded:
            self.load_plugins()
        for plugin in self._plugins:
            if not plugin.enabled:
                continue
            try:
                plugin.oauth_logout()
            except PluginError as e:
                from .util import _event_log

                _event_log(
                    "Plugin %s failed for oauth_logout(): %s" % (plugin.id, str(e))
                )
            except NotImplementedError:
                pass
