#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison

import tempfile

import yara
from fwhunt_scan import UefiAnalyzer, UefiRule, UefiScanner, UefiScannerError

from lvfs import db

from lvfs.components.models import Component, ComponentShard
from lvfs.firmware.models import Firmware
from lvfs.metadata.models import Remote
from lvfs.tasks.models import Task

from .models import YaraQuery, YaraQueryResult


def task_query_run(task: Task) -> None:
    query = (
        db.session.query(YaraQuery).filter(YaraQuery.yara_query_id == task.id).first()
    )
    if not query:
        return
    _query_run(query, task=task)


def _query_run_shard_fwhunt_scan(
    query: YaraQuery, md: Component, shard: ComponentShard
) -> None:

    if not shard.blob:
        return
    if shard.blob[0:2] != b"MZ" and shard.blob[0:2] != b"VZ":
        return
    try:
        with UefiAnalyzer(blob=shard.blob) as uefi_analyzer:
            scanner = UefiScanner(uefi_analyzer, [query.rules])
            for result in scanner.results:
                if result.res:
                    query.results.append(
                        YaraQueryResult(md=md, shard=shard, result=shard.guid)
                    )
                    break
    except UefiScannerError as e:
        raise NotImplementedError from e


def _query_run_shard_yara(
    query: YaraQuery, md: Component, shard: ComponentShard
) -> None:

    if not shard.blob:
        return
    matches = query.rules.match(data=shard.blob)
    for match in matches:
        msg = match.rule
        for string in match.strings:
            if len(string) == 3:
                try:
                    msg += ": found {}".format(string[2].decode())
                except UnicodeDecodeError:
                    pass
        query.results.append(YaraQueryResult(md=md, shard=shard, result=msg))


def _query_run_component_yara(query: YaraQuery, md: Component) -> None:
    if not md.blob:
        return
    matches = query.rules.match(data=md.blob)
    for match in matches:
        msg = match.rule
        for string in match.strings:
            if len(string) == 3:
                try:
                    msg += ": found {}".format(string[2].decode())
                except UnicodeDecodeError:
                    pass
        query.results.append(YaraQueryResult(md=md, result=msg))

    # one more thing scanned
    query.total += 1

    # unallocate the cached blob as it's no longer needed
    md.blob = None


def _query_setup_yara(query: YaraQuery) -> None:
    try:
        query.rules = yara.compile(source=query.value)
    except yara.SyntaxError as e:
        raise NotImplementedError("Failed to compile rules: {}".format(str(e))) from e
    for line in query.value.replace("{", "\n").split("\n"):
        if line.startswith("rule "):
            query.title = line[5:]
            break
    db.session.commit()


def _query_setup_fwhunt_scan(query: YaraQuery) -> None:
    with tempfile.NamedTemporaryFile(
        mode="wb", prefix="fwhunt_scan_rule", suffix=".yml", dir=None, delete=True
    ) as f:
        f.write(query.value.encode())
        f.flush()
        query.rules = UefiRule(f.name)
        if not query.rules.name:
            raise NotImplementedError("No name found for rule")
        query.title = query.rules.name
        if query.rules.volume_guids:
            for guid in query.rules.volume_guids:
                query.volume_guids.append(guid.lower())
    db.session.commit()


def _query_run(query: YaraQuery, task: Task) -> None:

    # use different vfuncs for YARA and FwHunt scan
    if not query.kind or query.kind == "yara":
        query.setup = _query_setup_yara
        query.run_shard = _query_run_shard_yara
        query.run_component = _query_run_component_yara
    elif query.kind == "FwHunt":
        query.setup = _query_setup_fwhunt_scan
        query.run_shard = _query_run_shard_fwhunt_scan
    if query.setup:
        try:
            query.setup(query)
        except NotImplementedError as e:
            query.error = str(e)
            db.session.commit()
            return

    # restrict these to specific GUIDs
    for line in query.value.split("\n"):
        idx = line.find("X-Volume-GUIDs")
        if idx != -1:
            for guid in line[idx + len("X-Volume-GUIDs") + 1 :].split(","):
                query.volume_guids.append(guid.strip().lower())

    # run query
    with query:

        # don't allow scanning *all* files as it takes ~14h of CPU time
        if not query.user.check_acl("@admin") and not query.volume_guids:
            query.error = "no X-Volume-GUIDs specified"
            return

        component_ids = [
            x[0]
            for x in db.session.query(Component.component_id)
            .join(Firmware)
            .join(Remote)
            .filter(Remote.name == "stable")
            .all()
        ]
        if component_ids:
            fraction = 100 / len(component_ids)
            for idx, component_id in enumerate(component_ids):
                md = (
                    db.session.query(Component)
                    .filter(Component.component_id == component_id)
                    .one()
                )
                task.add_pass(
                    "running query on {} ({})".format(component_id, md.appstream_id)
                )

                # scan each shard
                for shard in md.shards:
                    if query.run_shard and shard.guid in query.volume_guids:
                        query.run_shard(query, md, shard)
                        query.total += 1
                    shard.blob = None

                # scan each component
                if query.run_component:
                    query.run_component(query, md)
                query.found = len(query.results)
                query.percentage = (idx + 1) * fraction
                db.session.commit()


def _query_run_all(task: Task) -> None:

    # get all pending queries
    pending = (
        db.session.query(YaraQuery)
        .filter(YaraQuery.started_ts == None)
        .filter(YaraQuery.error == None)
    )
    for query in pending:
        _query_run(query, task=task)
