#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position,singleton-comparison

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_yara_query(self, _app, client):

        yara_rule = """
rule AMITestKey
{
    condition:
        true
}
"""

        # upload a file
        self.login()
        self.add_namespace()
        self.upload()

        # create a new query
        self.login()
        rv = client.post(
            "/lvfs/queries/create",
            data=dict(value=yara_rule, kind="yara"),
            follow_redirects=True,
        )
        assert b"added and will be run soon" in rv.data, rv.data.decode()

        # add duplicate
        rv = client.post(
            "/lvfs/queries/create",
            data=dict(value=yara_rule, kind="yara"),
            follow_redirects=True,
        )
        assert b"Already a query" in rv.data, rv.data.decode()

        self.run_task_worker()

        rv = client.get("/lvfs/queries/1", follow_redirects=True)
        assert b"AMITestKey" in rv.data, rv.data.decode()

        rv = client.get("/lvfs/queries", follow_redirects=True)
        assert b"Retry" in rv.data, rv.data.decode()

        rv = client.post("/lvfs/queries/1/retry", follow_redirects=True)
        assert b"will be rerun soon" in rv.data, rv.data.decode()

        rv = client.post("/lvfs/queries/1/delete", follow_redirects=True)
        assert b"Deleted query" in rv.data, rv.data.decode()

        rv = client.get("/lvfs/queries/1", follow_redirects=True)
        assert b"No query found" in rv.data, rv.data.decode()


if __name__ == "__main__":
    unittest.main()
