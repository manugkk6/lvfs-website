#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods

from typing import List
import datetime

from sqlalchemy import Column, Integer, Text, Boolean, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from lvfs import db


class Category(db.Model):  # type: ignore

    __tablename__ = "categories"

    category_id = Column(Integer, primary_key=True)
    ctime = Column(DateTime, default=datetime.datetime.utcnow)
    value = Column(Text, nullable=False, unique=True)  # 'X-System'
    name = Column(Text, default=None)  # 'System Update'
    icon = Column(Text, default=None)  # 'battery'
    expect_device_checksum = Column(Boolean, default=False)
    fallback_id = Column(Integer, ForeignKey("categories.category_id"), nullable=True)

    fallback = relationship("Category", remote_side=[category_id])

    def matches(self, values: List[str]) -> bool:
        for value in values:
            if self.value == value:
                return True
            if self.fallback and self.fallback.value == value:
                return True
        return False

    def __repr__(self) -> str:
        return "Category object %s:%s" % (self.category_id, self.value)
