#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position,singleton-comparison

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_categories(self, _app, client):

        self.login()
        self.upload()
        rv = client.get("/lvfs/categories/")
        assert "X-Acme" not in rv.data.decode("utf-8"), rv.data

        # create
        rv = client.post(
            "/lvfs/categories/create",
            data=dict(
                value="X-Acme",
            ),
            follow_redirects=True,
        )
        assert b"Added category" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/categories/")
        assert "X-Acme" in rv.data.decode("utf-8"), rv.data.decode()
        rv = client.post(
            "/lvfs/categories/create",
            data=dict(
                value="X-Acme",
            ),
            follow_redirects=True,
        )
        assert b"already exists" in rv.data, rv.data.decode()

        # modify
        rv = client.post(
            "/lvfs/categories/26/modify",
            data=dict(
                name="ACME",
                example="1.2.3.4",
                fwupd_version="1.2.3",
            ),
            follow_redirects=True,
        )
        assert b"Modified category" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/categories/")
        assert "ACME" in rv.data.decode("utf-8"), rv.data.decode()

        # show
        rv = client.get("/lvfs/categories/26", follow_redirects=True)
        assert b"ACME" in rv.data, rv.data.decode()

        # delete
        rv = client.post("/lvfs/categories/26/delete", follow_redirects=True)
        assert b"Deleted category" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/categories/")
        assert "X-Acme" not in rv.data.decode("utf-8"), rv.data.decode()


if __name__ == "__main__":
    unittest.main()
