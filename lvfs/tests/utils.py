#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison,too-many-nested-blocks,unused-argument

import os

from typing import List

from lvfs import db, ploader

from lvfs.firmware.models import Firmware
from lvfs.tests.models import Test
from lvfs.tasks.models import Task


def _test_priority_sort_func(test: Test) -> int:
    plugin = ploader.get_by_id(test.plugin_id)
    if not plugin:
        return 0
    return plugin.priority


def _test_run_all(tests: List[Test], task: Task) -> None:

    # process each test
    for test in sorted(tests, key=_test_priority_sort_func):
        plugin = ploader.get_by_id(test.plugin_id)
        with test:
            if not plugin:
                task.add_fail("No plugin {}".format(test.plugin_id))
                continue
            try:
                test.container_id = os.environ.get("CONTAINER_ID")
                task.add_pass(
                    "Running test {} for firmware {}".format(
                        test.plugin_id,
                        test.fw.firmware_id,
                    )
                )
                try:
                    if not plugin.require_test_for_fw(test.fw):
                        continue
                except NotImplementedError:
                    pass
                try:
                    plugin.run_test_on_fw(test, test.fw)
                except NotImplementedError:
                    pass
                for md in test.fw.mds:
                    try:
                        if not plugin.require_test_for_md(md):
                            continue
                    except NotImplementedError:
                        pass
                    try:
                        plugin.run_test_on_md(test, md)
                    except NotImplementedError:
                        pass
                # don't leave a failed task running
                db.session.commit()
            except Exception as e:  # pylint: disable=broad-except
                test.add_fail("An exception occurred", str(e))

    # all done
    db.session.commit()


def task_run_all(task: Task) -> None:
    tests = (
        db.session.query(Test)
        .filter(Test.started_ts == None)
        .with_for_update(of=Test)
        .all()
    )
    if not tests:
        return
    _test_run_all(tests, task=task)


def task_test_run(task: Task) -> None:
    tests = (
        db.session.query(Test)
        .filter(Test.started_ts == None)
        .filter(Test.test_id == task.id)
        .with_for_update(of=Test)
        .all()
    )
    if not tests:
        return
    _test_run_all(tests, task=task)


def task_run_for_firmware(task: Task) -> None:
    tests = (
        db.session.query(Test)
        .filter(Test.started_ts == None)
        .filter(Test.firmware_id == task.id)
        .order_by(Test.scheduled_ts)
        .with_for_update(of=Test)
        .all()
    )
    if not tests:
        return
    _test_run_all(tests, task=task)


def task_test_ensure(task: Task) -> None:

    # ensure the test has been added for the firmware type
    for (firmware_id,) in db.session.query(Firmware.firmware_id).order_by(
        Firmware.timestamp
    ):
        fw = (
            db.session.query(Firmware).filter(Firmware.firmware_id == firmware_id).one()
        )
        if not fw.is_deleted:
            ploader.ensure_test_for_fw(fw)
            db.session.commit()
