#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison

import importlib
import datetime
from typing import List

from sqlalchemy.exc import IntegrityError

from lvfs import db

from .models import Task


def _run_task(task: Task) -> None:
    try:
        (modulename, funcname) = task.function.rsplit(".", maxsplit=1)
        module = importlib.import_module(modulename)
        func = getattr(module, funcname)
        func(task)
        db.session.commit()
    except BaseException as e:  # pylint: disable=broad-except
        task.add_fail("Failed to run", str(e))
        db.session.commit()


def task_run_all() -> None:

    for task in (
        db.session.query(Task)
        .filter(Task.started_ts == None)
        .with_for_update(of=Task)
        .order_by(Task.priority.desc())
    ):
        task.started_ts = datetime.datetime.utcnow()
        db.session.commit()
        _run_task(task)
        task.ended_ts = datetime.datetime.utcnow()
        db.session.commit()


def task_autodelete(task: Task) -> None:

    now = datetime.datetime.utcnow()
    deleted: List[str] = []
    for task_tmp in (
        db.session.query(Task)
        .filter(Task.created_ts < now - datetime.timedelta(days=2))
        .order_by(Task.created_ts.asc())
    ):
        if not task_tmp.success:
            continue
        try:
            db.session.delete(task_tmp)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            continue
        deleted.append("#{}".format(task_tmp.task_id))
    if deleted:
        task.add_pass("autodeleted old tasks: {}".format(", ".join(deleted)))

    # just check to see if we can make this a db constraint
    for task_tmp in db.session.query(Task).filter(Task.user is None):
        task.add_fail("Task {} has no user".format(task_tmp.value))
