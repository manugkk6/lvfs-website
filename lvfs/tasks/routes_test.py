#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position,singleton-comparison

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_tasks(self, _app, client):

        self.login()
        self.upload()
        rv = client.get("/lvfs/tasks/")
        assert "acme" not in rv.data.decode("utf-8"), rv.data

        # create
        rv = client.post(
            "/lvfs/tasks/scheduler/create",
            data=dict(
                function="acme",
            ),
            follow_redirects=True,
        )
        assert b"Added task" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/tasks/scheduler/all")
        assert "acme" in rv.data.decode("utf-8"), rv.data.decode()
        rv = client.post(
            "/lvfs/tasks/scheduler/create",
            data=dict(
                function="acme",
            ),
            follow_redirects=True,
        )
        assert b"Already exists" in rv.data, rv.data.decode()

        # modify
        rv = client.post(
            "/lvfs/tasks/scheduler/13/modify",
            data=dict(
                function="ACME",
            ),
            follow_redirects=True,
        )
        assert b"Modified task" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/tasks/scheduler/all")
        assert "ACME" in rv.data.decode("utf-8"), rv.data.decode()

        # show
        rv = client.get("/lvfs/tasks/scheduler/13", follow_redirects=True)
        assert b"ACME" in rv.data, rv.data.decode()

        # delete
        rv = client.post("/lvfs/tasks/scheduler/13/delete", follow_redirects=True)
        assert b"Deleted task" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/tasks/scheduler/all")
        assert "acme" not in rv.data.decode("utf-8"), rv.data.decode()


if __name__ == "__main__":
    unittest.main()
