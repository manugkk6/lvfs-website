#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison,no-member,global-statement

import datetime
import time
import sys
import signal

from typing import Any

from flask import g
from sqlalchemy.orm.exc import ObjectDeletedError

from lvfs import db, create_app

from lvfs.tasks.models import Task, TaskScheduler
from lvfs.tasks.utils import _run_task
from lvfs.users.models import User

# global
_run_mainloop: bool = True


def _signal_handler(signum: int, _frame: Any) -> None:
    print("{}, quitting mainloop".format(signal.strsignal(signum)))
    sys.stdout.flush()
    global _run_mainloop
    _run_mainloop = False


def _check_schedulers() -> bool:

    # any schedulers that have never been run, or are past due
    for task_sched in (
        db.session.query(TaskScheduler)
        .filter(TaskScheduler.interval > 0)
        .with_for_update(of=TaskScheduler)
        .order_by(TaskScheduler.task_scheduler_id)
    ):
        if (
            not task_sched.started_ts
            or task_sched.started_ts + datetime.timedelta(hours=task_sched.interval)
            < datetime.datetime.now()
        ):
            task = Task(
                caller=__name__,
                user=g.user,
                function=task_sched.function,
                priority=task_sched.priority,
            )
            db.session.add(task)
            task_sched.started_ts = datetime.datetime.now()
            task_sched.task = task
            db.session.commit()
            return True

    # nothing to do
    db.session.commit()
    return False


def _run_mainloop_with_ctx() -> None:

    # the lock is important here
    task = (
        db.session.query(Task)
        .filter(Task.started_ts == None)
        .filter(
            Task.created_ts < datetime.datetime.utcnow() - datetime.timedelta(seconds=5)
        )
        .with_for_update(of=Task)
        .order_by(Task.priority.desc())
        .first()
    )
    if task:

        # sanity check
        if task.started_ts:
            task.add_pass("Skipped", "Already run")
            return

        # mark the task as started
        task.started_ts = datetime.datetime.utcnow()

        # mark any duplicate tasks as skipped
        for task_dup in (
            db.session.query(Task)
            .filter(Task.task_id != task.task_id)
            .filter(Task.started_ts == None)
            .filter(Task.ended_ts == None)
            .filter(Task.function == task.function)
            .filter(Task.value == task.value)
        ):
            task_dup.ended_ts = datetime.datetime.utcnow()
            task_dup.add_pass("Skipped", "Running {} instead".format(task.task_id))
    # unlock
    db.session.commit()
    if not task:
        if _check_schedulers():
            return
        time.sleep(1)
        return
    with task:
        task.status = "Running"
        db.session.commit()
        print("running task {}: {}".format(task.function, task.value))
        sys.stdout.flush()
        _run_task(task)
    try:
        db.session.commit()
    except ObjectDeletedError:
        pass


if __name__ == "__main__":

    signal.signal(signal.SIGINT, _signal_handler)
    signal.signal(signal.SIGTERM, _signal_handler)
    app = create_app()
    app.config["SERVER_NAME"] = app.config["HOST_NAME"]
    with app.app_context(), app.test_request_context():
        g.user = (
            db.session.query(User).filter(User.username == "anon@fwupd.org").first()
        )
        while _run_mainloop:
            _run_mainloop_with_ctx()
    print("done!")
