#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position,singleton-comparison

from typing import Any
from flask import Blueprint, redirect, render_template, url_for, flash
from sqlalchemy.exc import NoResultFound

from lvfs import db, cache

from lvfs.agreements.models import Agreement
from lvfs.protocols.models import Protocol
from lvfs.vendors.models import Vendor
from lvfs.verfmts.models import Verfmt

bp_docs = Blueprint("docs", __name__, template_folder="templates")


@bp_docs.route("/developers")
@cache.cached()
def route_developers() -> Any:
    return render_template("docs-developers.html")


@bp_docs.route("/privacy")
def route_privacy() -> Any:
    return redirect("https://lvfs.readthedocs.io/en/latest/privacy.html", code=302)


@bp_docs.route("/users")
@cache.cached()
def route_users() -> Any:
    return render_template("docs-users.html")


@bp_docs.route("/lvfs/news")
def route_news() -> Any:
    return redirect("https://lvfs.readthedocs.io/en/latest/news.html", code=302)


@bp_docs.route("/vendors")
@cache.cached()
def route_vendors() -> Any:
    return render_template("docs-vendors.html")


@bp_docs.route("/metainfo/version")
@cache.cached()
def route_metainfo_version() -> Any:
    verfmts = db.session.query(Verfmt).order_by(Verfmt.verfmt_id.asc()).all()
    return render_template(
        "docs-metainfo-version.html", category="documentation", verfmts=verfmts
    )


@bp_docs.route("/metainfo/protocol")
@cache.cached()
def route_metainfo_protocol() -> Any:
    protocols = db.session.query(Protocol).order_by(Protocol.protocol_id.asc()).all()
    return render_template(
        "docs-metainfo-protocol.html", category="documentation", protocols=protocols
    )


@bp_docs.route("/composite")
@bp_docs.route("/metainfo")
@bp_docs.route("/metainfo/category")
@bp_docs.route("/metainfo/intro")
@bp_docs.route("/metainfo/restrict")
@bp_docs.route("/metainfo/style")
@bp_docs.route("/metainfo/urls")
def route_metainfo() -> Any:
    return redirect("https://lvfs.readthedocs.io/en/latest/metainfo.html", code=302)


@bp_docs.route("/archive")
@bp_docs.route("/affiliates")
def route_archive() -> Any:
    return redirect("https://lvfs.readthedocs.io/en/latest/upload.html", code=302)


@bp_docs.route("/telemetry")
def route_telemetry() -> Any:
    return redirect("https://lvfs.readthedocs.io/en/latest/telemetry.html", code=302)


@bp_docs.route("/introduction")
def route_introduction() -> Any:
    return redirect("https://lvfs.readthedocs.io/en/latest/intro.html", code=302)


@bp_docs.route("/agreement")
@cache.cached()
def route_agreement() -> Any:
    try:
        agreement = db.session.query(Agreement).order_by(Agreement.version.desc()).one()
    except NoResultFound:
        flash("No argreements created", "danger")
        return redirect(url_for("main.route_index_uncached"))
    return render_template(
        "docs-agreement.html", category="documentation", agreement=agreement
    )


@bp_docs.route("/consulting")
@cache.cached()
def route_consulting() -> Any:
    vendors = (
        db.session.query(Vendor)
        .filter(Vendor.consulting_text != None)
        .order_by(Vendor.display_name.desc())
        .all()
    )
    return render_template(
        "docs-consulting.html", category="documentation", vendors=vendors
    )


@bp_docs.route("/consulting/info")
@cache.cached()
def route_consulting_info() -> Any:
    return render_template("docs-consulting-info.html", category="documentation")
