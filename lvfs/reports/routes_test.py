#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position,singleton-comparison

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_reports_signed(self, _app, client):

        # upload a firmware that can receive a report
        self.login()
        self.upload(target="testing", fwchecks=False)

        # send empty
        rv = client.post("/lvfs/firmware/report")
        assert b"No data" in rv.data, rv.data.decode()

        # a signed report that does not exist for user -- invalid is ignored
        rv = self._report(signed=True, signature_valid=False)
        assert b'"success": true' in rv.data, rv.data.decode()

        # set certificate for user
        self._add_certificate()

        # send a normal unsigned report
        rv = self._report()
        assert b'"success": true' in rv.data, rv.data.decode()

        rv = client.get("/lvfs/reports/1")
        assert b"SystemIntegrityOld" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/firmware/1/tests")
        assert b"CHANGED changed" in rv.data, rv.data.decode()
        assert b"OLD was removed" in rv.data, rv.data.decode()
        assert b"NEW was added" in rv.data, rv.data.decode()

        # send a valid signed report which replaces the unsigned one
        rv = self._report(signed=True)
        assert b"serial matched" in rv.data, rv.data.decode()
        assert b'"success": true' in rv.data, rv.data.decode()

        # check we detected the user correctly
        rv = client.get("/lvfs/reports/1")
        assert b"Acme" in rv.data, rv.data.decode()

        # send an invalid signed report
        rv = self._report(signed=True, signature_valid=False)
        assert b"Signature did not validate" in rv.data, rv.data.decode()

        # check the credit was shown on the overview page
        rv = client.get("/lvfs/firmware/1")
        assert b"verified by these vendors" in rv.data, rv.data.decode()
        assert b"Fedora 27" in rv.data, rv.data.decode()

    def test_reports(self, _app, client):

        # upload a firmware that can receive a report
        self.login()
        self.upload(target="testing")

        # send empty
        rv = client.post("/lvfs/firmware/report")
        assert b"No data" in rv.data, rv.data.decode()

        # self less than what we need
        rv = client.post("/lvfs/firmware/report", data='{"MachineId" : "abc"}')
        assert b"invalid data, expected ReportVersion" in rv.data, rv.data.decode()

        # send a valid report for firmware that is not known to us
        rv = self._report(checksum="c0243a8553f19d3c405004d3642d1485a723c948")
        assert (
            b"c0243a8553f19d3c405004d3642d1485a723c948 did not match any known firmware archive"
            in rv.data
        ), rv.data

        # send a valid report for firmware that is known
        rv = self._report(updatestate=3)
        assert b'"success": true' in rv.data, rv.data.decode()
        assert b"replaces old report" not in rv.data, rv.data.decode()

        # send an update
        rv = self._report()
        assert b'"success": true' in rv.data, rv.data.decode()
        assert b"replaces old report" in rv.data, rv.data.decode()

        # get a report that does not exist
        rv = client.get("/lvfs/reports/123456")
        assert b"Report does not exist" in rv.data, rv.data.decode()

        # check the saved report
        rv = client.get("/lvfs/reports/1")
        assert b'UpdateState": "success' in rv.data, rv.data.decode()

        # download the firmware at least once
        self.run_task_worker()
        self._download_firmware()

        # delete the report
        rv = client.post("/lvfs/reports/1/delete", follow_redirects=True)
        assert b"Deleted report" in rv.data, rv.data.decode()

        # check it is really deleted
        rv = client.get("/lvfs/reports/1")
        assert b"Report does not exist" in rv.data, rv.data.decode()


if __name__ == "__main__":
    unittest.main()
