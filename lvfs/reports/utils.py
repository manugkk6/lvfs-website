#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison,unused-argument

import datetime
import json

from flask import render_template
from flask import current_app as app

from lvfs import db

from lvfs.emails import send_email_sync
from lvfs.firmware.models import FirmwareEvent, FirmwareEventKind, Firmware
from lvfs.metadata.models import Remote
from lvfs.users.models import User
from lvfs.tasks.models import Task

from .models import Report


def _demote_back_to_testing(fw: Firmware, task: Task) -> None:

    # only on the production instance
    if app.config["RELEASE"] != "production":
        return

    # from the server admin
    user = db.session.query(User).filter(User.username == "anon@fwupd.org").first()
    if not user:
        return

    # send email to uploading user
    if fw.user.get_action("notify-demote-failures"):
        send_email_sync(
            "[LVFS] Firmware has been demoted",
            fw.user.email_address,
            render_template("email-firmware-demote.txt", user=fw.user, fw=fw),
            task=task,
        )

    # asynchronously sign straight away, even public remotes
    remote = db.session.query(Remote).filter(Remote.name == "testing").first()
    for r in set([remote, fw.remote]):
        db.session.add(
            Task(
                value=json.dumps({"id": r.remote_id}),
                caller=__name__,
                user=task.user,
                function="lvfs.metadata.utils.task_regenerate_remote",
            )
        )
    fw.events.append(
        FirmwareEvent(
            kind=FirmwareEventKind.DEMOTED.value,
            remote_old=fw.remote,
            remote=remote,
            user=user,
        )
    )
    fw.remote = remote
    task.add_fail(
        "Demoted firmware {}".format(fw.firmware_id),
        "Reported success {}%".format(fw.success),
    )
    db.session.commit()


def _generate_stats_firmware_reports(fw: Firmware, task: Task) -> None:

    # count how many times any of the firmware files were downloaded
    reports_success = 0
    reports_failure = 0
    reports_issue = 0
    for r in db.session.query(Report).filter(
        Report.firmware_id == fw.firmware_id,
        Report.timestamp > datetime.date.today() - datetime.timedelta(weeks=26),
    ):
        if r.state == 2:
            reports_success += 1
        if r.state == 3:
            if r.issue_id:
                reports_issue += 1
            else:
                reports_failure += 1

    # update
    fw.report_success_cnt = reports_success
    fw.report_failure_cnt = reports_failure
    fw.report_issue_cnt = reports_issue

    # check the limits and demote back to embargo if required
    if fw.remote.name == "stable" and fw.is_failure:
        _demote_back_to_testing(fw, task=task)


def _regenerate_reports(task: Task) -> None:

    # update FirmwareReport counts
    for (firmware_id,) in (
        db.session.query(Firmware.firmware_id)
        .join(Remote)
        .filter(Remote.name != "deleted")
        .order_by(Firmware.firmware_id.asc())
    ):
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=Firmware)
            .one()
        )
        _generate_stats_firmware_reports(fw, task=task)
    db.session.commit()


def task_regenerate(task: Task) -> None:
    _regenerate_reports(task)
