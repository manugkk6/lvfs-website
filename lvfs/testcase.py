#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-many-public-methods,line-too-long,too-many-arguments
# pylint: disable=too-many-instance-attributes,wrong-import-position,import-outside-toplevel

import os
import sys
import glob
import datetime
import base64
import unittest
import tempfile
import subprocess
from typing import List, Any, Optional

from flask import g
from flask import current_app as app
import flask_unittest

from lvfs import db, create_app
from lvfs.dbutils import init_db

# allows us to run this from the project root
sys.path.append(os.path.realpath("."))


class LvfsTestCase(flask_unittest.AppClientTestCase):
    def create_app(self):

        # create app
        _app = create_app(
            {
                "SECRET_KEY": "foobarbaz",
                "SERVER_NAME": "localhost.localhost",
                "HOST_NAME": "localhost",
                "TICKET_URL": "http://foo.bar.baz/",
                "CDN_DOMAIN": "",
                "SQLALCHEMY_DATABASE_URI": "sqlite:///",
                "SQLALCHEMY_TRACK_MODIFICATIONS": False,
                "DEBUG": True,
                "USE_INSECURE_PASSWORD_HASH": True,
                "DEBUG_TB_ENABLED": False,
                "DEBUG_TB_INTERCEPT_REDIRECTS": False,
                "DOWNLOAD_DIR": "/tmp",
                "UPLOAD_DIR": "/tmp",
                "SHARD_DIR": "/tmp",
                "SECRET_ADDR_SALT": "addr%%%",
                "SECRET_VENDOR_SALT": "vendor%%%'",
                "MAIL_SUPPRESS_SEND": True,
                "FLASH_USE_EVENTLOG": True,
                "WTF_CSRF_CHECK_DEFAULT": False,
                "RELEASE": "production",
                "CACHE_TYPE": "NullCache",
            }
        )

        # create the database and load test data
        with _app.app_context():
            init_db(db)

            # ensure the plugins settings are set up
            self.login()
            with _app.test_client() as client:
                client.get("/lvfs/settings/create")
                client.post("/lvfs/agreements/create")
                client.post("/lvfs/agreements/1/accept")
                rv = client.post(
                    "/lvfs/settings/modify",
                    data={
                        "clamav_enable": "disabled",
                        "virustotal_enable": "disabled",
                        "uefi_extract_size_min": "0",
                    },
                    follow_redirects=True,
                )
                assert b"Updated settings" in rv.data, rv.data.decode()
            self.logout()
            yield _app
            db.drop_all()

    def setUp(self, _app, _client):

        # global checksums
        self.checksum_signed_sha1 = None
        self.checksum_signed_sha256 = None
        self.checksum_upload_sha1 = None
        self.checksum_upload_sha256 = None

        # ensure firmware files are not present
        for fn in glob.glob(os.path.join(app.config["DOWNLOAD_DIR"], "*.cab")):
            os.remove(fn)

    def clear_cache(self) -> None:
        from lvfs import cache

        cache.clear()

    def _login(self, username: str, password: str = "Pa$$w0rd") -> Any:
        with app.test_client() as client:
            return client.post(
                "/lvfs/login",
                data={"username": username, "password": password},
                follow_redirects=True,
            )

    def _logout(self) -> Any:
        with app.test_client() as client:
            return client.get("/lvfs/logout", follow_redirects=True)

    def login(
        self,
        username: str = "sign-test@fwupd.org",
        password: str = "Pa$$w0rd",
        accept_agreement: bool = True,
    ) -> None:
        rv = self._login(username, password)
        assert b"/lvfs/upload/firmware" in rv.data, rv.data.decode()
        assert b"Incorrect username" not in rv.data, rv.data.decode()
        if accept_agreement and username != "sign-test@fwupd.org":
            with app.test_client() as client:
                rv = client.post("/lvfs/agreements/1/accept", follow_redirects=True)
            assert b"Recorded acceptance of the agreement" in rv.data, rv.data.decode()

    def logout(self) -> None:
        rv = self._logout()
        assert b"Logged out" in rv.data, rv.data.decode()
        assert b"/lvfs/upload/firmware" not in rv.data, rv.data.decode()

    def delete_firmware(self, firmware_id: int = 1) -> None:
        with app.test_client() as client:
            rv = client.post(
                "/lvfs/firmware/%i/delete" % firmware_id, follow_redirects=True
            )
        assert b"Firmware deleted" in rv.data, rv.data.decode()

    def _add_user(self, username: str, group_id: str, password: str) -> Any:
        with app.test_client() as client:
            return client.post(
                "/lvfs/users/create",
                data={
                    "username": username,
                    "password_new": password,
                    "group_id": group_id,
                    "display_name": "Generic Name",
                },
                follow_redirects=True,
            )

    def add_user(
        self,
        username: str = "testuser@fwupd.org",
        group_id: str = "testgroup",
        password: str = "Pa$$w0rd",
        is_qa: bool = False,
        is_analyst: bool = False,
        is_vendor_manager: bool = False,
        is_approved_public: bool = False,
        is_robot: bool = False,
        is_researcher: bool = False,
        is_translator: bool = False,
    ) -> None:
        rv = self._add_user(username, group_id, password)
        assert b"Added user" in rv.data, rv.data.decode()
        user_id_idx = rv.data.decode("utf-8").find("Added user ")
        assert user_id_idx != -1, rv.data
        user_id = int(rv.data[user_id_idx + 11 : user_id_idx + 12])
        assert user_id != 0, rv.data
        if is_qa or is_analyst or is_vendor_manager or is_approved_public or is_robot:
            data = {"auth_type": "local"}
            if is_qa:
                data["qa"] = "1"
            if is_analyst:
                data["analyst"] = "1"
            if is_vendor_manager:
                data["vendor-manager"] = "1"
            if is_researcher:
                data["researcher"] = "1"
            if is_translator:
                data["translator"] = "1"
            if is_approved_public:
                data["approved-public"] = "1"
            if is_robot:
                data["robot"] = "1"
            with app.test_client() as client:
                rv = client.post(
                    "/lvfs/users/%i/modify_by_admin" % user_id,
                    data=data,
                    follow_redirects=True,
                )
            assert b"Updated profile" in rv.data, rv.data.decode()

    def _upload(
        self,
        filename: str = "contrib/hughski-colorhug2-2.0.3.cab",
        target: str = "private",
        vendor_id: Optional[int] = None,
    ) -> Any:
        with open(filename, "rb") as fd:
            data = {"target": target, "file": (fd, filename)}
            if vendor_id:
                data["vendor_id"] = str(vendor_id)
            with app.test_client() as client:
                return client.post(
                    "/lvfs/upload/firmware", data=data, follow_redirects=True
                )

    def _ensure_checksums_from_upload(self) -> None:
        # peek into the database to get the checksums
        from lvfs.firmware.models import Firmware

        fw = db.session.query(Firmware).first()
        if fw:
            self.checksum_signed_sha1 = fw.checksum_signed_sha1
            self.checksum_signed_sha256 = fw.checksum_signed_sha256
            self.checksum_upload_sha1 = fw.checksum_upload_sha1
            self.checksum_upload_sha256 = fw.checksum_upload_sha256

    def token_create(self) -> Optional[str]:

        with app.test_client() as client:
            rv = client.post("/lvfs/users/token/create", follow_redirects=True)
        assert b"Added token" in rv.data, rv.data.decode()

        # peek into the database to get the token
        from lvfs.users.models import User

        user = db.session.query(User).first()
        return base64.b64encode(
            "{}:{}".format(user.username, user.tokens[0].value).encode()
        ).decode()

    def upload(
        self,
        target: str = "private",
        vendor_id: Optional[int] = None,
        filename: str = "contrib/hughski-colorhug2-2.0.3.cab",
        fwchecks: bool = True,
    ) -> None:
        rv = self._upload(filename, target, vendor_id)
        assert b"Uploaded file" in rv.data, rv.data.decode()
        self._ensure_checksums_from_upload()
        assert self.checksum_upload_sha256 is not None
        assert self.checksum_upload_sha256.encode("utf-8") in rv.data, rv.data.decode()
        if fwchecks:
            self.run_task_worker()

    def _download_firmware(self, useragent: str = "fwupd/1.5.0") -> None:
        assert self.checksum_signed_sha256 is not None
        with app.test_client() as client:
            rv = client.get(
                "/downloads/"
                + self.checksum_signed_sha256
                + "-hughski-colorhug2-2.0.3.cab",
                environ_base={"HTTP_USER_AGENT": useragent},
            )
        assert rv.status_code == 200, rv.status_code
        assert len(rv.data) < 20000, len(rv.data)

    def run_task_worker(self) -> None:

        from lvfs.tasks.utils import task_run_all

        with app.test_request_context():
            task_run_all()
            db.session.commit()

        # signing checksum has now changed
        self._ensure_checksums_from_upload()
        self.clear_cache()

    @staticmethod
    def run_cron_stats() -> None:

        from lvfs.main.utils import task_regenerate as _regenerate_metrics
        from lvfs.reports.utils import _regenerate_reports
        from lvfs.analytics.utils import _generate_stats_for_datestr
        from lvfs.shards.utils import _regenerate_shard_infos
        from lvfs.users.models import User
        from lvfs.util import _get_datestr_from_datetime
        from lvfs.tasks.models import Task

        with app.test_request_context():
            g.user = (
                db.session.query(User).filter(User.username == "anon@fwupd.org").one()
            )
            task = Task(user=g.user, function="lvfs.testcase", caller=__name__)
            db.session.add(task)
            _generate_stats_for_datestr(
                _get_datestr_from_datetime(datetime.datetime.now()), task=task
            )
            _regenerate_metrics(task=task)
            _regenerate_reports(task=task)
            _regenerate_shard_infos(task=task)
            db.session.commit()

    def add_vendor(self, group_id: str) -> None:
        with app.test_client() as client:
            rv = client.post(
                "/lvfs/vendors/create",
                data={"group_id": group_id},
                follow_redirects=True,
            )
        assert b"Added vendor" in rv.data, rv.data.decode()

    def _report(
        self,
        updatestate: int = 2,
        distro_id: str = "fedora",
        checksum: Optional[str] = None,
        signed: bool = False,
        signature_valid: bool = True,
    ) -> Any:
        if not checksum:
            checksum = self.checksum_signed_sha1
        payload = """
{
  "ReportVersion" : 2,
  "MachineId" : "abc",
  "Metadata" : {
    "DistroId" : "%s",
    "DistroVersion" : "27",
    "DistroVariant" : "workstation"
  },
  "Reports" : [
    {
      "Checksum" : "%s",
      "UpdateState" : %i,
      "UpdateError" : "UEFI firmware update failed: failed to make /boot/efi/EFI/arch/fw: No such file or directory",
      "Guid" : "7514fc4b0e1a306337de78c58f10e9e68f791de2",
      "Plugin" : "colorhug",
      "VersionOld" : "2.0.0",
      "VersionNew" : "2.0.3",
      "Flags" : 34,
      "Created" : 1518212684,
      "Modified" : 1518212754,
      "Metadata" : {
        "SystemIntegrityOld" : "CHANGED=919\\nUEFI=3f81\\nOLD=456\\nINVALID",
        "SystemIntegrityNew" : "CHANGED=920\\nUEFI=3f81\\nNEW=123",
        "AppstreamGlibVersion" : "0.7.5",
        "CpuArchitecture" : "x86_64",
        "FwupdVersion" : "1.0.5",
        "GUsbVersion" : "0.2.11",
        "BootTime" : "1518082325",
        "KernelVersion" : "4.14.16-300.fc27.x86_64"
      }
    }
  ]
} """ % (
            distro_id,
            checksum,
            updatestate,
        )

        # legacy
        if not signed:
            with app.test_client() as client:
                return client.post("/lvfs/firmware/report", data=payload)

        # signed
        if signature_valid:
            if "LVFS_RECREATE_CERT" in os.environ:
                with tempfile.NamedTemporaryFile(
                    mode="wb", prefix="pkcs7_", suffix=".cab", dir=None, delete=True
                ) as dat:
                    dat.write(payload.encode("utf8"))
                    dat.flush()
                    argv = [
                        "sudo",
                        "certtool",
                        "--p7-detached-sign",
                        "--p7-time",
                        "--no-p7-include-cert",
                        "--load-certificate",
                        "contrib/client.pem",
                        "--load-privkey",
                        "contrib/secret.key",
                        "--infile",
                        dat.name,
                        "--outfile",
                        "contrib/test.p7b",
                    ]
                    with subprocess.Popen(
                        argv, stdout=subprocess.PIPE, stderr=subprocess.PIPE
                    ) as ps:
                        _, err = ps.communicate()
                        if ps.returncode != 0:
                            raise IOError(err)
            with open("contrib/test.p7b", "rb") as f:
                signature = f.read().decode("utf8")
        else:
            # signing some crazy thing
            signature = """
-----BEGIN PKCS7-----
MIICYgYJKoZIhvcNAQcCoIICUzCCAk8CAQExDTALBglghkgBZQMEAgEwCwYJKoZI
hvcNAQcBMYICLDCCAigCAQEwGDAAAhRfEaI3uZSTG774ab0BUyNYdPqPizALBglg
hkgBZQMEAgGgaTAYBgkqhkiG9w0BCQMxCwYJKoZIhvcNAQcBMBwGCSqGSIb3DQEJ
BTEPFw0xOTAzMTAxNzU1MjBaMC8GCSqGSIb3DQEJBDEiBCA5srgWrdr1OV1vxW0m
Q7zNvoxFlVkp1mrAIrVQCpUekzANBgkqhkiG9w0BAQEFAASCAYADec2WR6u++SnR
zNwZ44reU3TaVe2zCELE07aPN9pAk9V3ez8ZSGRSKo2hPWl5wzqBP8rGv4D/vTgr
r+42OHYoiAQ4113D1adMHvAmyPt20yzrWAP138C2ajeX1m7vDT0guLlEBoJPigB7
6nVYa1LCM9+EpJI+JrAEIXXTeuXIIeEROu0vvGrg1uvQeLg5ZdvqJUfbs0/fD29R
LEgNMCeQo0yqGx+511hQDybQnx1pNtSUTTsQ6o5h6W8ELLD924C0Yqd3bRf5JOdm
qWhfysGJNGlQubM4nyjks+9b5DPiZWxNdsUE+l9xQZc4gR+wJg3dfocbZ6kfo/pI
Dbskni3KiRc13+HmUBdbjhdLWYS4hirSVuyZ2n8UjIS4Pp/S2cPDe47YJwCbOn97
WmOPP+2xuvr/sTV8AAbcAZgK2TBBVUjZMJeCBcLIba8O9mJJVHE4I1PzXcf+l6D7
ma+I7fM5pmgsEL4tkCZAg0+CPTyhHkMV/cWuOZUjqTsYbDq1pZI=
-----END PKCS7-----
"""
        data = {
            "payload": payload,
            "signature": signature,
        }
        with app.test_client() as client:
            return client.post(
                "/lvfs/firmware/report", data=data, content_type="multipart/form-data"
            )

    def add_affiliation(
        self,
        vendor_id_oem: int,
        vendor_id_odm: int,
        default_actions: bool = True,
        actions: Optional[List[str]] = None,
    ) -> None:
        with app.test_client() as client:
            rv = client.post(
                "/lvfs/vendors/%u/affiliation/create" % vendor_id_oem,
                data={"vendor_id_odm": vendor_id_odm},
                follow_redirects=True,
            )
        assert b"Added affiliation" in rv.data, rv.data.decode()

        # get the affiliation ID
        text = rv.data.decode()
        idx = text.find("Added affiliation")
        aff_id = text[idx + 18 : idx + 19]
        assert int(aff_id) > 0
        assert int(aff_id) < 9

        # default set
        actions_all: List[str] = []
        if actions:
            for act in actions:
                actions_all.append(act)
        if default_actions:
            actions_all.append("@modify-limit")
        with app.test_client() as client:
            for act in actions_all:
                rv = client.post(
                    "/lvfs/vendors/{}/affiliation/{}/action/create/{}".format(
                        vendor_id_oem, aff_id, act
                    ),
                    follow_redirects=True,
                )
                assert b"Added action" in rv.data, rv.data.decode()

    def add_namespace(self, vendor_id: int = 1, value: str = "com.hughski") -> None:
        with app.test_client() as client:
            rv = client.post(
                "/lvfs/vendors/{}/namespace/create".format(vendor_id),
                data={"value": value},
                follow_redirects=True,
            )
        assert b"Added namespace" in rv.data, rv.data.decode()

    def add_issue(
        self,
        issue_id: int = 1,
        url: str = "https://github.com/fwupd/fwupd/wiki/Arch-Linux",
        name: str = "ColorHug on Fedora",
    ) -> None:

        with app.test_client() as client:
            # create an issue
            rv = client.post(
                "/lvfs/issues/create",
                data={
                    "url": url,
                },
                follow_redirects=True,
            )
            assert b"Added issue" in rv.data, rv.data.decode()
            rv = client.get("/lvfs/issues/")
            assert url in rv.data.decode("utf-8"), rv.data
            rv = client.get("/lvfs/issues/%i/details" % issue_id, follow_redirects=True)
            assert url in rv.data.decode("utf-8"), rv.data

            # modify the description
            data = {"name": name, "description": "Matches updating ColorHug on Fedora"}
            rv = client.post(
                "/lvfs/issues/%i/modify" % issue_id, data=data, follow_redirects=True
            )
            assert name in rv.data.decode("utf-8"), rv.data
            assert b"Matches updating ColorHug on Fedora" in rv.data, rv.data.decode()

    def _enable_issue(self, issue_id: int = 1) -> Any:
        with app.test_client() as client:
            return client.post(
                "/lvfs/issues/%i/modify" % issue_id,
                data={
                    "enabled": True,
                },
                follow_redirects=True,
            )

    def enable_issue(self, issue_id: int = 1) -> None:
        rv = self._enable_issue(issue_id)
        assert b"Modified issue" in rv.data, rv.data.decode()

    def _add_issue_condition(
        self,
        issue_id: int = 1,
        key: str = "DistroId",
        value: str = "fedora",
        compare: str = "eq",
    ) -> Any:
        data = {
            "key": key,
            "value": value,
            "compare": compare,
        }
        with app.test_client() as client:
            return client.post(
                "/lvfs/issues/%i/condition/create" % issue_id,
                data=data,
                follow_redirects=True,
            )

    def add_issue_condition(self, issue_id: int = 1) -> None:
        rv = self._add_issue_condition(issue_id)
        assert b"Added condition" in rv.data, rv.data.decode()

    def _add_certificate(self, filename: str = "contrib/client.pem") -> Any:
        with open(filename, "rb") as fd:
            data = {"file": (fd, filename)}
            with app.test_client() as client:
                return client.post(
                    "/lvfs/users/certificate/create", data=data, follow_redirects=True
                )

        self.logout()

    def _get_token_from_eventlog(self, token_before: str) -> Optional[str]:
        self.login()
        with app.test_client() as client:
            rv = client.get("/lvfs/tasks/")
        self.logout()
        found_token = False
        for tok in rv.data.decode("utf-8").replace("\\n", "\n").split():
            if found_token:
                return tok
            if tok == token_before:
                found_token = True
        return None


if __name__ == "__main__":
    unittest.main()
