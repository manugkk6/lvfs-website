#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison,unused-argument

import datetime
from collections import defaultdict
from typing import Dict, Optional, Tuple

from lvfs import db

from lvfs.util import _get_datestr_from_datetime

from lvfs.firmware.models import Firmware
from lvfs.main.models import Client
from lvfs.metadata.models import Remote
from lvfs.vendors.models import Vendor
from lvfs.tasks.models import Task

from .models import (
    Analytic,
    AnalyticFirmware,
    AnalyticUseragent,
    AnalyticUseragentKind,
    AnalyticVendor,
)


def _generate_stats_for_vendor(v: Vendor, datestr: int, task: Task) -> None:

    # is datestr older than firmware
    if not v.ctime:
        return
    if datestr < _get_datestr_from_datetime(v.ctime - datetime.timedelta(days=1)):
        return

    # get all the firmware for a specific vendor
    fw_ids = [fw.firmware_id for fw in v.fws]
    if not fw_ids:
        return

    # count how many times any of the firmware files were downloaded
    cnt = (
        db.session.query(Client.id)
        .filter(Client.firmware_id.in_(fw_ids))
        .filter(Client.datestr == datestr)
        .count()
    )
    analytic = AnalyticVendor(vendor_id=v.vendor_id, datestr=datestr, cnt=cnt)
    task.add_pass("adding {}:{} = {}".format(datestr, v.group_id, cnt))
    db.session.add(analytic)


def _generate_stats_for_firmware(fw: Firmware, datestr: int) -> None:

    # is datestr older than firmware
    if datestr < _get_datestr_from_datetime(fw.timestamp):
        return

    # count how many times any of the firmware files were downloaded
    cnt = (
        db.session.query(Client.id)
        .filter(Client.firmware_id == fw.firmware_id)
        .filter(Client.datestr == datestr)
        .count()
    )
    analytic = AnalyticFirmware(firmware_id=fw.firmware_id, datestr=datestr, cnt=cnt)
    db.session.add(analytic)


def _get_app_from_ua(ua: str) -> str:
    # always exists
    return ua.split(" ")[0]


def _get_fwupd_from_ua(ua: str) -> str:
    for part in ua.split(" "):
        if part.startswith("fwupd/"):
            return part[6:]
    return "Unknown"


def _get_lang_distro_from_ua(ua: str) -> Optional[Tuple[str, str]]:
    start = ua.find("(")
    end = ua.rfind(")")
    if start == -1 or end == -1:
        return None
    parts = ua[start + 1 : end].split("; ")
    if len(parts) != 3:
        return None
    return (parts[1], parts[2])


def _generate_stats_for_datestr(datestr: int, task: Task) -> None:

    # update AnalyticVendor
    db.session.query(AnalyticVendor).filter(AnalyticVendor.datestr == datestr).delete()
    db.session.commit()
    for v in db.session.query(Vendor):
        _generate_stats_for_vendor(v, datestr, task=task)
    db.session.commit()

    # update AnalyticFirmware
    db.session.query(AnalyticFirmware).filter(
        AnalyticFirmware.datestr == datestr
    ).delete()
    db.session.commit()
    for (firmware_id,) in (
        db.session.query(Firmware.firmware_id)
        .join(Remote)
        .filter(Remote.name != "deleted")
    ):
        fw = (
            db.session.query(Firmware)
            .filter(Firmware.firmware_id == firmware_id)
            .with_for_update(of=Firmware)
            .one()
        )
        _generate_stats_for_firmware(fw, datestr)
        db.session.commit()

    # update AnalyticUseragent
    db.session.query(AnalyticUseragent).filter(
        AnalyticUseragent.datestr == datestr
    ).delete()
    db.session.commit()
    ua_apps: Dict[str, int] = defaultdict(lambda: 1)
    ua_fwupds: Dict[str, int] = defaultdict(lambda: 1)
    ua_distros: Dict[str, int] = defaultdict(lambda: 1)
    ua_langs: Dict[str, int] = defaultdict(lambda: 1)
    country_codes: Dict[str, int] = defaultdict(lambda: 1)
    clients = (
        db.session.query(Client.user_agent, Client.country_code)
        .filter(Client.datestr == datestr)
        .all()
    )
    for (
        ua,
        cc,
    ) in clients:

        # user agent, so app, fwupd version, language and distro
        if ua:
            ua_apps[_get_app_from_ua(ua)] += 1
            ua_fwupds[_get_fwupd_from_ua(ua)] += 1
            ua_lang_distro = _get_lang_distro_from_ua(ua)
            if ua_lang_distro:
                ua_langs[ua_lang_distro[0]] += 1
                ua_distros[ua_lang_distro[1]] += 1

        # country code
        if cc:
            country_codes[cc] += 1

    for ua in ua_apps:
        db.session.add(
            AnalyticUseragent(
                kind=int(AnalyticUseragentKind.APP),
                value=ua,
                datestr=datestr,
                cnt=ua_apps[ua],
            )
        )
    for ua in ua_fwupds:
        db.session.add(
            AnalyticUseragent(
                kind=int(AnalyticUseragentKind.FWUPD),
                value=ua,
                datestr=datestr,
                cnt=ua_fwupds[ua],
            )
        )
    for ua in ua_langs:
        db.session.add(
            AnalyticUseragent(
                kind=int(AnalyticUseragentKind.LANG),
                value=ua,
                datestr=datestr,
                cnt=ua_langs[ua],
            )
        )
    for ua in ua_distros:
        db.session.add(
            AnalyticUseragent(
                kind=int(AnalyticUseragentKind.DISTRO),
                value=ua,
                datestr=datestr,
                cnt=ua_distros[ua],
            )
        )
    for cc in country_codes:
        db.session.add(
            AnalyticUseragent(
                kind=int(AnalyticUseragentKind.COUNTRY),
                value=cc,
                datestr=datestr,
                cnt=country_codes[cc],
            )
        )
    db.session.commit()

    # update Analytic
    db.session.query(Analytic).filter(Analytic.datestr == datestr).delete()
    db.session.commit()
    db.session.add(Analytic(datestr=datestr, cnt=len(clients)))
    db.session.commit()

    # for the log
    task.add_pass("generated for {}".format(datestr))


def task_generate(task: Task) -> None:
    datestr = _get_datestr_from_datetime(
        datetime.datetime.today() - datetime.timedelta(days=1)
    )
    _generate_stats_for_datestr(datestr, task=task)


def task_generate_all(task: Task) -> None:
    now = datetime.datetime.today() - datetime.timedelta(days=1)
    for _ in range(30):
        _generate_stats_for_datestr(_get_datestr_from_datetime(now), task=task)
        now -= datetime.timedelta(days=1)
