#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=singleton-comparison,too-few-public-methods

import datetime
from collections import defaultdict
from typing import Dict, List, Tuple, Optional, Any

from sqlalchemy import and_

from flask import Blueprint, render_template, flash, redirect, url_for
from flask_login import login_required


from lvfs import db

from lvfs.analytics.models import Analytic
from lvfs.components.models import Component, ComponentShard, ComponentIssue
from lvfs.firmware.models import Firmware
from lvfs.main.models import Client
from lvfs.metadata.models import Remote
from lvfs.reports.models import Report
from lvfs.search.models import SearchEvent
from lvfs.reports.models import ReportAttribute

from lvfs.util import _get_datestr_from_datetime, _split_search_string
from lvfs.util import admin_login_required
from lvfs.util import _get_chart_labels_months, _get_chart_labels_days

from .models import (
    AnalyticUseragent,
    AnalyticUseragentKind,
    AnalyticVendor,
    AnalyticFirmware,
)

bp_analytics = Blueprint("analytics", __name__, template_folder="templates")

PALETTE = [
    "06c990",  # green
    "2f8ba0",  # teal
    "845f80",  # purple
    "ffd160",  # yellow
    "ee8510",  # orange
    "ef4760",  # red
]


def _get_chart_labels_hours(limit: int = 24) -> List[str]:
    """Gets the chart labels"""
    labels: List[str] = []
    for i in range(0, limit):
        labels.append("%02i" % i)
    return labels


class Dataset:
    def __init__(
        self, label: str = "Unknown", idx: Optional[int] = None, value: int = 0
    ):
        self.data: List[str] = []
        self.value = value
        self.label = label
        if idx is None:
            self.color = PALETTE[0]
        else:
            self.color = PALETTE[idx % len(PALETTE)]

    def append(self, val: Any) -> None:
        if val:
            self.data.append(str(val))
        else:
            self.data.append("null")

    def append_all(self, vals: List[Any]) -> None:
        for val in vals:
            self.append(val)


class Bubble:
    def __init__(self, x: float = 0.0, y: float = 0.0, r: float = 0.0) -> None:
        self.x: float = x
        self.y: float = y
        self.r: float = r

    def __str__(self) -> str:
        return str({"x": self.x, "y": self.y, "r": self.r})


@bp_analytics.route("/issues")
@login_required
@admin_login_required
def route_issues() -> Any:
    """show how long each issue takes to fix"""

    data: Dict[str, List[Component]] = defaultdict(list)
    points_per_vendor: Dict[str, Dict[str, Bubble]] = defaultdict(dict)
    for issue in (
        db.session.query(ComponentIssue)
        .filter(ComponentIssue.published != None)
        .join(Component)
        .join(Firmware)
        .join(Remote)
        .filter(Remote.name == "stable")
        .order_by(ComponentIssue.value.asc())
    ):
        data[issue].append(issue.md)

        # these are broken
        if issue.value_display in ["CVE-2020-8670", "CVE-2020-8672"]:
            continue
        ts = issue.md.fw.release_ts
        fpdate = round(
            ts.year + ((ts.month - 1) / 12.0) + ((ts.day - 1) / (30.0 * 12.0)), 3
        )
        if fpdate < 2019:
            continue

        # dedupe and add to radius
        key = "{}:{}".format(fpdate, issue.release_delta)
        bubbles = points_per_vendor[issue.md.fw.vendor.display_name]
        if key in bubbles:
            bubbles[key].r += 0.02
        else:
            bubbles[key] = Bubble(x=fpdate, y=issue.release_delta, r=3)

    datasets: List[Dataset] = []
    for idx, vendor in enumerate(sorted(points_per_vendor)):
        ds = Dataset(label=vendor, idx=idx)
        ds.append_all(list(points_per_vendor[vendor].values()))
        datasets.append(ds)

    return render_template(
        "analytics-issues.html",
        category="analytics",
        data=data,
        datasets=datasets,
    )


@bp_analytics.route("/microcode")
@login_required
@admin_login_required
def route_microcode() -> Any:
    """show microcode by year"""

    now = datetime.datetime.today()

    # query all public shards
    data: Dict[str, Dict[str, str]] = {}
    cnt_years: Dict[int, int] = defaultdict(int)
    for shard in (
        db.session.query(ComponentShard)
        .filter(ComponentShard.guid == "3f0229ad-0a00-5269-90cf-0a45d8781b72")
        .join(Component)
        .join(Firmware)
        .join(Remote)
        .filter(Remote.is_public)
        .order_by(Firmware.signed_timestamp.desc())
    ):

        # already added newer data
        if shard.md.appstream_id in data:
            continue

        # only add interesting data
        yyyymmdd = shard.get_attr_value("yyyymmdd")
        if not yyyymmdd:
            continue
        data[shard.md.appstream_id] = {
            "yyyymmdd": yyyymmdd,
            "version": shard.get_attr_value("version"),
        }
        year = int(yyyymmdd[:4])
        if year < now.year - 5:
            year = now.year - 5
        cnt_years[year] += 1

    # format into per-year datasets
    datasets: List[Dataset] = []
    for idx in range(6):
        year = now.year - idx
        label = str(year) if idx < 5 else "＜{}".format(now.year - 4)
        ds = Dataset(label=label, idx=idx, value=cnt_years[year])
        datasets.append(ds)

    return render_template(
        "analytics-microcode.html",
        category="analytics",
        data=data,
        datasets=datasets,
        cnt_years=cnt_years,
    )


@bp_analytics.route("/day")
@bp_analytics.route("/day/<int:offset>")
@bp_analytics.route("/day/<int:offset>/<int:limit>")
@login_required
@admin_login_required
def route_day(offset: int = 0, limit: int = 3) -> Any:
    """A analytics screen to show information about users"""
    now = datetime.datetime.today() - datetime.timedelta(days=offset)
    datasets: List[Dataset] = []
    for idx in range(limit):
        datestr = _get_datestr_from_datetime(now)
        ds = Dataset(label=str(datestr), idx=idx)
        data_tmp = [0] * 24
        for (ts,) in db.session.query(Client.timestamp).filter(
            Client.datestr == datestr
        ):
            data_tmp[ts.hour] += 1
        ds.append_all(data_tmp)
        datasets.append(ds)

        # back one day
        now -= datetime.timedelta(days=1)

    return render_template(
        "analytics-day.html",
        category="analytics",
        labels_days=_get_chart_labels_hours(),
        datasets=datasets,
    )


@bp_analytics.route("/")
@bp_analytics.route("/month")
@login_required
@admin_login_required
def route_month() -> Any:
    """A analytics screen to show information about users"""

    # this is somewhat klunky
    data: List[int] = []
    now = datetime.datetime.now() - datetime.timedelta(days=1)
    for _ in range(30):
        datestr = _get_datestr_from_datetime(now)
        analytic = (
            db.session.query(Analytic).filter(Analytic.datestr == datestr).first()
        )
        if analytic:
            data.append(int(analytic.cnt))
        else:
            data.append(0)

        # back one day
        now -= datetime.timedelta(days=1)

    return render_template(
        "analytics-month.html",
        category="analytics",
        labels_days=_get_chart_labels_days()[::-1],
        data_days=data[::-1],
    )


@bp_analytics.route("/year")
@bp_analytics.route("/year/<int:ts>")
@login_required
@admin_login_required
def route_year(ts: int = 3) -> Any:
    """A analytics screen to show information about users"""

    # this is somewhat klunky
    data: List[int] = []
    now = datetime.datetime.now() - datetime.timedelta(days=1)
    for _ in range(12 * ts):
        datestrold = _get_datestr_from_datetime(now)
        now -= datetime.timedelta(days=30)
        datestrnew = _get_datestr_from_datetime(now)
        analytics = (
            db.session.query(Analytic)
            .filter(Analytic.datestr < datestrold)
            .filter(Analytic.datestr > datestrnew)
            .all()
        )

        # sum up all the totals for each day in that month
        cnt = 0
        for analytic in analytics:
            cnt += analytic.cnt
        data.append(int(cnt))

    return render_template(
        "analytics-year.html",
        category="analytics",
        labels_months=_get_chart_labels_months(ts)[::-1],
        data_months=data[::-1],
    )


@bp_analytics.route("/devices")
@bp_analytics.route("/devices/<int:ts>")
@login_required
@admin_login_required
def route_devices(ts: int = 3) -> Any:
    """A analytics screen to show information about devices"""

    # this is somewhat klunky
    data: List[int] = []
    now = datetime.datetime.utcnow()
    for _ in range(12 * ts):
        cnt = (
            db.session.query(Component)
            .join(Firmware)
            .filter(Firmware.timestamp < now)
            .join(Remote)
            .filter(Remote.is_public)
            .distinct(Component.appstream_id)
            .count()
        )
        now -= datetime.timedelta(days=30)
        data.append(int(cnt))

    return render_template(
        "analytics-year.html",
        category="analytics",
        labels_months=_get_chart_labels_months(ts)[::-1],
        data_months=data[::-1],
    )


def _user_agent_wildcard(user_agent: str) -> str:
    tokens = user_agent.split("/")
    if len(tokens) != 2:
        return user_agent
    if tokens[0] == "Mozilla":
        return "browser"
    if tokens[0] == "Java":
        return "bot"
    if tokens[0] == "gnome-software":
        versplt = tokens[1].split(".")
        if len(versplt) == 2:
            return tokens[0] + " " + ".".join((versplt[0], "x"))
        if len(versplt) >= 3:
            return tokens[0] + " " + ".".join((versplt[0], versplt[1], "x"))
    return user_agent


@bp_analytics.route("/user_agent")
@bp_analytics.route("/user_agent/<kind>")
@bp_analytics.route("/user_agent/<kind>/<int:timespan_days>")
@bp_analytics.route("/user_agent/<kind>/<int:timespan_days>/<keys>")
@login_required
@admin_login_required
def route_user_agents(
    kind: str = "APP", timespan_days: int = 30, keys: Optional[str] = None
) -> Any:
    """A analytics screen to show information about users"""

    # map back to AnalyticUseragentKind
    try:
        kind_enum = AnalyticUseragentKind[kind]
    except KeyError as e:
        flash("Unable to view analytic type: {}".format(str(e)), "danger")
        return redirect(url_for("analytics.route_user_agents"))

    # get data for this time period
    cnt_total: Dict[str, int] = {}
    cached_cnt: Dict[str, int] = {}
    yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
    datestr_start = _get_datestr_from_datetime(
        yesterday - datetime.timedelta(days=timespan_days)
    )
    datestr_end = _get_datestr_from_datetime(yesterday)
    for ug in (
        db.session.query(AnalyticUseragent)
        .filter(AnalyticUseragent.kind == kind_enum.value)
        .filter(
            and_(
                AnalyticUseragent.datestr > datestr_start,
                AnalyticUseragent.datestr <= datestr_end,
            )
        )
    ):
        user_agent_safe = _user_agent_wildcard(ug.value)
        if kind == "FWUPD":
            splt = user_agent_safe.split(".", 3)
            if len(splt) == 3:
                user_agent_safe = "{}.{}.x".format(splt[0], splt[1])
        elif kind == "DISTRO":
            if user_agent_safe.startswith("Red Hat Enterprise Linux 7"):
                user_agent_safe = "RHEL 7"
            elif user_agent_safe.startswith("Red Hat Enterprise Linux 8"):
                user_agent_safe = "RHEL 8"
            elif user_agent_safe.startswith("Red Hat Enterprise Linux 9"):
                user_agent_safe = "RHEL 9"
        key = str(ug.datestr) + user_agent_safe
        if key not in cached_cnt:
            cached_cnt[key] = ug.cnt
        else:
            cached_cnt[key] += ug.cnt
        if not user_agent_safe in cnt_total:
            cnt_total[user_agent_safe] = ug.cnt
            continue
        cnt_total[user_agent_safe] += ug.cnt

    # find most popular user agent strings
    values: List[str] = []
    if keys:
        for key in keys.split(","):
            values.append(key)
    else:
        for key, _ in sorted(
            iter(cnt_total.items()), key=lambda k_v: (k_v[1], k_v[0]), reverse=True
        ):
            values.append(key)
            if len(values) >= 6:
                break

    # generate enough for the template
    datasets: List[Dataset] = []
    idx = 0
    for val in values:
        ds = Dataset(label=str(val), idx=idx)
        idx += 1
        data: List[str] = []
        for i in range(timespan_days):
            datestr = _get_datestr_from_datetime(yesterday - datetime.timedelta(days=i))
            key = str(datestr) + str(val)
            dataval = "NaN"
            if key in cached_cnt:
                dataval = str(cached_cnt[key])
            data.append(dataval)
        ds.data = data[::-1]
        datasets.append(ds)
    return render_template(
        "analytics-user-agent.html",
        category="analytics",
        kind=kind,
        labels_user_agent=_get_chart_labels_days(timespan_days)[::-1],
        datasets=datasets,
        cnt_total=cnt_total,
    )


@bp_analytics.route("/reportattrs")
@login_required
@admin_login_required
def route_reportattrs() -> Any:
    datestr_year = datetime.datetime.now() - datetime.timedelta(days=365)
    attrs = [
        attr
        for attr, in db.session.query(ReportAttribute.key)
        .distinct(ReportAttribute.key)
        .join(Report)
        .filter(Report.timestamp < datestr_year)
        .all()
    ]
    return render_template(
        "analytics-reportattrs.html", category="analytics", attrs=attrs
    )


@bp_analytics.route("/reportattrs/<kind>")
@bp_analytics.route("/reportattrs/<kind>/<int:timespan_days>")
@login_required
@admin_login_required
def route_reportattrs_kind(kind: str, timespan_days: int = 90) -> Any:
    """A analytics screen to show information about users"""

    # get data for this time period
    yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
    datestr_start = yesterday - datetime.timedelta(days=timespan_days)
    cnt_total: Dict[str, int] = {}
    cached_cnt: Dict[str, int] = {}
    for report_ts, attr_val in (
        db.session.query(Report.timestamp, ReportAttribute.value)
        .filter(ReportAttribute.key == kind)
        .join(Report)
        .filter(and_(Report.timestamp > datestr_start, Report.timestamp <= yesterday))
    ):

        key = str(_get_datestr_from_datetime(report_ts)) + attr_val
        if key not in cached_cnt:
            cached_cnt[key] = 1
        else:
            cached_cnt[key] += 1
        if not attr_val in cnt_total:
            cnt_total[attr_val] = 1
            continue
        cnt_total[attr_val] += 1

    # find most popular user agent strings
    most_popular: List[str] = []
    for key, _ in sorted(
        iter(cnt_total.items()), key=lambda k_v: (k_v[1], k_v[0]), reverse=True
    ):
        most_popular.append(key)
        if len(most_popular) >= 6:
            break

    # generate enough for the template
    datasets: List[Dataset] = []
    idx = 0
    for cnt in most_popular:
        ds = Dataset(label=cnt, idx=idx)
        idx += 1
        data: List[str] = []
        for i in range(timespan_days):
            datestr = _get_datestr_from_datetime(yesterday - datetime.timedelta(days=i))
            key = str(datestr) + str(cnt)
            dataval = "NaN"
            if key in cached_cnt:
                dataval = str(cached_cnt[key])
            data.append(dataval)
        ds.data = data[::-1]
        datasets.append(ds)
    return render_template(
        "analytics-reportattrs-kind.html",
        category="analytics",
        kind=kind,
        labels_user_agent=_get_chart_labels_days(timespan_days)[::-1],
        datasets=datasets,
    )


def _running_mean(l: List[int], points: int) -> List[int]:
    total: int = 0
    result: List[int] = list(0 for x in l)
    for i in range(0, points):
        total = total + l[i]
        result[i] = total / (i + 1)  # type: ignore
    for i in range(points, len(l)):
        total = total - l[i - points] + l[i]
        result[i] = int(total / points)
    return result


@bp_analytics.route("/vendor")
@bp_analytics.route("/vendor/<int:timespan_days>")
@bp_analytics.route("/vendor/<int:timespan_days>/<int:vendor_cnt>")
@bp_analytics.route("/vendor/<int:timespan_days>/<int:vendor_cnt>/<int:smoothing>")
@login_required
@admin_login_required
def route_vendor(
    timespan_days: int = 30, vendor_cnt: int = 6, smoothing: int = 0
) -> Any:
    """A analytics screen to show information about users"""

    # get data for this time period
    cnt_total: Dict[str, int] = defaultdict(int)
    cached_cnt: Dict[str, int] = defaultdict(int)
    yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
    datestr_start = _get_datestr_from_datetime(
        yesterday - datetime.timedelta(days=timespan_days)
    )
    datestr_end = _get_datestr_from_datetime(yesterday)
    for ug in db.session.query(AnalyticVendor).filter(
        and_(
            AnalyticVendor.datestr >= datestr_start,
            AnalyticVendor.datestr <= datestr_end,
        )
    ):
        display_name = ug.vendor.display_name
        key = str(ug.datestr) + display_name
        cached_cnt[key] += ug.cnt
        cnt_total[display_name] += ug.cnt

    # find most popular user agent strings
    most_popular: List[str] = []
    for key, _ in sorted(
        iter(cnt_total.items()), key=lambda k_v: (k_v[1], k_v[0]), reverse=True
    ):
        most_popular.append(key)
        if len(most_popular) >= vendor_cnt:
            break

    # optionally smooth
    if not smoothing:
        smoothing = int(timespan_days / 25)

    # generate enough for the template
    datasets: List[Dataset] = []
    idx = 0
    for cnt in sorted(most_popular):
        ds = Dataset(label=cnt, idx=idx)
        idx += 1
        data: List[int] = []
        for i in range(timespan_days, 0, -1):
            datestr = _get_datestr_from_datetime(yesterday - datetime.timedelta(days=i))
            key = str(datestr) + str(cnt)
            dataval = 0
            if key in cached_cnt:
                dataval = cached_cnt[key]
            data.append(dataval)
        if smoothing > 1:
            data = _running_mean(data, smoothing)
        ds.append_all(data)
        datasets.append(ds)

    return render_template(
        "analytics-vendor.html",
        category="analytics",
        labels_user_agent=_get_chart_labels_days(timespan_days)[::-1],
        datasets=datasets,
    )


@bp_analytics.route("/firmware")
@bp_analytics.route("/firmware/<int:timespan_days>")
@bp_analytics.route("/firmware/<int:timespan_days>/<int:firmware_cnt>")
@bp_analytics.route("/firmware/<int:timespan_days>/<int:firmware_cnt>/<int:smoothing>")
@login_required
@admin_login_required
def route_firmware(
    timespan_days: int = 30, firmware_cnt: int = 6, smoothing: int = 0
) -> Any:
    """A analytics screen to show information about users"""

    # get data for this time period
    cnt_total: Dict[str, int] = defaultdict(int)
    cached_cnt: Dict[str, int] = defaultdict(int)
    yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
    datestr_start = _get_datestr_from_datetime(
        yesterday - datetime.timedelta(days=timespan_days)
    )
    datestr_end = _get_datestr_from_datetime(yesterday)
    for ug in (
        db.session.query(AnalyticFirmware)
        .filter(AnalyticFirmware.cnt > 5)
        .filter(
            and_(
                AnalyticFirmware.datestr >= datestr_start,
                AnalyticFirmware.datestr <= datestr_end,
            )
        )
    ):
        try:
            display_name = ug.firmware.md_prio.names[0]
        except TypeError:
            continue
        key = str(ug.datestr) + display_name
        cached_cnt[key] += ug.cnt
        cnt_total[display_name] += ug.cnt

    # find most popular user agent strings
    most_popular: List[str] = []
    for key, _ in sorted(
        iter(cnt_total.items()), key=lambda k_v: (k_v[1], k_v[0]), reverse=True
    ):
        most_popular.append(key)
        if len(most_popular) >= firmware_cnt:
            break

    # optionally smooth
    if not smoothing:
        smoothing = int(timespan_days / 25)

    # generate enough for the template
    datasets: List[Dataset] = []
    idx = 0
    for cnt in sorted(most_popular):
        ds = Dataset(label=str(cnt), idx=idx)
        idx += 1
        data: List[int] = []
        for i in range(timespan_days, 0, -1):
            datestr = _get_datestr_from_datetime(yesterday - datetime.timedelta(days=i))
            key = str(datestr) + str(cnt)
            dataval = 0
            if key in cached_cnt:
                dataval = cached_cnt[key]
            data.append(dataval)
        if smoothing > 1:
            data = _running_mean(data, smoothing)
        ds.append_all(data)
        datasets.append(ds)

    return render_template(
        "analytics-firmware.html",
        category="analytics",
        labels_user_agent=_get_chart_labels_days(timespan_days)[::-1],
        datasets=datasets,
    )


@bp_analytics.route("/clients")
@login_required
@admin_login_required
def route_clients() -> Any:
    """A analytics screen to show information about users"""

    clients = db.session.query(Client).order_by(Client.timestamp.desc()).limit(25).all()
    return render_template(
        "analytics-clients.html", category="analytics", clients=clients
    )


@bp_analytics.route("/reports")
@bp_analytics.route("/reports/user/<int:user_id>")
@login_required
@admin_login_required
def route_reports(user_id: Optional[int] = None) -> Any:
    """A analytics screen to show information about users"""

    stmt = db.session.query(Report)
    if user_id is None:
        stmt = stmt.filter(Report.user_id == None)
    else:
        if user_id:
            stmt = stmt.filter(Report.user_id == user_id)
        else:
            stmt = stmt.filter(Report.user_id != None)
    reports = stmt.order_by(Report.timestamp.desc()).limit(25).all()
    return render_template(
        "analytics-reports.html",
        category="analytics",
        reports=reports,
        user_id=user_id,
    )


@bp_analytics.route("/search_history")
@login_required
@admin_login_required
def route_search_history() -> Any:
    search_events = (
        db.session.query(SearchEvent)
        .order_by(SearchEvent.timestamp.desc())
        .limit(1000)
        .all()
    )
    return render_template(
        "analytics-search-history.html",
        category="analytics",
        search_events=search_events,
    )


@bp_analytics.route("/search_stats")
@bp_analytics.route("/search_stats/<int:limit>")
@login_required
@admin_login_required
def route_search_stats(limit: int = 20) -> Any:
    search_events = (
        db.session.query(SearchEvent)
        .order_by(SearchEvent.timestamp.desc())
        .limit(99999)
        .all()
    )

    keywords: Dict[str, int] = {}
    for ev in search_events:
        for tok in _split_search_string(ev.value):
            if tok in keywords:
                keywords[tok] += 1
                continue
            keywords[tok] = 1
    results: List[Tuple[str, Any]] = []
    for keyword, value in keywords.items():
        results.append((keyword, value))
    results.sort(key=lambda k: k[1], reverse=True)  # type: ignore

    # generate the graph data
    labels: List[str] = []
    data: List[Any] = []
    for res in results[0:limit]:
        labels.append(str(res[0]))
        data.append(res[1])
    return render_template(
        "analytics-search-stats.html", category="analytics", labels=labels, data=data
    )
