#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=too-few-public-methods,protected-access,too-many-instance-attributes
# pylint: disable=singleton-comparison

import os
import datetime
import hashlib
import enum
import json
from typing import Optional, List

from flask import g, url_for
from flask import current_app as app

from sqlalchemy import (
    Column,
    Boolean,
    DateTime,
    ForeignKey,
    Integer,
    String,
    Text,
)
from sqlalchemy.orm import relationship

from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.exc import OperationalError

from cabarchive import CabArchive, NotSupportedError

from lvfs import db

from lvfs.vendors.models import VendorAffiliation
from lvfs.users.models import User
from lvfs.tests.models import Test, TestResult
from lvfs.claims.models import Claim
from lvfs.components.models import Component
from lvfs.reports.models import Report
from lvfs.tasks.models import Task


class FirmwareEventKind(enum.Enum):
    UNKNOWN = 0
    UPLOADED = 1
    MOVED = 2
    DELETED = 3
    UNDELETED = 4
    DEMOTED = 5
    AFFILIATION_CHANGE = 6


class FirmwareEvent(db.Model):  # type: ignore

    __tablename__ = "firmware_events"

    firmware_event_id = Column(Integer, primary_key=True)
    firmware_id = Column(
        Integer, ForeignKey("firmware.firmware_id"), nullable=False, index=True
    )
    kind = Column(Integer, default=0)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)
    timestamp = Column(
        DateTime, nullable=False, default=datetime.datetime.utcnow, index=True
    )
    remote_id = Column(
        Integer, ForeignKey("remotes.remote_id"), nullable=False, index=True
    )
    remote_old_id = Column(Integer, ForeignKey("remotes.remote_id"), default=None)

    fw = relationship("Firmware", back_populates="events")

    user = relationship("User", foreign_keys=[user_id])
    remote = relationship("Remote", foreign_keys=[remote_id], lazy="joined")
    remote_old = relationship("Remote", foreign_keys=[remote_old_id])

    @property
    def kind_color(self):
        if self.kind in (
            FirmwareEventKind.DELETED.value,
            FirmwareEventKind.UNDELETED.value,
        ):
            return "warning"
        if self.kind == FirmwareEventKind.DEMOTED.value:
            return "danger"
        return "info"

    @property
    def kind_display(self) -> str:
        # lets try and work this out
        if not self.kind:
            return "Moved"
        if self.kind == FirmwareEventKind.UPLOADED.value:
            return "Uploaded"
        if self.kind == FirmwareEventKind.MOVED.value:
            return "Moved"
        if self.kind == FirmwareEventKind.DEMOTED.value:
            return "Demoted"
        if self.kind == FirmwareEventKind.DELETED.value:
            return "Deleted"
        if self.kind == FirmwareEventKind.UNDELETED.value:
            return "Undeleted"
        if self.kind == FirmwareEventKind.AFFILIATION_CHANGE.value:
            return "Vendor changed"
        return FirmwareEventKind(self.kind).name

    def __repr__(self) -> str:
        return "FirmwareEvent object %s" % self.firmware_event_id


class FirmwareRevision(db.Model):  # type: ignore

    __tablename__ = "firmware_revisions"

    firmware_revision_id = Column(Integer, primary_key=True)
    firmware_id = Column(
        Integer, ForeignKey("firmware.firmware_id"), default=None, index=True
    )
    filename = Column(Text, nullable=False, index=True)
    timestamp = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    immutable = Column(Boolean, default=False)  # do not auto-delete

    fw = relationship("Firmware", back_populates="revisions")

    @property
    def absolute_path(self) -> str:
        return os.path.join(app.config["DOWNLOAD_DIR"], self.filename)

    @property
    def blob(self) -> bytes:
        with open(self.absolute_path, "rb") as f:
            return f.read()

    @property
    def url(self) -> str:
        return os.path.join("/downloads", self.filename)

    def write(self, blob: bytes, verify: bool = False) -> None:
        with open(self.absolute_path, "wb") as f:
            f.write(blob)
        if verify:
            with open(self.absolute_path, "rb") as f:
                blob2: bytes = f.read()
            if blob != blob2:
                raise PermissionError(
                    "file was not written to {} correctly: got {}, expected {}".format(
                        self.absolute_path,
                        hashlib.sha256(blob2).hexdigest(),
                        hashlib.sha256(blob).hexdigest(),
                    )
                )

    def __repr__(self) -> str:
        return "FirmwareRevision({}, {})".format(self.filename, self.timestamp)


class FirmwareVendor(db.Model):  # type: ignore

    __tablename__ = "firmware_vendors"
    firmware_vendor_id = Column(Integer, primary_key=True)
    firmware_id = Column(
        Integer, ForeignKey("firmware.firmware_id"), nullable=False, index=True
    )
    vendor_id = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)

    fw = relationship("Firmware", back_populates="odm_vendors_map")
    vendor = relationship("Vendor", foreign_keys=[vendor_id])
    user = relationship("User", foreign_keys=[user_id])

    def check_acl(self, action: str, user: Optional[User] = None) -> bool:

        # fall back
        if not user:
            user = g.user
        if not user:
            return False
        if user.check_acl("@admin"):
            return True

        # depends on the action requested
        if action == "@delete":
            if self.fw.vendor != user.vendor:
                return False
            if not user.check_acl("@vendor-manager"):
                return False
            return True
        if action == "@view":
            if self.fw.vendor == user.vendor:
                return True
            if self.vendor == user.vendor:
                return True
            return False
        raise NotImplementedError(
            "unknown security check action: %s:%s" % (self, action)
        )

    def __repr__(self) -> str:
        return "FirmwareVendor object {}".format(self.firmware_vendor_id)


class FirmwareAction(db.Model):  # type: ignore

    __tablename__ = "firmware_actions"

    firmware_action_id = Column(Integer, primary_key=True)
    firmware_id = Column(
        Integer, ForeignKey("firmware.firmware_id"), nullable=False, index=True
    )
    remote_id = Column(
        Integer, ForeignKey("remotes.remote_id"), nullable=False, index=True
    )
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    mtime = Column(DateTime, nullable=False)  # when to move the firmware
    dtime = Column(DateTime, default=None)  # when the firmware moved

    fw = relationship("Firmware", back_populates="actions")
    user = relationship("User", foreign_keys=[user_id])
    remote = relationship("Remote", foreign_keys=[remote_id])

    def __repr__(self) -> str:
        return "FirmwareAction object {}".format(self.firmware_action_id)


class FirmwareAsset(db.Model):  # type: ignore

    __tablename__ = "firmware_assets"

    firmware_asset_id = Column(Integer, primary_key=True)
    firmware_id = Column(
        Integer, ForeignKey("firmware.firmware_id"), nullable=False, index=True
    )
    filename = Column(Text, nullable=False)
    ctime = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False)

    fw = relationship("Firmware", back_populates="assets")
    user = relationship("User", foreign_keys=[user_id])

    @property
    def absolute_path(self) -> str:
        return os.path.join(app.config["DOWNLOAD_DIR"], self.filename)

    @property
    def url(self) -> str:
        return os.path.join("/downloads", self.filename)

    def __repr__(self) -> str:
        return "<FirmwareAsset {}>".format(self.firmware_asset_id)


class Firmware(db.Model):  # type: ignore

    __tablename__ = "firmware"

    firmware_id = Column(Integer, primary_key=True)
    vendor_id = Column(
        Integer, ForeignKey("vendors.vendor_id"), nullable=False, index=True
    )
    addr = Column(String(40), nullable=False)
    timestamp = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    _filename = Column("filename", Text, nullable=True, default=None)
    download_cnt = Column(Integer, default=0)
    checksum_upload_sha1 = Column(String(40), nullable=False, index=True, unique=True)
    checksum_upload_sha256 = Column(String(64), nullable=False)
    _version_display = Column("version_display", Text, nullable=True, default=None)
    remote_id = Column(
        Integer, ForeignKey("remotes.remote_id"), nullable=False, index=True
    )
    checksum_signed_sha1 = Column(String(40), nullable=False)
    checksum_signed_sha256 = Column(String(64), nullable=False)
    user_id = Column(Integer, ForeignKey("users.user_id"), nullable=False, index=True)
    signed_timestamp = Column(DateTime, default=None, index=True)  # to be signed
    signed_epoch = Column(Integer, default=None)
    _banned_country_codes = Column(
        "banned_country_codes", Text, default=None
    )  # ISO 3166, delimiter ','
    report_success_cnt = Column(Integer, default=0)  # updated by celery task
    report_failure_cnt = Column(Integer, default=0)  # updated by celery task
    report_issue_cnt = Column(Integer, default=0)  # updated by celery task
    failure_minimum = Column(Integer, default=0)
    failure_percentage = Column(Integer, default=0)
    _do_not_track = Column("do_not_track", Boolean, default=False)
    restricted_ts = Column(DateTime, default=None)

    mds = relationship(
        "Component",
        back_populates="fw",
        lazy="joined",
        cascade="all,delete,delete-orphan",
    )
    revisions = relationship(
        "FirmwareRevision",
        order_by="desc(FirmwareRevision.timestamp)",
        back_populates="fw",
        lazy="joined",
        cascade="all,delete,delete-orphan",
    )
    events = relationship(
        "FirmwareEvent",
        order_by="desc(FirmwareEvent.timestamp)",
        back_populates="fw",
        cascade="all,delete,delete-orphan",
    )
    reports = relationship(
        "Report", back_populates="fw", cascade="all,delete,delete-orphan"
    )
    clients = relationship(
        "Client", back_populates="fw", cascade="all,delete,delete-orphan"
    )
    actions = relationship(
        "FirmwareAction", back_populates="fw", cascade="all,delete,delete-orphan"
    )
    tests = relationship(
        "Test",
        order_by="desc(Test.scheduled_ts)",
        back_populates="fw",
        cascade="all,delete,delete-orphan",
    )
    analytics = relationship(
        "AnalyticFirmware",
        back_populates="firmware",
        cascade="all,delete,delete-orphan",
    )
    assets = relationship(
        "FirmwareAsset", back_populates="fw", cascade="all,delete,delete-orphan"
    )

    vendor = relationship("Vendor", foreign_keys=[vendor_id])
    odm_vendors_map = relationship(
        "FirmwareVendor", back_populates="fw", cascade="all,delete,delete-orphan"
    )
    odm_vendors = association_proxy("odm_vendors_map", "vendor")
    user = relationship("User", foreign_keys=[user_id], back_populates="fws")
    remote = relationship(
        "Remote", foreign_keys=[remote_id], lazy="joined", back_populates="fws"
    )

    @property
    def is_dirty(self) -> bool:
        return (
            db.session.query(Task)
            .filter(Task.function == "lvfs.firmware.utils.task_sign_fw")
            .filter(Task.ended_ts == None)
            .filter(Task.value == json.dumps({"id": self.firmware_id}))
            .first()
        ) != None

    @property
    def reports_verified(self) -> List[Report]:
        try:
            return (
                db.session.query(Report)
                .filter(Report.firmware_id == self.firmware_id)
                .filter(Report.state == 2)
                .filter(Report.user_id != None)
                .order_by(Report.timestamp.desc())
                .all()
            )
        except OperationalError:
            return []

    @property
    def original_filename(self) -> str:

        # only add sections that are not checksums
        valid: List[str] = []
        for chunk in self.revisions[0].filename.split("-"):
            if chunk.isalnum() and len(chunk) in [40, 64]:
                continue
            valid.append(chunk)
        return "-".join(valid)

    @property
    def target_duration(self) -> int:
        if not self.events:
            return 0
        return datetime.datetime.utcnow() - self.events[0].timestamp.replace(
            tzinfo=None
        )

    @property
    def release_ts(self) -> Optional[DateTime]:
        for ev in self.events:
            if ev.remote.is_public:
                return ev.timestamp.replace(tzinfo=None)
        return None

    @property
    def do_not_track(self) -> bool:
        return self._do_not_track or self.vendor.do_not_track

    @do_not_track.setter
    def do_not_track(self, value: bool) -> None:
        self._do_not_track = value

    @property
    def is_deleted(self) -> bool:
        if not self.remote:
            return False
        return self.remote.is_deleted

    @property
    def banned_country_codes(self) -> List[str]:
        if self._banned_country_codes:
            return self._banned_country_codes.split(",")
        if self.vendor.banned_country_codes:
            return self.vendor.banned_country_codes.split(",")
        return []

    @banned_country_codes.setter
    def banned_country_codes(self, values: List[str]) -> None:
        self._banned_country_codes = ",".join(values)

    @property
    def get_possible_users_to_email(self) -> List[User]:
        users: List[User] = []

        # vendor that owns the firmware
        for u in self.vendor.users:
            if u.check_acl("@qa") or u.check_acl("@vendor-manager"):
                users.append(u)

        # odm that uploaded the firmware
        for vendor in self.odm_vendors:
            for u in vendor.users:
                if u.check_acl("@qa") or u.check_acl("@vendor-manager"):
                    users.append(u)
        return users

    @property
    def success(self) -> Optional[int]:
        total = self.report_failure_cnt + self.report_success_cnt
        if not total:
            return None
        return (self.report_success_cnt * 100) / total

    @property
    def success_confidence(self) -> str:
        total = (
            self.report_failure_cnt + self.report_success_cnt + self.report_issue_cnt
        )
        if total > 50:
            return "high"
        if total > 25:
            return "medium"
        return "low"

    @property
    def color(self) -> str:
        if self.success is None:
            return "secondary"
        if self.success > 95:
            return "success"
        if self.success > 80:
            return "warning"
        return "danger"

    @property
    def names(self) -> List[str]:
        names: List[str] = []
        for md in self.mds:
            if md.names:
                names.extend(md.names)
        return names

    @property
    def is_failure(self) -> bool:
        if not self.report_failure_cnt:
            return False
        if not self.failure_minimum:
            return False
        if not self.failure_percentage:
            return False
        if self.report_failure_cnt < self.failure_minimum:
            return False
        return self.success < self.failure_percentage

    @property
    def inhibit_download(self) -> bool:
        for md in self.mds:
            if md.inhibit_download:
                return True
        return False

    def find_test_by_plugin_id(self, plugin_id: Optional[str]) -> Optional[Test]:
        if not plugin_id:
            return None
        for test in self.tests:
            if test.plugin_id == plugin_id:
                return test
        return None

    @property
    def autoclaims(self) -> List[Claim]:
        # return the smallest of all the components, i.e. the least secure
        md_lowest = None
        claims: List[Claim] = []
        for md in self.mds:
            if not md_lowest or md.security_level < md_lowest.security_level:
                md_lowest = md
                claims = md.autoclaims

        # been virus checked
        test = self.find_test_by_plugin_id("clamav")
        if test and test.ended_ts and test.result == TestResult.PASS:
            claims.append(
                Claim(
                    kind="virus-safe",
                    icon="success",
                    summary="Virus checked using ClamAV",
                    url="https://lvfs.readthedocs.io/en/latest/claims.html#virus-safe",
                )
            )

        # FwHunt-clean
        test = self.find_test_by_plugin_id("uefi_scanner")
        if test and test.ended_ts:
            if test.result == TestResult.PASS:
                claims.append(
                    Claim(
                        kind="fwhunt",
                        icon="success",
                        summary="No issues found using FwHunt from Binarly",
                        url="https://lvfs.readthedocs.io/en/latest/claims.html#fwhunt",
                    )
                )
            elif test.result == TestResult.FAIL:
                claims.append(
                    Claim(
                        kind="fwhunt",
                        icon="warning",
                        summary="Issues found using FwHunt from Binarly",
                        url="https://lvfs.readthedocs.io/en/latest/claims.html#fwhunt",
                    )
                )
        return claims

    @property
    def claims(self) -> List[Claim]:
        claims: List[Claim] = []
        for md in self.mds:
            for claim in md.claims:
                if claim not in claims:
                    claims.append(claim)
        return claims

    @property
    def scheduled_signing(self) -> DateTime:
        now = datetime.datetime.now()
        secs = ((5 - (now.minute % 5)) * 60) + (60 - now.second)
        return datetime.datetime.now() + datetime.timedelta(seconds=secs)

    @property
    def version_display(self) -> str:
        if self._version_display and self.vendor.default_inf_parsing:
            return self._version_display
        md_versions: List[str] = []
        for md in self.mds:
            if not md.version_display:
                continue
            if md.version_display not in md_versions:
                md_versions.append(md.version_display)
        return ", ".join(md_versions)

    @version_display.setter
    def version_display(self, value):
        self._version_display = value

    @property
    def md_prio(self) -> Optional[Component]:
        md_prio = None
        for md in self.mds:
            if not md_prio or md.priority > md_prio.priority:
                md_prio = md
        return md_prio

    @property
    def md_generic(self) -> Optional[Component]:
        for md in self.mds:
            if md.appstream_type == "generic":
                return md
        return None

    @property
    def is_restricted(self) -> bool:
        if not self.restricted_ts:
            return False
        return self.restricted_ts > datetime.datetime.utcnow()

    @property
    def problems(self) -> List[Claim]:
        # does the firmware have any warnings
        problems: List[Claim] = []
        if self.is_deleted:
            problems.append(
                Claim(
                    kind="deleted",
                    icon="trash",
                    summary="Firmware has been deleted",
                    description="Once a file has been deleted on the LVFS it must be "
                    "undeleted before it can be moved to a different target.",
                    url=url_for("firmware.route_show", firmware_id=self.firmware_id),
                )
            )
        if not self.signed_timestamp:
            problems.append(
                Claim(
                    kind="unsigned",
                    icon="waiting",
                    summary="Firmware is unsigned",
                    description="Signing a firmware file on the LVFS is automatic and will "
                    "be completed soon.\n"
                    "You can refresh this page to find out when the firmware "
                    "has been signed.",
                    url=url_for("firmware.route_show", firmware_id=self.firmware_id),
                )
            )
        if self.is_restricted:
            problems.append(
                Claim(
                    kind="restricted",
                    icon="waiting",
                    summary="Firmware cannot be public until {}".format(
                        self.restricted_ts.strftime("%F %T")
                    ),
                    description="This firmware contains security advisories that  "
                    "are under CVE embargo until a specific date.\n"
                    "This problem cannot be waived.",
                    url=url_for("firmware.route_show", firmware_id=self.firmware_id),
                )
            )

        # test failures
        for test in self.tests:
            if not test.started_ts:
                problems.append(
                    Claim(
                        kind="test-pending",
                        icon="waiting",
                        summary="Runtime test {} is pending".format(test.plugin_id),
                        description="The LVFS runs tests on certain types of firmware to check they "
                        "are valid.\n"
                        "Some tests are still pending and will be completed shortly.\n"
                        "You can refresh this page to find out when the firmware has "
                        "been tested.",
                        url=url_for(
                            "firmware.route_tests", firmware_id=self.firmware_id
                        ),
                    )
                )
            elif test.result == TestResult.FAIL and not test.waived_ts:
                if test.waivable:
                    problems.append(
                        Claim(
                            kind="test-failed",
                            icon="warning",
                            summary="Runtime test {} did not succeed".format(
                                test.plugin_id
                            ),
                            description="A check on the firmware has failed.\n"
                            "This failure can be waived by a QA user and ignored.",
                            url=url_for(
                                "firmware.route_tests", firmware_id=self.firmware_id
                            ),
                        )
                    )
                else:
                    problems.append(
                        Claim(
                            kind="test-failed",
                            icon="warning",
                            summary="Runtime test {} did not succeed".format(
                                test.plugin_id
                            ),
                            description="A non-waivable check on the firmware has failed.",
                            url=url_for(
                                "firmware.route_tests", firmware_id=self.firmware_id
                            ),
                        )
                    )
        for md in self.mds:
            for problem in md.problems:
                problem.md = md
                problems.append(problem)
        return problems

    def _is_owner(self, user: User) -> bool:
        return self.user_id == user.user_id

    @property
    def blob(self) -> Optional[bytes]:
        if not hasattr(self, "_blob"):
            try:
                with open(self.revisions[0].absolute_path, "rb") as f:
                    self._blob = f.read()
            except FileNotFoundError:
                return None
        return self._blob

    @blob.setter
    def blob(self, value: bytes) -> None:
        self._blob = value

    def _ensure_blobs(self) -> None:
        try:
            cabarchive = CabArchive(self.blob)
        except NotSupportedError:
            return
        for md in self.mds:
            try:
                md._blob = cabarchive[md.filename_contents].buf
            except KeyError:
                pass

    def _is_vendor(self, user: User) -> bool:
        return self.vendor_id == user.vendor_id

    def _is_odm(self, user: User) -> bool:
        return user.vendor in self.odm_vendors

    def _is_permitted_action(self, action: str, user: User) -> bool:

        # is vendor
        if self._is_vendor(user):
            return True

        # the user is not a member of the ODM vendor
        if user.vendor not in self.odm_vendors:
            return False

        # check ODM permissions
        aff = (
            db.session.query(VendorAffiliation)
            .filter(VendorAffiliation.vendor_id == self.vendor_id)
            .filter(VendorAffiliation.vendor_id_odm == user.vendor_id)
            .first()
        )
        if not aff:
            return False
        return aff.get_action(action)

    def check_acl(self, action: str, user: Optional[User] = None) -> bool:

        # fall back
        if not user:
            user = g.user
        if not user:
            return False
        if user.check_acl("@admin"):
            return True

        # depends on the action requested
        if action == "@delete":
            if self.is_deleted:
                return False
            if user.check_acl("@qa") and self._is_permitted_action(action, user):
                return True
            return False
        if action == "@nuke":
            if (
                self.is_deleted
                and self._is_vendor(user)
                and user.check_acl("@vendor-manager")
            ):
                return True
            return False
        if action == "@view":
            if user.check_acl("@qa") and self._is_permitted_action(action, user):
                return True
            if user.check_acl("@analyst") and self._is_permitted_action(action, user):
                return True
            if self._is_owner(user):
                return True
            return False
        if action == "@view-analytics":
            if not self.check_acl("@view", user):
                return False
            if user.check_acl("@qa") or user.check_acl("@analyst"):
                return True
            return False
        if action == "@derestrict":
            if user.check_acl("@vendor-manager") and self._is_vendor(user):
                return True
            return False
        if action == "@undelete":
            if user.check_acl("@qa") and self._is_permitted_action(action, user):
                return True
            if self._is_owner(user):
                return True
            return False
        if action == "@add-odm-vendor":
            if self._is_owner(user):
                return True
            if not user.check_acl("@qa"):
                return False
            if self.vendor != user.vendor:
                return False
            return True
        if action == "@resign":
            if user.check_acl("@qa") and self._is_permitted_action(action, user):
                return True
            if self._is_owner(user):
                return True
            return False
        if action in ("@promote-stable", "@promote-testing"):
            if user.check_acl("@approved-public") and self._is_permitted_action(
                action, user
            ):
                return True
            return False
        if action.startswith("@promote-"):
            if user.check_acl("@qa") and self._is_vendor(user):
                return True
            # ODM vendor can always move private<->embargo
            if self._is_odm(user) or self._is_owner(user):
                old = self.remote.name
                if old.startswith("embargo-"):
                    old = "embargo"
                new = action[9:]
                if new.startswith("embargo-"):
                    new = "embargo"
                if old in ("private", "embargo") and new in ("private", "embargo"):
                    return True
            return False
        if action == "@modify":
            if user.check_acl("@qa") and self._is_permitted_action(action, user):
                return True
            if self._is_owner(user):
                return True
            return False
        if action == "@modify-limit":
            if user.check_acl("@qa") and self._is_permitted_action(action, user):
                return True
            return False
        if action == "@modify-affiliation":
            if user.check_acl("@qa") and self._is_permitted_action(action, user):
                return True
            return False
        raise NotImplementedError(
            "unknown security check action: %s:%s" % (self, action)
        )

    def __repr__(self) -> str:
        return "Firmware object %s" % self.checksum_upload_sha1
