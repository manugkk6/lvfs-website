#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from typing import Any

from flask import Blueprint, request, url_for, redirect, flash, render_template
from flask_login import login_required
from sqlalchemy.exc import NoResultFound, IntegrityError

from lvfs import db

from lvfs.util import admin_login_required
from lvfs.verfmts.models import Verfmt

bp_verfmts = Blueprint("verfmts", __name__, template_folder="templates")


@bp_verfmts.route("/")
@login_required
@admin_login_required
def route_list() -> Any:
    verfmts = db.session.query(Verfmt).order_by(Verfmt.verfmt_id.asc()).all()
    return render_template("verfmt-list.html", category="admin", verfmts=verfmts)


@bp_verfmts.post("/create")
@login_required
@admin_login_required
def route_create() -> Any:
    # ensure has enough data
    try:
        value = request.form["value"]
    except KeyError:
        flash("No form data found!", "warning")
        return redirect(url_for("verfmts.route_list"))
    if not value or value.find(" ") != -1:
        flash("Failed to add version format: Value needs to be valid", "warning")
        return redirect(url_for("verfmts.route_list"))

    # add verfmt
    try:
        verfmt = Verfmt(value=request.form["value"])
        db.session.add(verfmt)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Failed to add version format: Already exists", "info")
        return redirect(url_for("verfmts.route_list"))
    flash("Added version format", "info")
    return redirect(url_for("verfmts.route_show", verfmt_id=verfmt.verfmt_id))


@bp_verfmts.post("/<int:verfmt_id>/delete")
@login_required
@admin_login_required
def route_delete(verfmt_id: int) -> Any:

    # get verfmt
    try:
        db.session.query(Verfmt).filter(Verfmt.verfmt_id == verfmt_id).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Cannot delete", "danger")
        return redirect(url_for("verfmts.route_list"))
    flash("Deleted version format", "info")
    return redirect(url_for("verfmts.route_list"))


@bp_verfmts.post("/<int:verfmt_id>/modify")
@login_required
@admin_login_required
def route_modify(verfmt_id: int) -> Any:

    # find verfmt
    try:
        verfmt = (
            db.session.query(Verfmt)
            .filter(Verfmt.verfmt_id == verfmt_id)
            .with_for_update(of=Verfmt)
            .one()
        )
    except NoResultFound:
        flash("No version format found", "info")
        return redirect(url_for("verfmts.route_list"))

    # modify verfmt
    for key in ["name", "example", "value", "fwupd_version", "fallbacks"]:
        if key in request.form:
            setattr(verfmt, key, request.form[key])
    db.session.commit()

    # success
    flash("Modified version format", "info")
    return redirect(url_for("verfmts.route_show", verfmt_id=verfmt_id))


@bp_verfmts.route("/<int:verfmt_id>")
@login_required
@admin_login_required
def route_show(verfmt_id: int) -> Any:

    # find verfmt
    try:
        verfmt = db.session.query(Verfmt).filter(Verfmt.verfmt_id == verfmt_id).one()
    except NoResultFound:
        flash("No version format found", "info")
        return redirect(url_for("verfmts.route_list"))

    # show details
    return render_template("verfmt-details.html", category="admin", verfmt=verfmt)
