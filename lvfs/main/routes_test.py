#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position,singleton-comparison

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_login_logout(self, _app, _client):

        # test logging in and out
        rv = self._login("sign-test@fwupd.org", "Pa$$w0rd")
        assert b"/lvfs/upload/firmware" in rv.data, rv.data.decode()
        rv = self._logout()
        rv = self._login("sign-test@fwupd.org", "Pa$$w0rd")
        assert b"/lvfs/upload/firmware" in rv.data, rv.data.decode()
        rv = self._logout()
        assert b"/lvfs/upload/firmware" not in rv.data, rv.data.decode()
        rv = self._login("sign-test@fwupd.orgx", "default")
        assert b"Incorrect username" in rv.data, rv.data.decode()
        rv = self._login("sign-test@fwupd.org", "defaultx")
        assert b"Incorrect password" in rv.data, rv.data.decode()
        rv = self._login("SIGN-TEST@FWUPD.ORG", "Pa$$w0rd")
        assert b"/lvfs/upload/firmware" in rv.data, rv.data.decode()

    def test_pulp_manifest(self, _app, client):

        # login, upload, make stable, logout
        self.login()
        self.add_namespace()
        self.upload()
        self.run_task_worker()
        rv = client.post("/lvfs/firmware/1/promote/stable", follow_redirects=True)
        assert b"Moving firmware" in rv.data, rv.data.decode()
        self.run_task_worker()
        rv = client.get("/lvfs/firmware/1/target", follow_redirects=True)
        assert b">stable<" in rv.data, rv.data.decode()
        self.run_task_worker()
        self.logout()

        # download PULP_MANIFEST
        rv = client.get("/downloads/PULP_MANIFEST")
        assert b"firmware-00001-stable.xml.gz" in rv.data, rv.data.decode()
        assert b"firmware-00001-stable.xml.xz" in rv.data, rv.data.decode()
        assert b"firmware.xml.gz" in rv.data, rv.data.decode()
        assert b"firmware.xml.gz.jcat" in rv.data, rv.data.decode()
        assert b"hughski-colorhug2-2.0.3.cab" in rv.data, rv.data.decode()

    def test_pulp_manifest_vendor(self, _app, client):

        # login, make token, upload, sign, logout
        self.login()
        basic_token = self.token_create()
        self.add_namespace()
        self.upload(target="embargo")
        self.run_task_worker()
        self.logout()

        # no auth
        rv = client.get("/downloads/PULP_MANIFEST/vendor/1")
        assert b"Unauthorized Access" in rv.data, rv.data.decode()
        assert rv.status_code == 401, rv.status_code

        # bad auth
        rv = client.get(
            "/downloads/PULP_MANIFEST/vendor/1", headers={"Authorization": "Basic FOO"}
        )
        assert b"Unauthorized Access" in rv.data, rv.data.decode()
        assert rv.status_code == 401, rv.status_code

        # invalid vendor
        rv = client.get(
            "/downloads/PULP_MANIFEST/vendor/999",
            headers={"Authorization": "Basic {}".format(basic_token)},
        )
        assert b"Invalid Vendor" in rv.data, rv.data.decode()
        assert rv.status_code == 404, rv.status_code

        # all okay
        rv = client.get(
            "/downloads/PULP_MANIFEST/vendor/1",
            headers={"Authorization": "Basic {}".format(basic_token)},
        )
        assert b"hughski-colorhug2-2.0.3.cab" in rv.data, rv.data.decode()
        assert b"firmware-admin.xml.gz.jcat" in rv.data, rv.data.decode()
        assert b"firmware-00001-admin.xml.xz" in rv.data, rv.data.decode()
        assert b"firmware-admin.xml.xz.jcat" in rv.data, rv.data.decode()
        assert rv.status_code == 200, rv.status_code

    def test_eventlog(self, _app, client):

        # login, upload then check both events were logged
        self.login()
        self.add_user("alice@fwupd.org")
        self.add_user("bob@fwupd.org", is_qa=True)
        self.add_user("mario@oem.com", is_qa=True, group_id="oem")
        self.logout()

        # alice cannot see her own event
        self.login("alice@fwupd.org")
        self.upload()
        rv = client.get("/lvfs/eventlog", follow_redirects=True)
        assert b"Unable to show event log for non-QA user" in rv.data, rv.data.decode()
        assert b"Uploaded file" not in rv.data, rv.data.decode()
        assert b"Logged in" not in rv.data, rv.data.decode()
        self.logout()

        # sign firmware, to create a admin-only event
        self.run_task_worker()

        # mario can't see anything as he's in a different vendor group
        self.login("mario@oem.com")
        rv = client.get("/lvfs/eventlog")
        assert b"Uploaded file" not in rv.data, rv.data.decode()
        assert b"Logged in" in rv.data, rv.data.decode()
        assert b"Signed firmware" not in rv.data, rv.data.decode()
        assert b"mario@oem.com" in rv.data, rv.data.decode()
        assert b"alice@fwupd.org" not in rv.data, rv.data.decode()
        self.logout()

        # bob is QA and can see just event for his vendor group
        self.login("bob@fwupd.org")
        rv = client.get("/lvfs/eventlog")
        assert b"Uploaded file" in rv.data, rv.data.decode()
        assert b"Logged in" in rv.data, rv.data.decode()
        assert b">anonymous<" not in rv.data, rv.data.decode()
        assert b"Signed firmware" not in rv.data, rv.data.decode()
        assert b"mario@oem.com" not in rv.data, rv.data.decode()
        assert b"alice@fwupd.org" in rv.data, rv.data.decode()
        self.logout()

        # root can see everything
        self.login()
        rv = client.get("/lvfs/eventlog")
        assert b"Uploaded file" in rv.data, rv.data.decode()
        assert b"Logged in" in rv.data, rv.data.decode()
        assert b"alice@fwupd.org" in rv.data, rv.data.decode()
        assert b"bob@fwupd.org" in rv.data, rv.data.decode()
        assert b"mario@oem.com" in rv.data, rv.data.decode()

    def test_metrics(self, _app, client):

        self.login()
        self.upload()
        self.logout()
        self.run_task_worker()
        self._download_firmware()
        self.run_cron_stats()

        rv = client.get("/lvfs/metrics")
        assert b'ClientCnt": 1' in rv.data, rv.data.decode()

    def test_nologin_required(self, _app, client):

        # all these are viewable without being logged in
        uris = [
            "/",
            "/lvfs",
            "/vendors",
            "/users",
            "/developers",
            "/status",
            "/vendorlist",
            "/lvfs/devices",
            "/lvfs/devices/2082b5e0-7a64-478a-b1b2-e3404fab6dad",
            "/lvfs/docs/agreement",
            "/lvfs/docs/developers",
            "/lvfs/docs/users",
            "/lvfs/docs/vendors",
            "/users.html",
            "/vendors.html",
            "/developers.html",
            "/index.html",
        ]
        for uri in uris:
            rv = client.get(uri, follow_redirects=True)
            assert b"favicon.ico" in rv.data, rv.data.decode()
            assert b"LVFS: Error" not in rv.data, rv.data.decode()

    def test_fail_when_login_required(self, _app, client):

        # all these are an error when not logged in
        uris = ["/lvfs/firmware"]
        for uri in uris:
            rv = client.get(uri, follow_redirects=True)
            assert b"favicon.ico" in rv.data, rv.data.decode()
            assert b"Permission denied: Tried to request" in rv.data, rv.data.decode()

    def test_guid_generator(self, _app, client):

        rv = client.get("/lvfs/guid", follow_redirects=True)
        assert b"<form" in rv.data, rv.data.decode()

        rv = client.post(
            "/lvfs/guid",
            data={"instance_id": "USB\\VID_273F&PID_100A"},
            follow_redirects=True,
        )
        assert b"USB\\VID_273F&amp;PID_100A" in rv.data, rv.data.decode()
        assert b"f5b42624-ff57-5073-ba09-b7c9c04241be" in rv.data, rv.data.decode()
        rv = client.post(
            "/lvfs/guid",
            data={"instance_id": "[USB\\VID_273F&PID_100A]"},
            follow_redirects=True,
        )
        assert b"f5b42624-ff57-5073-ba09-b7c9c04241be" in rv.data, rv.data.decode()

    def test_horrible_hackers(self, _app, client):

        # all these are an error when not logged in
        uris = ["/wp-login.php"]
        for uri in uris:
            rv = client.get(uri)
            assert b"bad karma" in rv.data, rv.data.decode()

    def test_uswid_generator(self, _app, client):

        rv = client.get("/lvfs/uswid", follow_redirects=True)
        assert b"<form" in rv.data, rv.data.decode()

        rv = client.post(
            "/lvfs/uswid",
            data={
                "tag_id": "foo",
                "software_version": "1.2.3",
                "entity_name": "Hughski",
                "entity_regid": "hughski.com",
                "format": "ini",
            },
            follow_redirects=True,
        )
        assert b"uSWID" in rv.data, rv.data.decode()
        assert b"1.2.3" in rv.data, rv.data.decode()


if __name__ == "__main__":
    unittest.main()
