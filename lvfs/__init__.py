#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2015 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position,wrong-import-order,too-many-locals

import os
import sqlalchemy
import humanize
import iso3166
import iso639
import datetime
import html
import logging
from typing import List, Optional, Dict, Any

from logging.handlers import SMTPHandler

from flask import (
    Blueprint,
    Flask,
    flash,
    render_template,
    message_flashed,
    request,
    redirect,
    url_for,
    Response,
    g,
)
from flask_login import LoginManager
from flask_httpauth import HTTPBasicAuth
from flask_migrate import Migrate
from flask_mail import Mail
from flask_oauthlib.client import OAuth
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect, CSRFError
from flask_caching import Cache
from flask_debugtoolbar import DebugToolbarExtension
from werkzeug.local import LocalProxy
import posixpath

from lvfs.dbutils import drop_db, init_db
from lvfs.util import _xml_from_markdown

db: SQLAlchemy = SQLAlchemy()
oauth: OAuth = OAuth()
cache: Cache = Cache()
mail: Mail = Mail()
csrf: CSRFProtect = CSRFProtect()
migrate: Migrate = Migrate()
lm: LoginManager = LoginManager()
lm.login_view = "login1"
auth: HTTPBasicAuth = HTTPBasicAuth()
toolbar = DebugToolbarExtension()

from lvfs.pluginloader import Pluginloader

ploader = Pluginloader("plugins")


def cdn_url_builder(_error, endpoint, values):
    if endpoint != "cdn":
        return None
    from flask import current_app as app

    return posixpath.join(app.config["CDN_DOMAIN"], "static", values["filename"])


def flash_save_eventlog(unused_sender, message, category, **unused_extra):

    # optional, but used in the self tests
    from flask import current_app as app

    if app.config.get("FLASH_USE_EVENTLOG", False):
        from lvfs.util import _event_log

        _event_log(str(message), category in ["danger", "warning"])
        return

    # output to the console, to be caught by CloudWatch
    tokens: List[str] = []
    if category:
        tokens.append("category:{}".format(category))
    if request:
        tokens.append("request:{}".format(request.path))
    if hasattr(g, "user") and g.user:
        tokens.append("user:{}".format(g.user.username))
        tokens.append("vendor:{}".format(g.user.vendor.group_id))
    if message:
        tokens.append(str(message))
    print("FLASH: " + ", ".join(tokens))


@lm.user_loader
def load_user(user_id):
    from lvfs.users.models import User

    g.user = db.session.query(User).filter(User.username == user_id).one()
    return g.user


@auth.verify_password
def verify_password(username: str, password: str) -> Optional[str]:

    # find the user+token combination
    from lvfs.users.models import User, UserToken

    stmt = (
        db.session.query(UserToken.user_id)
        .filter(UserToken.value == password)
        .subquery()
    )
    g.user = (
        db.session.query(User)
        .join(stmt, User.user_id == stmt.c.user_id)
        .filter(User.username == username)
        .first()
    )
    if g.user:
        if not g.user.check_acl("@robot"):
            from lvfs.util import _event_log

            _event_log("{} authenticated using token".format(g.user.username))
        return g.user.username
    return None


def create_app(config: Optional[Dict[str, Any]] = None) -> Flask:

    app: Flask = Flask(__name__)
    app.url_build_error_handlers.append(cdn_url_builder)

    if config:
        for key, value in config.items():
            app.config[key] = value

    if not app.config.get("SECRET_KEY"):
        app.config.from_pyfile("flaskapp.cfg")
        try:
            app.config.from_pyfile("custom.cfg")
        except FileNotFoundError:
            pass

    db.init_app(app)
    oauth.init_app(app)
    cache.init_app(app)
    mail.init_app(app)
    csrf.init_app(app)
    migrate.init_app(app, db)
    lm.init_app(app)
    toolbar.init_app(app)

    from lvfs.agreements.routes import bp_agreements
    from lvfs.analytics.routes import bp_analytics
    from lvfs.categories.routes import bp_categories
    from lvfs.licenses.routes import bp_licenses
    from lvfs.claims.routes import bp_claims
    from lvfs.components.routes import bp_components
    from lvfs.devices.routes import bp_devices
    from lvfs.hsireports.routes import bp_hsireports
    from lvfs.mdsync.routes import bp_mdsync
    from lvfs.docs.routes import bp_docs
    from lvfs.firmware.routes import bp_firmware
    from lvfs.issues.routes import bp_issues
    from lvfs.main.routes import bp_main
    from lvfs.metadata.routes import bp_metadata
    from lvfs.fsck.routes import bp_fsck
    from lvfs.geoip.routes import bp_geoip
    from lvfs.protocols.routes import bp_protocols
    from lvfs.queries.routes import bp_queries
    from lvfs.reports.routes import bp_reports
    from lvfs.search.routes import bp_search
    from lvfs.settings.routes import bp_settings
    from lvfs.shards.routes import bp_shards
    from lvfs.telemetry.routes import bp_telemetry
    from lvfs.tests.routes import bp_tests
    from lvfs.upload.routes import bp_upload
    from lvfs.users.routes import bp_users
    from lvfs.vendors.routes import bp_vendors
    from lvfs.verfmts.routes import bp_verfmts
    from lvfs.tasks.routes import bp_tasks
    from lvfs.translations.routes import bp_translations

    app.register_blueprint(bp_agreements, url_prefix="/lvfs/agreements")
    app.register_blueprint(bp_analytics, url_prefix="/lvfs/analytics")
    app.register_blueprint(bp_categories, url_prefix="/lvfs/categories")
    app.register_blueprint(bp_licenses, url_prefix="/lvfs/licenses")
    app.register_blueprint(bp_claims, url_prefix="/lvfs/claims")
    app.register_blueprint(bp_components, url_prefix="/lvfs/components")
    app.register_blueprint(bp_devices, url_prefix="/lvfs/devices")
    app.register_blueprint(bp_mdsync, url_prefix="/lvfs/mdsync")
    app.register_blueprint(bp_docs, url_prefix="/lvfs/docs")
    app.register_blueprint(bp_firmware, url_prefix="/lvfs/firmware")
    app.register_blueprint(bp_issues, url_prefix="/lvfs/issues")
    app.register_blueprint(bp_main)
    app.register_blueprint(bp_metadata, url_prefix="/lvfs/metadata")
    app.register_blueprint(bp_fsck, url_prefix="/lvfs/fsck")
    app.register_blueprint(bp_geoip, url_prefix="/lvfs/geoip")
    app.register_blueprint(bp_protocols, url_prefix="/lvfs/protocols")
    app.register_blueprint(bp_queries, url_prefix="/lvfs/queries")
    app.register_blueprint(bp_reports, url_prefix="/lvfs/reports")
    app.register_blueprint(bp_search, url_prefix="/lvfs/search")
    app.register_blueprint(bp_settings, url_prefix="/lvfs/settings")
    app.register_blueprint(bp_shards, url_prefix="/lvfs/shards")
    app.register_blueprint(bp_telemetry, url_prefix="/lvfs/telemetry")
    app.register_blueprint(bp_tests, url_prefix="/lvfs/tests")
    app.register_blueprint(bp_upload, url_prefix="/lvfs/upload")
    app.register_blueprint(bp_users, url_prefix="/lvfs/users")
    app.register_blueprint(bp_vendors, url_prefix="/lvfs/vendors")
    app.register_blueprint(bp_verfmts, url_prefix="/lvfs/verfmts")
    app.register_blueprint(bp_tasks, url_prefix="/lvfs/tasks")
    app.register_blueprint(bp_translations, url_prefix="/lvfs/translations")
    app.register_blueprint(bp_hsireports, url_prefix="/lvfs/hsireports")

    @app.before_request
    def before_request_func():
        g.container_id = os.environ.get("CONTAINER_ID")
        g.host_name = app.config["HOST_NAME"]
        g.release = app.config["RELEASE"]

    @app.cli.command("initdb")  # type: ignore
    def initdb_command():
        init_db(db)

    @app.cli.command("dropdb")  # type: ignore
    def dropdb_command():
        drop_db(db)

    message_flashed.connect(flash_save_eventlog, app)

    @app.errorhandler(401)
    def errorhandler_401(msg: Optional[str] = None) -> Any:
        return render_template("error-401.html", msg=msg), 401

    @app.errorhandler(404)
    def error_page_not_found(unused_msg: Optional[str] = None) -> Any:
        # the world is a horrible place
        if request.path in ["/wp-login.php", "/a2billing/common/javascript/misc.js"]:
            return Response(response="bad karma", status=404, mimetype="text/plain")
        return render_template("error-404.html"), 404

    @app.errorhandler(CSRFError)
    def error_csrf(e):
        flash(str(e), "danger")
        return redirect(url_for("main.route_dashboard"))

    @app.after_request  # type: ignore
    def add_header(rsp: Response) -> Response:
        if not rsp.content_type or rsp.content_type.find("text/html") == -1:
            return rsp
        rsp.headers["Access-Control-Allow-Origin"] = "*"
        rsp.headers["Referrer-Policy"] = "same-origin"
        rsp.headers["X-Content-Type-Options"] = "nosniff"
        rsp.headers["X-Frame-Options"] = "DENY"
        rsp.headers["X-XSS-Protection"] = ";".join(["1", "mode=block"])
        if not app.debug:
            rsp.headers["Content-Security-Policy"] = ";".join(
                [
                    "default-src 'self' $CDN_DOMAIN$",
                    "script-src 'self' 'unsafe-inline' 'unsafe-eval' $CDN_DOMAIN$",
                    "img-src 'self' $CDN_DOMAIN$ data:",
                    "style-src 'self' 'unsafe-inline' $CDN_DOMAIN$",
                    "font-src 'self' $CDN_DOMAIN$",
                    "frame-ancestors 'none'",
                    "object-src 'none'",
                ],
            ).replace("$CDN_DOMAIN$", app.config["CDN_DOMAIN"])
            rsp.headers["Strict-Transport-Security"] = ";".join(
                ["max-age=63072000", "includeSubDomains", "preload"]
            )
        return rsp

    @app.context_processor
    def inject_globals():
        from lvfs.tests.models import TestResult

        return {"TestResult": TestResult}

    @app.context_processor
    def utility_processor():
        def format_humanize_naturaldelta(tmp):
            if not tmp:
                return "n/a"
            return humanize.naturaldelta(tmp)

        def format_humanize_naturaltime(tmp):
            if not tmp:
                return "n/a"
            return humanize.naturaltime(tmp.replace(tzinfo=None))

        def format_humanize_intchar(tmp):
            if tmp > 1000000:
                return "%.0fM" % (float(tmp) / 1000000)
            if tmp > 1000:
                return "%.0fK" % (float(tmp) / 1000)
            return tmp

        def format_timedelta_approx(tmp):
            return humanize.naturaltime(tmp).replace(" from now", "")

        def format_size(num, suffix="B"):
            if not isinstance(num, int) and not isinstance(num, int):
                return "???%s???" % num
            for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
                if abs(num) < 1024.0:
                    return "%3.1f%s%s" % (num, unit, suffix)
                num /= 1024.0
            return "%.1f%s%s" % (num, "Yi", suffix)

        def format_locale(locale):
            if not locale:
                return iso639.to_name("en")
            try:
                return iso639.to_name(locale)
            except iso639.NonExistentLanguageError:
                pass
            try:
                return iso639.to_name(locale.split("_")[0])
            except iso639.NonExistentLanguageError:
                pass
            return "Unknown"

        def format_iso3166(tmp):
            return iso3166.countries.get(tmp, ["Unknown!"])[0]

        def format_plugin_id(tmp):
            return ploader.get_by_id(tmp)

        def format_html_from_markdown(tmp: Optional[str]) -> str:
            if not tmp:
                return "<p>None</p>"
            root = _xml_from_markdown(tmp)
            if root is None:
                return "<p>None</p>"
            txt = ""
            for n in root:
                if n.tag == "p" and n.text:
                    txt += "<p>" + html.escape(n.text) + "</p>"
                elif n.tag in ["ul", "ol"]:
                    txt += "<ul>"
                    for c in n:
                        if c.tag == "li" and c.text:
                            txt += "<li>" + html.escape(c.text) + "</li>"
                    txt += "</ul>"
            return txt

        return dict(
            format_size=format_size,
            format_locale=format_locale,
            format_humanize_naturaltime=format_humanize_naturaltime,
            format_humanize_naturaldelta=format_humanize_naturaldelta,
            format_humanize_intchar=format_humanize_intchar,
            format_timedelta_approx=format_timedelta_approx,
            format_html_from_markdown=format_html_from_markdown,
            format_iso3166=format_iso3166,
            format_plugin_id=format_plugin_id,
            loader_plugins=sorted(ploader.get_all(), key=lambda x: x.name),
        )

    return app
