#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=wrong-import-position,singleton-comparison

import os
import sys
import unittest

sys.path.append(os.path.realpath("."))

from lvfs.testcase import LvfsTestCase


class LocalTestCase(LvfsTestCase):
    def test_views_search(self, _app, client):

        # upload firmware and move to stable
        self.login()
        self.add_namespace()
        self.upload(target="embargo")
        self.run_task_worker()
        rv = client.post("/lvfs/firmware/1/promote/stable", follow_redirects=True)
        assert b"Moving firmware" in rv.data, rv.data.decode()
        self.run_task_worker()
        rv = client.get("/lvfs/firmware/1/target", follow_redirects=True)
        assert b">stable<" in rv.data, rv.data.decode()

        # stats
        rv = client.get("/lvfs/analytics/search_history")
        assert b"No searches exist" in rv.data, rv.data.decode()

        # search logged in
        rv = client.get("/lvfs/search/firmware?value=colorhug2")
        assert b"ColorHug2 Device Update" in rv.data, rv.data.decode()
        rv = client.get(
            "/lvfs/search/firmware?value=bfedb98d190ed6ff560129f7dcde7e45a01b19007bd37cc09ac50131aa61cebb",
            follow_redirects=True,
        )
        assert b"ColorHug2" in rv.data, rv.data.decode()
        rv = client.get("/lvfs/search/firmware?value=foobarbaz")
        assert (
            b"No firmware has been uploaded or is visible by this user" in rv.data
        ), rv.data
        self.logout()

        # search anon
        rv = client.get("/lvfs/search?value=colorhug2")
        assert b"ColorHug2 Device Update" in rv.data, rv.data.decode()

        # analytics
        self.login()
        rv = client.get("/lvfs/analytics/search_history")
        assert b"No searches exist" not in rv.data, rv.data.decode()

    def test_anon_search(self, _app, client):

        # upload file with keywords
        self.login()
        self.add_namespace()
        self.upload(target="testing")
        self.logout()

        # search for something that does not exist
        rv = client.get("/lvfs/search?value=Edward")
        assert b"No results found for" in rv.data, rv.data.decode()

        # search for one defined keyword
        rv = client.get("/lvfs/search?value=Alice")
        assert b"ColorHug2" in rv.data, rv.data.decode()

        # search for one defined keyword, again
        rv = client.get("/lvfs/search?value=Alice")
        assert b"ColorHug2" in rv.data, rv.data.decode()

        # search for a keyword and a name match
        rv = client.get("/lvfs/search?value=Alice+Edward+ColorHug2")
        assert b"No results found for" in rv.data, rv.data.decode()

    def test_anon_search_not_promoted(self, _app, client):

        # upload file with keywords
        self.login()
        self.add_namespace()
        self.upload(target="embargo")
        self.logout()

        # search for something that does not exist
        rv = client.get("/lvfs/search?value=alice")
        assert b"No results found for" in rv.data, rv.data.decode()


if __name__ == "__main__":
    unittest.main()
