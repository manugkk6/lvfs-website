#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from typing import List, Any

from flask import Blueprint, request, render_template, flash, redirect, url_for
from flask_login import login_required

from sqlalchemy import func, or_
from sqlalchemy.exc import IntegrityError

from lvfs import db

from lvfs.components.models import (
    Component,
    ComponentGuid,
    ComponentTag,
    ComponentIssue,
    ComponentKeyword,
    ComponentRequirement,
)
from lvfs.firmware.models import Firmware, FirmwareRevision
from lvfs.hash import _addr_hash
from lvfs.metadata.models import Remote
from lvfs.util import admin_login_required, _split_search_string, _get_client_address
from lvfs.vendors.models import Vendor

from .models import SearchEvent

bp_search = Blueprint("search", __name__, template_folder="templates")


@bp_search.post("/<int:search_event_id>/delete")
@login_required
@admin_login_required
def route_delete(search_event_id: int) -> Any:
    try:
        db.session.query(SearchEvent).filter(
            SearchEvent.search_event_id == search_event_id
        ).delete()
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        flash("Cannot delete", "danger")
        return redirect(url_for("search.route_search"))
    flash("Deleted search event", "info")
    return redirect(url_for("analytics.route_search_history"))


def _add_search_event(ev: SearchEvent) -> None:
    if (
        db.session.query(SearchEvent)
        .filter(SearchEvent.value == ev.value)
        .filter(SearchEvent.addr == ev.addr)
        .first()
    ):
        return
    db.session.add(ev)
    db.session.commit()


@bp_search.route("/firmware", methods=["GET", "POST"])
@bp_search.route("/firmware/<int:max_results>", methods=["GET", "POST"])
@login_required
def route_fw(max_results: int = 100) -> Any:

    if "value" not in request.args:
        flash("No search value!", "danger")
        return redirect(url_for("search.route_search"))
    keywords_unsafe = _split_search_string(request.args["value"])
    if not keywords_unsafe:
        keywords_unsafe = request.args["value"].split(" ")

    # never allow empty keywords
    keywords: List[str] = []
    for keyword in keywords_unsafe:
        if keyword:
            keywords.append(keyword)
    if not keywords:
        flash("No valid search value!", "danger")
        return redirect(url_for("search.route_search"))

    # use keywords first
    fws = (
        db.session.query(Firmware)
        .join(Component)
        .join(ComponentKeyword)
        .filter(ComponentKeyword.value.in_(keywords))
        .order_by(Firmware.timestamp.desc())
        .limit(max_results)
        .all()
    )

    # try GUIDs
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(Component)
            .join(ComponentGuid)
            .filter(ComponentGuid.value.in_(keywords))
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # try tags
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(Component)
            .join(ComponentTag)
            .filter(ComponentTag.value.in_(keywords))
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # try version numbers
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(Component)
            .filter(Component.version.in_(keywords))
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # try appstream ID
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(Component)
            .filter(Component.appstream_id.startswith(keywords[0]))
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # try CVE, e.g. CVE-2018-3646
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(Component)
            .join(ComponentIssue)
            .filter(func.lower(ComponentIssue.value).in_(keywords))
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # try filename (with hash)
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(FirmwareRevision)
            .filter(FirmwareRevision.filename.in_(keywords))
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # try checksums
    if not fws:
        fws = (
            db.session.query(Firmware)
            .filter(
                or_(
                    Firmware.checksum_upload_sha1.in_(keywords),
                    Firmware.checksum_upload_sha256.in_(keywords),
                    Firmware.checksum_signed_sha1.in_(keywords),
                    Firmware.checksum_signed_sha256.in_(keywords),
                )
            )
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # try requirements
    if not fws:
        fws = (
            db.session.query(Firmware)
            .join(Component)
            .join(ComponentRequirement)
            .filter(ComponentRequirement.value.in_(keywords))
            .order_by(Firmware.timestamp.desc())
            .limit(max_results)
            .all()
        )

    # filter by ACL
    fws_safe: List[Firmware] = []
    for fw in fws:
        if fw.check_acl("@view"):
            fws_safe.append(fw)

    return render_template(
        "firmware-search.html",
        category="firmware",
        state="search",
        remote=None,
        fws=fws_safe,
    )


@bp_search.route("", methods=["GET", "POST"])
@bp_search.post("/<int:max_results>")
def route_search(max_results: int = 150) -> Any:

    # no search results
    if "value" not in request.args:
        return render_template(
            "search.html", mds=None, search_size=-1, keywords_good=[], keywords_bad=[]
        )

    # components that match
    keywords = _split_search_string(request.args["value"])
    ids = (
        db.session.query(ComponentKeyword.component_id)
        .filter(ComponentKeyword.value.in_(keywords))
        .group_by(ComponentKeyword.component_id)
        .having(func.count() == len(keywords))
        .subquery()
    )
    mds: List[Component] = []
    appstream_ids: List[str] = []
    vendors: List[Vendor] = []
    for md in (
        db.session.query(Component)
        .join(ids)
        .join(Firmware)
        .join(Remote)
        .filter(Remote.is_public)
        .order_by(Component.version.desc())
        .limit(max_results * 4)
    ):
        if md.appstream_id in appstream_ids:
            continue
        mds.append(md)
        appstream_ids.append(md.appstream_id)
        if md.fw.vendor not in vendors:
            vendors.append(md.fw.vendor)

    # get any vendor information as a fallback
    keywords_good: List[str] = []
    keywords_bad: List[str] = []
    if mds:
        keywords_good.extend(keywords)
        search_method = "FW"
    else:
        search_method = "AND"

    # always add vendor results
    for vendor in db.session.query(Vendor).filter(Vendor.visible_for_search):
        for kw in keywords:
            if vendor.keywords:
                if kw in vendor.keywords:
                    if vendor not in vendors:
                        vendors.append(vendor)
                    if kw not in keywords_good:
                        keywords_good.append(kw)
                    break
            if vendor.display_name:
                if kw in _split_search_string(vendor.display_name):
                    if vendor not in vendors:
                        vendors.append(vendor)
                    if kw not in keywords_good:
                        keywords_good.append(kw)
                    break
    for kw in keywords:
        if not kw in keywords_good:
            keywords_bad.append(kw)

    # this seems like we're over-logging but I'd like to see how people are
    # searching so we can tweak the algorithm used
    _add_search_event(
        SearchEvent(
            value=request.args["value"],
            addr=_addr_hash(_get_client_address()),
            count=len(mds) + len(vendors),
            method=search_method,
        )
    )

    return render_template(
        "search.html",
        mds=mds[:max_results],
        search_size=len(mds),
        vendors=vendors,
        keywords_good=keywords_good,
        keywords_bad=keywords_bad,
    )
