FROM quay.io/centos/centos:stream9
EXPOSE 5000

RUN dnf install -y epel-release dnf-plugins-core \
	&& dnf config-manager --set-enabled epel \
	&& dnf -y copr enable rhughes/lvfs-website \
	&& dnf -y --setopt=install_weak_deps=False --best install \
	git \
	bubblewrap \
	bsdtar \
	clamav \
	clamav-update \
	clamd \
	rizin \
	jq \
	gcc \
	python3-devel \
	UEFITool \
	&& rm -rf /var/cache/dnf

# create all our dirs
RUN bash -c 'mkdir -p /app/{scripts,conf,logs/uwsgi}' \
	&& mkdir /data /backups
WORKDIR /app

# build and install the serverless tools
RUN curl https://dl.google.com/go/go1.19.1.linux-amd64.tar.gz -o go-linux-amd64.tar.gz \
	&& tar -C /usr/local -xf go-linux-amd64.tar.gz \
	&& rm go-linux-amd64.tar.gz \
	&& git clone https://github.com/google/trillian-examples.git \
	&& cd trillian-examples \
	&& /usr/local/go/bin/go build ./serverless/cmd/sequence \
	&& /usr/local/go/bin/go build ./serverless/cmd/integrate \
	&& /usr/local/go/bin/go build ./serverless/cmd/generate_keys \
	&& /usr/local/go/bin/go build ./serverless/cmd/client \
	&& cp sequence integrate generate_keys client /bin/ \
	&& cd - \
	&& rm -rf /usr/local/go

# create and activate a venv
ENV VIRTUAL_ENV=/app/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# install all the things
COPY requirements.txt /app/conf
RUN pip3 install --no-cache-dir --upgrade pip \
	&& pip3 install --no-cache-dir wheel \
	&& pip3 install --no-cache-dir -r conf/requirements.txt

# and datadog
RUN pip3 install --no-cache-dir ddtrace

# copy the app; various configs and scripts
COPY wsgi.py /app/
COPY lvfs/ /app/lvfs/
COPY pkgversion/ /app/pkgversion/
COPY jcat/ /app/jcat/
COPY infparser/ /app/infparser/
COPY plugins/ /app/plugins/
COPY migrations/ /app/migrations/
COPY docker/files/application/gunicorn.py /app/conf/gunicorn.py
COPY docker/files/application/flaskapp.cfg /app/lvfs/flaskapp.cfg
COPY docker/files/application/scan.conf /etc/clamd.d/scan.conf
COPY docker/files/lvfs-entrypoint.sh /app/lvfs-entrypoint.sh

# install the public FwHunt rules
RUN git clone https://github.com/binarly-io/FwHunt.git \
	&& find FwHunt -name "*.yml" -exec cp -v {} /app/plugins/uefi_scanner/rules/ \;\
	&& rm -rf FwHunt

RUN chown -R 65534:65534 /app /data /backups

ENTRYPOINT [ "./lvfs-entrypoint.sh" ]
