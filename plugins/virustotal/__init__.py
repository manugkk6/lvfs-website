#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+

from typing import Dict

import requests

from lvfs.pluginloader import PluginBase, PluginError
from lvfs.pluginloader import PluginSetting, PluginSettingBool, PluginSettingList
from lvfs.tests.models import Test
from lvfs.firmware.models import Firmware
from lvfs.components.models import Component


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self)
        self.name = "VirusTotal Monitor"
        self.summary = "Upload firmware to VirusTotal for false-positive detection"
        self.settings.append(
            PluginSettingBool(key="virustotal_enable", name="Enabled", default=True)
        )
        self.settings.append(
            PluginSettingList(
                key="virustotal_remotes",
                name="Upload Firmware in Remotes",
                default=["stable", "testing"],
            )
        )
        self.settings.append(
            PluginSetting(key="virustotal_api_key", name="API Key", default="DEADBEEF")
        )
        self.settings.append(
            PluginSetting(
                key="virustotal_uri",
                name="Host",
                default="https://www.virustotal.com/api/v3/monitor/items",
            )
        )
        self.settings.append(
            PluginSetting(
                key="virustotal_user_agent", name="User Agent", default="LVFS"
            )
        )

    def ensure_test_for_fw(self, fw: Firmware) -> None:

        # is the firmware not in a correct remote
        remotes = self.get_setting("virustotal_remotes", required=True).split(",")
        if fw.remote.name not in remotes:
            return

        # add if not already exists on any component in the firmware
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id)
            fw.tests.append(test)

    def run_test_on_md(self, test: Test, md: Component) -> None:

        # sanity check
        if not md.blob:
            return

        # 32MB limit
        if len(md.blob) > 0x2000000:
            return

        # build the remote name
        remote_path = (
            "/"
            + md.fw.vendor.group_id
            + "/"
            + str(md.component_id)
            + "/"
            + md.filename_contents
        )

        # upload the file
        try:
            headers: Dict[str, str] = {}
            headers["X-Apikey"] = self.get_setting("virustotal_api_key", required=True)
            headers["User-Agent"] = self.get_setting(
                "virustotal_user_agent", required=True
            )
            url = self.get_setting("virustotal_uri", required=True)
            files = {"file": ("filepath", md.blob, "application/octet-stream")}
            args = {"path": remote_path}
            try:
                r = requests.post(
                    url,
                    files=files,
                    data=args,
                    headers=headers,
                    timeout=10,
                )
            except requests.exceptions.ReadTimeout as e:
                raise PluginError("Timeout from {}".format(url)) from e
            if r.status_code != 200:
                test.add_warn("Failed to upload", r.text)
                return
        except IOError as e:
            raise PluginError from e
