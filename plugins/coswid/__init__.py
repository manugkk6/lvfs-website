#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2022 Richard Hughes <richard@hughsie.com>
#
# SPDX-License-Identifier: GPL-2.0+
#
# pylint: disable=no-member,too-few-public-methods

from typing import Any
import pefile

from uswid import (
    uSwidContainer,
    uSwidIdentity,
    uSwidFormatCoswid,
    uSwidFormatSwid,
    uSwidFormatUswid,
    NotSupportedError,
)

from lvfs import db
from lvfs.pluginloader import PluginBase
from lvfs.pluginloader import PluginSettingBool
from lvfs.tests.models import Test
from lvfs.components.models import Component, ComponentSwid
from lvfs.firmware.models import Firmware


class Plugin(PluginBase):
    def __init__(self) -> None:
        PluginBase.__init__(self)
        self.rules = None
        self.name = "CoSWID"
        self.summary = "Look for CoSWID data for a SBoM"
        self.order_after = ["uefi-extract", "intelme"]
        self.settings.append(
            PluginSettingBool(key="coswid_enabled", name="Enabled", default=True)
        )

    def _add_identity(self, md: Component, identity: uSwidIdentity) -> None:
        """add test pass and add the SWID object"""
        md.swids.append(
            ComponentSwid(
                plugin_id=self.id,
                tag_id=identity.tag_id,
                value=uSwidFormatSwid().save(uSwidContainer([identity])).decode(),
            )
        )

    def _run_on_blob(self, test: Test, md: Component, title: str, blob: bytes) -> None:

        # a PE file with a COFF section
        try:
            pe = pefile.PE(data=blob)
            for sect in pe.sections:
                if sect.Name == b".sbom\0\0\0":
                    for identity in uSwidFormatCoswid().load(sect.get_data()):
                        test.add_pass("CoSWID", "{}: {}".format(title, str(identity)))
                        self._add_identity(md, identity)
        except pefile.PEFormatError:
            # not a PE file, which is fine
            pass

        # external uSWID sections
        try:
            for identity in uSwidFormatUswid().load(blob):
                test.add_pass("CoSWID", "{}: {}".format(title, str(identity)))
                self._add_identity(md, identity)
        except NotSupportedError:
            pass

    def ensure_test_for_fw(self, fw: Firmware) -> None:

        # add if not already exists
        test = fw.find_test_by_plugin_id(self.id)
        if not test:
            test = Test(plugin_id=self.id, waivable=True)
            fw.tests.append(test)

    def run_test_on_md(self, test: Test, md: Component) -> None:

        # clear all existing SWID objects created from this plugin
        for swid in md.swids:
            if swid.plugin_id == self.id:
                db.session.delete(swid)

        # run analysis on the component and any shards
        if md.blob:
            self._run_on_blob(test, md, md.filename_contents, md.blob)
        for shard in md.shards:
            if shard.blob:
                self._run_on_blob(test, md, shard.name, shard.blob)


# run with PYTHONPATH=. ./env/bin/python3 plugins/coswid/__init__.py
if __name__ == "__main__":
    import sys

    plugin = Plugin()
    _test = Test(plugin_id=plugin.id)
    _md = Component()
    for _argv in sys.argv[1:]:
        with open(_argv, "rb") as f:
            _md.blob = f.read()
        plugin.run_test_on_md(_test, _md)
        for attribute in _test.attributes:
            print(attribute)
