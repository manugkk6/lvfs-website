# Copyright (C) 2019 Richard Hughes <richard@hughsie.com>
# SPDX-License-Identifier: GPL-2.0+

VENV=./env
PYTHON=$(VENV)/bin/python
PYTEST=$(VENV)/bin/pytest
PYLINT=$(VENV)/bin/pylint
MYPY=$(VENV)/bin/mypy
SPHINX_BUILD=$(VENV)/bin/sphinx-build
FLASK=$(VENV)/bin/flask
CODESPELL=$(VENV)/bin/codespell
PIP=$(VENV)/bin/pip
BLACK=$(VENV)/bin/black
BANDIT=$(VENV)/bin/bandit

setup: requirements.txt
	virtualenv ./env
	$(VENV)/bin/pip install -r requirements.txt
	$(VENV)/bin/pip install pre-commit
	$(VENV)/bin/pre-commit install

clean:
	rm -rf ./build
	rm -rf ./htmlcov

run:
	KRB5CCNAME=/tmp FLASK_DEBUG=1 FLASK_APP=lvfs/__init__.py $(VENV)/bin/flask run

profile:
	FLASK_DEBUG=1 FLASK_APP=lvfs/__init__.py $(VENV)/bin/python run-profile.py

dbup:
	KRB5CCNAME=/tmp FLASK_APP=lvfs/__init__.py $(FLASK) db upgrade

dbdown:
	KRB5CCNAME=/tmp FLASK_APP=lvfs/__init__.py $(FLASK) db downgrade

freeze:
	$(PIP) freeze > requirements.txt

dbmigrate:
	KRB5CCNAME=/tmp FLASK_APP=lvfs/__init__.py $(FLASK) db migrate

dbdrop:
	KRB5CCNAME=/tmp FLASK_APP=lvfs/__init__.py $(FLASK) dropdb

docs:
	$(SPHINX_BUILD) docs build

worker:
	KRB5CCNAME=/tmp PYTHONPATH=. ./env/bin/python ./lvfs/worker.py

codespell: $(CODESPELL)
	$(CODESPELL) --write-changes --builtin en-GB_to_en-US --skip \
	.git,\
	.mypy_cache,\
	.coverage,\
	*.pyc,\
	*.cab,\
	*.png,\
	*.jpg,\
	*.js,\
	*.doctree,\
	*.pdf,\
	*.gz,\
	*.ico,\
	*.jcat,\
	*.pickle,\
	*.key,\
	env,\
	shards,\
	owl.carousel.js

$(PYTEST):
	$(PIP) install pytest-cov pylint pytest-pylint pyserial

$(MYPY):
	$(PIP) install mypy types-requests types-python-dateutil

$(BANDIT):
	$(PIP) install bandit

$(BLACK):
	$(PIP) install black

$(CODESPELL):
	$(PIP) install codespell

blacken:
	find \
	cabarchive \
	contrib \
	infparser \
	jcat \
	lvfs \
	pkgversion \
	plugins \
	-name '*.py' -exec $(BLACK) {} \;

pyup:
	rm -rf ./env
	virtualenv ./env
	$(PIP) install -r requirements.src
	$(PIP) freeze > requirements.txt

check: $(PYTEST) $(MYPY) $(BANDIT) contrib/blocklist.cab contrib/chipsec.cab
	KRB5CCNAME=/tmp $(PYTEST) \
		--cov=lvfs \
		--cov=pkgversion \
		--cov=infparser \
		--cov=plugins \
		--cov-report=html
	KRB5CCNAME=/tmp $(MYPY) --check-untyped-defs pkgversion lvfs plugins contrib
	KRB5CCNAME=/tmp $(BANDIT) -r -s B101,B106,B303,B107,B404,B311,B603,B320,B410,B108,B324,B105,B608 \
		jcat pkgversion lvfs plugins contrib
