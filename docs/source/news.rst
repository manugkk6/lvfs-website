LVFS Releases
#############

1.5.0 (2023-01-03)
==================

This release adds the following features:

 * Add a fsck action for the VersionFormat, release_tag, shard info GUIDs and key checksum
 * Add a user-visible claim for a detected SBoM
 * Add a waiveable test failure on system integrity report failure
 * Add BootGuard shards when extracting UEFI firmware
 * Add interesting public test failures to the mdsync export
 * Add new update categories like X-UsbDock and X-UsbReceiver
 * Allow filtering by tag when using sync-pulp.py
 * Allow setting the update message and image per-protocol
 * Cache the public pages to reduce load
 * Do not allow all protocols to use the X-Device category
 * Enforce no duplicate objects in the db layer
 * Enforce that release timestamp is not >2 years in the future or past
 * Generate additional xz metadata for a 25% size saving
 * Generate the PULP_MANIFEST at remote regeneration time
 * Only allow custom update messages for specific protocols
 * Remove the client useragent and country code after 3 years
 * Replace Celery with a built-in task scheduler and remove the beat ECS service
 * Replace uefi_r2 with fwhunt_scan to support new rules format
 * Show the HSI number in more places
 * Use a Flask application factory pattern
 * Verify the upload was written on EFS
 * Warn the QA user when promoting a firmware with no success reports

This release fixes the following bugs:

 * Add a more indexes to speed up database access
 * Allow choosing non-public protocols in the component view
 * Allow running local.py without a database set up
 * Allow the QA user to modify the component release date
 * Always ignore the first section in the reverse-DNS validation
 * Bind to all IPv4 and IPv6 interfaces
 * Build the docker container on CentOS 9 Stream
 * Check that previous CHIDs are always included in new firmware
 * Dedeupe URIs when sending report response
 * Detect and fix duplicate users
 * Do not assume every AppStream ID with 4 dashes is a GUID
 * Do not clear the waived timestamp when retrying a test
 * Do not disable 2FA when changing the users password
 * Do not export the metadata_license in the AppStream metadata
 * Do not fail the UEFI capsule test when using a valid FMP GUID
 * Do not garbage-collect old revisions when the latest revision is new
 * Do not include empty <device> tags in the metadata
 * Do not include the component description in the AppStream metadata
 * Do not mark the OTP textbox as 'password'
 * Do not require admin login to download a known shard
 * Do not show problems in the search view to fix performance issues
 * Do not store the firmware or remote dirty state and use runtime state instead
 * Do not use a HTML 404 page when downloading from a client
 * Do not use ; to split URIs, it's a valid char in RFC3986
 * Fix a crash when parsing very old HSI reports
 * Fix a warning when a PE file has no authenticode signature
 * Fix the displayed URLs and display name in the LVFS emails
 * Include the vendor name in the mdsync output
 * Increase the pulp download timeout to 60s
 * Make all the icons symbolic to match gnome-firmware
 * Make the eventlog address field more than 40 chars
 * Make the HSI aggregated data public
 * Move firmware promote and nuke to an async action
 * Only add <testing> elements when using artifacts
 * Only include the sizes for the artifact
 * Prevent duplicate usernames
 * Prevent the human user from being the same as the username
 * Relax the backdated checks to include older firmware
 * Remove all users of _error_internal()
 * Remove some unused database columns and obsolete migration scripts
 * Remove the hardcoded and duplicated release description text
 * Remove the IPFS functionality as it was almost completely unused
 * Remove the per-vendor event-log page
 * Remove the tests overview page, as this does not scale
 * Show a warning when doing an async promotion
 * Show the OEM firmware in 'State :: Embargo' for ODMs
 * Show when a user waived the test in the UI
 * Speed up downloading cab archives and most page loads
 * Update uSWID to fix reading and writing compressed payloads
 * Use less whitespace in the AppStream metadata file
 * Use the checksum as the shard absolute path as the name is not always unique
 * Use the correct artifact type for metainfo.xml files
 * Use the correct status code for mdsync export
 * Use the flask debug toolbar when running locally

1.4.0 (2022-05-24)
==================

This release adds the following features:
 * Add a progress indicator to the Yara scan
 * Add 'fwupd friendly firmware' certification
 * Add information about what models are EOL
 * Add new categories of X-Mouse and X-BaseboardManagementController
 * Add support for asynchronous uploads
 * Add support for external uSWID+CoSWID sections
 * Add the concept of vendor subgroups
 * Add device icons of usb-hub and usb-receiver
 * Add XLIFF v2 import and export for translation
 * Allow auto-moving firmware on defined dates
 * Allow creating a GUID from an instance ID
 * Allow creating a uSWID blob from form data
 * Allow firmware to have multiple ODMs
 * Allow importing, exporting and modifying localized update release notes
 * Allow marking firmware revisions as immutable
 * Allow updates to specify a level of device integrity
 * Allow uploading firmware using a username and token
 * Analyze Intel microcode versions
 * Build metadata into a firmware transparency log
 * Export the LVFS component ID into the AppStream metadata
 * Get the CVE descriptions description from VINCE and NIST NVD
 * Show the metadata upload failures in the UI
 * Use name_variant_suffix in the public metadata
 * Use signed reports for firmware QA
 * Use the CDN to distribute firmware

This release fixes the following bugs:
 * Add client requirements to the metadata
 * Add more JCat blob kinds
 * Allow modifications in the testing target
 * Allow OAuth users to modify subgroup and notification settings
 * Allow QA users to delete limits
 * Allow security researchers to run UEFI R2 scripts
 * Allow specifying file:// images that are copied from the archive
 * Allow users to share the [possibly private] signed report data
 * Check for the duplicate remote before checking problems
 * Detect more vendors pasting in Intel SA issues
 * Do no merge component with different self requirements
 * Do not allow an unsigned report to adjust the output of a signed one
 * Do not allow some name_variant_suffix content
 * Do not backtrace when trying to compare UTF-8 and UTF-16 text
 * Do not export optional component data XML
 * Do not force 'number' verfmts to hex in the metainfo
 * Do not show test passes in uefi_scanner
 * Do not split search terms on the hyphen
 * Do not use Google Fonts
 * Fix a crash when a component description was not set
 * Fix crash when old stable firmware has no update description
 * Fix runtime exception when checking inactive users
 * Ignore markdown elements with control chars
 * Make autoimporting issues CSRF-safe
 * Make Claim.allow_embargo per-instance, not per-class
 * Make the license have an optional clickable URL
 * Make the recovery email case insensitive
 * Make the update useful word requirement lower
 * Move some upload issues to runtime component problems
 * Never include ampersands in the revision filename
 * Never try to escape missing paragraph text
 * No longer detect Intel BIOSGuard
 * Remove parsing the developer_name tag
 * Remove the vendor description
 * Save non-empty UEFI padding sections as shards
 * Set a max-age when sending chunked files
 * Show a notification if unable to change component values
 * Show a warning when a security update is detected without any issues
 * Show better verified report output
 * Use a bubble graph for the CVE timeline
 * Use a volume guids to make UEFI R2 queries much, much faster
 * Use the AppStream ID when deduping uploaded firmware
 * Use the mirrored release image in more cases
 * Verify the AppStream ID was valid if modified

1.3.2 (2021-06-22)
==================

This release adds the following features:

 * Add an optional PSIRT URL for each vendor
 * Add a plugin which uses uefi_r2 to add shard attributes
 * Add support for component soft-requirements
 * Allow exporting the embargoed firmware using PULP_MANIFEST
 * Allow searching for files by checksum on the internal dashboard
 * Allow vendor managers to purge firmware without asking an admin
 * Do not overwrite when resigning and use unique filenames for each revision

This release fixes the following bugs:

 * Be more helpful when failing to load invalid XML
 * Dedupe the component requirements where allowed
 * Do not allow the update description to contain the firmware name
 * Do not autodecode content when mirroring using sync-pulp.py
 * Explicitly set the CDN Cache-Control to be 4 hours by default
 * Ask vendors to provide 10 useful release description words
 * Include the update images in the PULP_MANIFEST file
 * Resign any files that do not include the PKCS#7 certificate

1.3.1 (2021-04-06)
==================

This release adds the following features:

 * Add a firmware timestamp that specifies the CVE embargo date
 * Add a LVFS component problem if the version format is inconsistent
 * Hard require the version format to allow pushing to stable
 * Record the reason for moving a firmware to a new remote
 * Record the user and when a component issue was added
 * Support VINCE security advisory IDs

This release fixes the following bugs:

 * Allow setting a vendor default for the .inf firmware parsing
 * Allow uploading files with all issue types
 * Fix some checksum confusion for duplicate firmware
 * Fix unpinning files using Pinata
 * Fix warnings with new SQLAchemy versions
 * Never include generic components in the mdsync data
 * Return JSON for robot uploaders
 * Store the old remote ID in the FirmwareEvent
 * Use the remote name, not the icon name for mdsync export
 * Write the <issue> tags into the AppStream metadata

1.3.0 (2021-02-08)
==================

This release adds the following features:

 * Add new page for the latest devices supported
 * Add support for the ``<artifact>`` AppStream tag
 * Add support for the Intel technical advisory issue tags
 * Allow adding optional default icons to categories and protocols
 * Allow components to specify an optional branch
 * Allow exporting the component back to MetaInfo XML format
 * Assign a release tag style for specific vendor per-category
 * Mirror non-export-controlled public firmware to IPFS
 * Provide a healthcheck endpoint
 * Send a monthly email about firmware left in embargo or testing
 * Show a device status page showing all the versions in all remotes

This release fixes the following bugs:

 * Add missing support for ``LVFS::UpdateImage`` and ``Verfmt('number')``
 * Add some documentation on adding screenshots and using the LVFS offline
 * Allow adding and removing component GUIDs on the web UI
 * Allow a ``<project_license>`` of BSD
 * Allow changing firmware licenses without re-uploading firmware
 * Allow non-admin users to resign firmware
 * Allow QA users to change the component name, ID and summary
 * Allow searching by filename, requirement or CVE when logged in
 * Allow supplying a generic 'overview' component for composite devices
 * Allow vendors to specify client requirements
 * Change the dropped-GUID from an upload ``flash()`` to a waivable test
 * Check for more sneaky CVEs in update descriptions
 * De-duplicate the requirements where appropriate
 * Do not allow the vendor name "BIOS", "fwupd" or "LVFS" in the firmware ``<name>``
 * Do not do the GUID check against firmware uploaded to private
 * Do not ever store the client hashed IP address in the database
 * Do not use ``send_from_directory()`` to send large files
 * Fix all CSRF issues after some security review
 * Fix performance issue when getting recent firmware downloads
 * Include the copyright information for MIT licenses
 * Increase the upload timeout to 10 minutes
 * Move the disable 2FA slider to a button
 * Parse the AMI FPAT firmware prior to scanning with UEFIExtract
 * Provide a nudge when editing a component if required values are unset
 * Purge firmware that is deleted after just 30 days
 * Record the client country code for analytics
 * Reduce the number of buttons on the component overview
 * Regenerate embargo remotes when modifying restrictions
 * Run any pending tests every 60 minutes
 * Update the bundled version of Chart.js
 * Update the ``README.txt`` file during package signing
 * Use a non-predictable vendor icon filename
 * Use PyGnuTLS rather than using certtool when signing files
 * Use python-cabarchive rather than GCab for parsing
 * Use the CDN to serve public static files
 * Write the ``PULP_MANIFEST`` with a predicatable order

1.2.0 (2020-06-09)
==================

This release adds the following features:

* Add a filter view for user uploaded firmware
* Add a plugin to identify old microcode versions
* Add cached public stats of useful metrics
* Add support for LVFS::UpdateMessage
* Allow clients to upload anonymous HSI attrs
* Allow re-signing binaries
* Create Jcat files in archives and for metadata
* Delete firmware in embargo with newer public versions
* Disable unused user accounts for GDPR compliance
* Export the success confidence to the mdsync vendor
* Include LVFS::UpdateProtocol in the metadata
* Rewrite the AppStream screenshot URL to use the server CDN
* Rewrite the metainfo when signing the firmware
* Save metadata about Intel microcode blobs
* Support Lenovo, Dell and Intel specific security tags
* Use celery to process async operations

This release fixes the following bugs:

* Allow all users to view the profile page
* Allow a protocol to have no defined version format
* Allow QA users to see all ODM firmware uploaded
* Allow setting the category to 'Unknown'
* Allow specifying firmware versions when using the advanced requires editor
* Do not allow component modification when in testing and stable
* Do not backtrace if a component does not have a <name>
* Do not include a CSRF for public search queries
* Do not include the VersionFormat fallbacks if the fw requires a new enough fwupd
* Do not make the database server explode with a query like 'value=+foo'
* Do not save duplicate <requires>vendor-id</> tags to the metadata
* Ensure firmware again when it changes state
* Fix a regression when component claims were not being added
* Fix regression when getting security level of component
* Improve the report query speed by several orders of magnitude
* Include the vendor tag in the rewritten metainfo and AppStream XML
* Invalidate ODM remotes when a firmware is demoted back to private
* List <id> requires first in the metadata
* Make it more obvious that the firmware is waiting to be signed
* Make the LVFS username case insensitive
* Make the markdown to root function more robust
* Parse the <metadata_license> even when not in strict mode
* Set the SHA256 content checksum in the metadata
* Show a disabled button when the user has no ACL to move the firmware

1.1.6 (2020-01-28)
==================

This release adds the following features:

* Add a atom feed to public device page
* Add a claim for systems supporting Intel BiosGuard and BootGuard
* Add a ``dell-bios`` version format
* Add a page to list consultants that can work on the LVFS
* Add a plugin to add component claims for specific shard GUIDs
* Add a release tag to store the vendor-specific firmware identifier
* Allow adding component claims based on the hash of a shard
* Allow syncing with other firmware databases
* Move the formal documentation to Sphinx

This release fixes the following bugs:

* Add many more database indexes to improve performance
* Add some missing vendor checks when proxying to the user ACL
* Allow vendor managers to see a read-only view of the restrictions page
* Always use the vendor-id restrictions of the ODM, not the OEM
* Fix support for multiple ``LVFS::VersionFormat`` tags
* Include a vendor ID by default for testing accounts
* Make more queries compatible with PostgreSQL
* Never include firmware in private in any embargo remote
* Only show vendors with LVFS users on the vendorlist
* Reduce the memory consumption when running cron and doing yara queries
* Update the firmware report count at upload time
* Use SHA256 when storing the upload checksum
* Use the correct filename for a PKCS-7 payload signature
* Use UEFIExtract rather than chipsec to extract shards

1.1.5 (2019-11-15)
==================

This release adds the following features:

* Add support for matching firmware requirements on device parents
* Allow researchers to run YARA queries on the public firmware
* Allow the blocklist plugin to add persistent claims
* Use PSPTool to parse the AMD PSP section

This release fixes the following bugs:

* Add the Dell PFS as a component shard
* Allow the owner of the firmware to always change update details
* Convert to Blueprints to improve page loading time
* Do not hardcode the list of version formats in various places
* Do not share the shard name between GUIDs
* Only auto-demote stable-to-testing, not testing-to-embargo or stable-to-embargo
* Show the version format versions with no trailing zeros

1.1.4 (2019-09-26)
==================

This release adds the following features:

* Add component issues such as CVEs in a structured way
* Add more OEM notification emails for ODM actions
* Add support for name variant suffixes
* Add vendor namespaces to enforce ODM relationships
* Allow searching for CVEs when logged in
* Allow the OEM to better control what the ODM is able to do

This release fixes the following bugs:

* Allow vendors to optionally disable the inf parsing
* Blacklist generic GUIDs like 'main-system-firmware'
* Check the source and release URLs are valid if provided
* Do not show deleted firmware on the recent list on the dashboard
* Don't auto-demote firmware because of old reports
* Enforce the VersionFormat if the version is an integer
* Fix a crash if uploading a file with a missing metadata_license tag
* Provide a way to un-disable users as a vendor manager
* Regenerate embargo remotes ever 5 minutes
* Use a sane error message on upload when a component drops a GUID

1.1.3 (2019-08-06)
==================

This release adds the following features:

* Show a nag message for admin or manager account without 2FA
* Do not use AppStream-glib to parse the metainfo file
* Automatically demote firmware with more than 5 failures and a success rate of %lt;70%
* Allow firmware or vendors to enable DoNotTrack functionality
* Show the user capabilities in the headerbar
* Protect all forms against CSRF

This release fixes the following bugs:

* Retry all existing tests if the category or protocol is changed
* Do not allow forward slashes in AppStream ID values
* Use a proper AppStream ID for the CHIPSEC shards
* Show flashed messages on the landing page
* Better support firmware requires without conditions or versions
* Do not allow AppStream markup in non description elements

1.1.2 (2019-05-28)
==================

This release adds the following features:

* Add a new plugin to check portable executable files
* Save the shards in an on-disk cache which allows re-running tests
* Add a failure for any firmware that is signed with a 3-year expired certificate
* Add shard certificates to the database and show them in the component view

This release fixes the following bugs:

* Make it easier to enter multiline text as plugin settings

1.1.1 (2019-05-21)
==================

This release adds the following features:

* Allow managers to edit their own list of embargoed countries
* Record the size and entropy of the component shards when parsing
* Analyze Intel ME firmware when it is uploaded

This release fixes the following bugs:

* Do not expect device checksums for ME or EC firmware

1.1.0 (2019-05-14)
==================

This release adds the following features:

* Run CHIPSEC on all UEFI firmware files
* Show details of UEFI firmware volumes for capsule updates
* Show differences between public revisions of firmware
* Provide some extra information about detected firmware shards

This release fixes the following bugs:

* Only decompress the firmware once when running tests
* Make the component detail page a bit less monolithic
* Never leave tests in the running state if a plugin crashes

1.0.0 (2019-05-02)
==================

This release adds the following features:

* Allow the admin to change the AppStream ID or name of components

This release fixes the following bugs:

* Do not allow the telemetry card title to overflow
* Ensure the ``firmware-flashed`` value is a valid lowercase GUID
* Make the component requirements page easier to use
* Do not add duplicate ``<hardware>`` values
* Remove the hard-to-use breadcrumb and use a single back button
