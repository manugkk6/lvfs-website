#!/bin/sh
set -e
set -x

pytest --cov=lvfs --cov=plugins --cov=pkgversion --cov=infparser --cov-fail-under=60
